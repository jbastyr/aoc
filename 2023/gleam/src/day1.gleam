import gleam/string
import gleam/int
import gleam/pair
import gleam/list
import gleam/result

fn update_pair(acc: #(String, String), val: String) -> #(String, String) {
  case pair.first(acc), pair.second(acc) {
    "0", "0" -> #(val, val)
    f, _ -> #(f, val)
  }
}

fn try_word(val: String) -> Result(String, Nil) {
  [
    #("one", "1"),
    #("two", "2"),
    #("three", "3"),
    #("four", "4"),
    #("five", "5"),
    #("six", "6"),
    #("seven", "7"),
    #("eight", "8"),
    #("nine", "9"),
  ]
  |> list.filter(fn(x) { string.starts_with(val, pair.first(x)) })
  |> list.map(pair.second)
  |> list.first
}

fn do_next(
  vals: List(String),
  use_words: Bool,
  acc: #(String, String),
) -> #(String, String) {
  case vals {
    [first, ..rest] ->
      case int.parse(first), use_words {
        Error(_), True ->
          case try_word(string.join(vals, "")) {
            Ok(res) -> do_next(rest, use_words, update_pair(acc, res))
            Error(_) -> do_next(rest, use_words, acc)
          }
        Error(_), False -> do_next(rest, use_words, acc)
        Ok(_), _ -> do_next(rest, use_words, update_pair(acc, first))
      }
    [] -> acc
  }
}

fn combine(p: #(String, String)) -> Int {
  pair.first(p)
  |> string.append(pair.second(p))
  |> int.parse
  |> result.unwrap(0)
}

pub fn calibration(input: String, use_words: Bool) -> Int {
  input
  |> string.to_graphemes
  |> do_next(use_words, #("0", "0"))
  |> combine
}
