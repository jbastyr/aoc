import gleam/io

pub fn main() {
  io.println("Hello from aoc2023!")
  io.println("run `gleam test` to execute all days")
}
