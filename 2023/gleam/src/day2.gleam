import gleam/string
import gleam/list
import gleam/int
import gleam/result
import gleam/pair
import gleam/bool

pub type GameStage {
  GameStage(red: Int, green: Int, blue: Int)
}

pub type Game {
  Game(id: Int, stages: List(GameStage))
}

// "5 blue"
fn parse_pull(pull: String) -> Result(#(Int, String), Nil) {
  case string.split(pull, " ") {
    [count_s, color] ->
      case int.parse(count_s) {
        Ok(count) -> Ok(#(count, color))
        Error(_) -> Error(Nil)
      }
    _ -> Error(Nil)
  }
}

// "5 blue, 3 red"
fn parse_stage(stage: String) -> GameStage {
  string.split(stage, ", ")
  |> list.map(parse_pull)
  |> result.values()
  |> list.fold(GameStage(0, 0, 0), fn(acc, cur) {
    case pair.first(cur), pair.second(cur) {
      c, "red" -> max_stage(acc, GameStage(c, 0, 0))
      c, "green" -> max_stage(acc, GameStage(0, c, 0))
      c, "blue" -> max_stage(acc, GameStage(0, 0, c))
      _, _ -> acc
    }
  })
}

// "Game 1: 5 blue, 3 red; 2 green; 1 blue" 
pub fn parse_game(game: String) -> Result(Game, Nil) {
  case string.split(game, ": ") {
    [game_title, stages] ->
      case string.split(game_title, " ") {
        ["Game", id_s] ->
          case int.parse(id_s) {
            Ok(id) ->
              Ok(Game(
                id,
                string.split(stages, "; ")
                |> list.map(parse_stage),
              ))
            _ -> Error(Nil)
          }
        _ -> Error(Nil)
      }
    _ -> Error(Nil)
  }
}

fn does_not_exceed(stage: GameStage, limit: GameStage) -> Bool {
  [stage.red <= limit.red, stage.green <= limit.green, stage.blue <= limit.blue]
  |> list.fold(True, bool.and)
}

// fn concat_stage(a: GameStage, b: GameStage) -> GameStage {
//   GameStage(a.red + b.red, a.green + b.green, a.blue + b.blue)
// }

fn max_stage(a: GameStage, b: GameStage) -> GameStage {
  GameStage(
    int.max(a.red, b.red),
    int.max(a.green, b.green),
    int.max(a.blue, b.blue),
  )
}

pub fn check_game(game: Game, limit: GameStage) -> Bool {
  game.stages
  |> list.fold(GameStage(0, 0, 0), max_stage)
  |> does_not_exceed(limit)
}

pub fn sum_valid_games(games: List(Game), limit: GameStage) -> Int {
  games
  |> list.filter(fn(game) { check_game(game, limit) })
  //|> io.debug
  |> list.map(fn(game) { game.id })
  |> list.fold(0, int.add)
}

pub fn find_game_min(game: Game) -> GameStage {
  game.stages
  |> list.fold(GameStage(0, 0, 0), max_stage)
}

pub fn get_game_power(game: Game) -> Int {
  let stage = find_game_min(game)
  stage.red * stage.green * stage.blue
}
