import gleam/io
import gleam/list
import gleam/pair
import gleam/result
import gleam/int
import gleam/string
import gleeunit/should
import simplifile
import day2

pub fn day2_given_p1_test() {
  let cases = [
    #("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", True),
    #("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", True),
    #(
      "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
      False,
    ),
    #(
      "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
      False,
    ),
    #("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", True),
  ]

  io.debug("day2 part1 running cases")

  let limit = day2.GameStage(12, 13, 14)

  cases
  |> list.map(parse_first)
  |> list.each(fn(game) {
    case pair.first(game) {
      Ok(game_val) ->
        should.equal(day2.check_game(game_val, limit), pair.second(game))
      _ -> {
        io.debug("Game did not parse properly")
        Nil
      }
    }
  })

  cases
  |> list.map(pair.first)
  |> list.map(day2.parse_game)
  |> result.values
  |> day2.sum_valid_games(limit)
  |> io.debug
}

pub fn day2_given_p2_test() {
  let cases = [
    #("Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green", 48),
    #("Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue", 12),
    #(
      "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
      1560,
    ),
    #(
      "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
      630,
    ),
    #("Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green", 36),
  ]

  io.debug("day2 part2 running cases")

  cases
  |> list.map(parse_first)
  |> list.each(fn(game) {
    case pair.first(game) {
      Ok(game_val) ->
        should.equal(day2.get_game_power(game_val), pair.second(game))
      _ -> {
        io.debug("Game did not parse properly")
        Nil
      }
    }
  })

  cases
  |> list.map(pair.first)
  |> list.map(day2.parse_game)
  |> result.values
  |> list.map(day2.get_game_power)
  |> list.fold(0, int.add)
  |> io.debug
}

fn parse_first(val: #(String, a)) -> #(Result(day2.Game, Nil), a) {
  pair.map_first(val, day2.parse_game)
}

pub fn day2_p1_test() {
  let assert Ok(data) = simplifile.read("./test/data/day2.txt")

  let limit = day2.GameStage(12, 13, 14)

  io.debug("day2 part1 output: ")
  data
  |> string.split("\n")
  |> list.map(day2.parse_game)
  |> result.values
  //|> io.debug
  |> day2.sum_valid_games(limit)
  |> io.debug
}

pub fn day2_p2_test() {
  let assert Ok(data) = simplifile.read("./test/data/day2.txt")

  io.debug("day2 part2 output: ")
  data
  |> string.split("\n")
  |> list.map(day2.parse_game)
  |> result.values
  |> list.map(day2.get_game_power)
  |> list.fold(0, int.add)
  |> io.debug
}
