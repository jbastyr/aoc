import gleam/io
import gleam/list
import gleam/pair
import gleam/string
import gleeunit/should
import simplifile
import day1

fn try_calibration(data: #(String, Int), use_words: Bool) -> Int {
  //io.debug(data)
  should.equal(get_calibration(pair.first(data), use_words), pair.second(data))
  pair.second(data)
}

fn get_calibration(data: String, use_words: Bool) -> Int {
  day1.calibration(data, use_words)
}

pub fn day1_given_p1_test() {
  let cases = [
    #("1abc2", 12),
    #("pqr3stu8vwx", 38),
    #("a1b2c3d4e5f", 15),
    #("treb7uchet", 77),
  ]
  io.debug("day1 part1 running cases")

  cases
  |> list.map(fn(x) { try_calibration(x, False) })
  |> list.fold(0, fn(acc, cur) { acc + cur })
  |> io.debug
}

pub fn day1_given_p2_test() {
  let cases = [
    #("two1nine", 29),
    #("eightwothree", 83),
    #("abcone2threexyz", 13),
    #("xtwone3four", 24),
    #("4nineeightseven2", 42),
    #("zoneight234", 14),
    #("7pqrstsixteen", 76),
  ]
  io.debug("day1 part2 running cases")

  cases
  |> list.map(fn(x) { try_calibration(x, True) })
  |> list.fold(0, fn(acc, cur) { acc + cur })
  |> io.debug
}

pub fn day1_p1_test() {
  let assert Ok(data) = simplifile.read("./test/data/day1.txt")

  io.debug("day1 part1 output: ")
  data
  |> string.split("\n")
  |> list.map(fn(x) { get_calibration(x, False) })
  |> list.fold(0, fn(acc, cur) { acc + cur })
  |> io.debug
}

pub fn day1_p2_test() {
  let assert Ok(data) = simplifile.read("./test/data/day1.txt")

  io.debug("day1 part2 output: ")
  data
  |> string.split("\n")
  |> list.map(fn(x) { get_calibration(x, True) })
  |> list.fold(0, fn(acc, cur) { acc + cur })
  |> io.debug
}
