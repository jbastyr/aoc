import gleam/string
import gleam/list
import gleam/result
import gleam/int

pub fn final_floor(input: String) -> Int {
  input
  |> string.to_graphemes
  |> list.map(fn(g) {
    case g {
      ")" -> Ok(-1)
      "(" -> Ok(1)
      _ -> Error(Nil)
    }
  })
  |> result.values
  |> list.fold(0, int.add)
}
