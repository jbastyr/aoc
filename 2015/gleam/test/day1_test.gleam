import gleam/int
import gleam/io
import gleam/list
import gleam/pair
import gleam/result
import gleam/string
import gleeunit/should
import simplifile
import day1

fn line_to_pair(line: String) -> Result(#(String, Int), Nil) {
  let split = string.split(line, " ")
  case split {
    [a, b] -> {
      case int.parse(b) {
        Ok(floor) -> Ok(pair.new(a, floor))
        _ -> Error(Nil)
      }
    }
    _ -> Error(Nil)
  }
}

fn try_final_floor(data: #(String, Int)) -> Nil {
  should.equal(pair.second(data), get_final_floor(pair.first(data)))
}

fn get_final_floor(data: String) -> Int {
  day1.final_floor(data)
}

pub fn day1_given_test() {
  let assert Ok(data) = simplifile.read("./test/data/day1/given.txt")
  data
  |> string.split("\n")
  |> list.map(line_to_pair)
  |> result.values
  |> list.each(try_final_floor)
}

pub fn day1_input_test() {
  let assert Ok(data) = simplifile.read("./test/data/day1/input.txt")
  let floor = get_final_floor(data)
  io.debug(string.append("day1 result: ", int.to_string(floor)))
}
