#ifndef MAIN_H
#define MAIN_H

#include "lib.h"

void day1(void);
void day2(void);
void day3(void);
void day4(void);
void day5(void);
void day6(void);
void day7(void);
void day8(void);
void day9(void);
void day10(void);
void day11(void);
void day12(void);
void day13(void);
void day14(void);
void day15(void);
void day16(void);
void day17(void);
void day18(void);
void day19(void);
void day20(void);
void day21(void);
void day22(void);
void day23(void);
void day24(void);
void day25(void);
void day26(void);
void day27(void);
void day28(void);
void day29(void);
void day30(void);
void day31(void);
char *readFile(char *fileName);

int runMany(char *input, dayFn day, reducerInt reducer, int init);

#endif
