#ifndef DAYS_H
#define DAYS_H

int day1_part1(char *input);
int day1_part2(char *input);

int day2_part1(char *input);
int day2_part2(char *input);

int day3_part1(char *input);
int day3_part2(char *input);

int day4_part1(char *input);
int day4_part2(char *input);

int day5_part1(char *input);
int day5_part2(char *input);

int day6_part1(char *input);
int day6_part2(char *input);

int day7_part1(char *input, char *target);
int day7_part2(char *input, char *target, char *overrideTarget);

int day8_part1(char *input);
int day8_part2(char *input);

int day9_part1(char *input);
int day9_part2(char *input);

int day10_part1(char *input);
int day10_part2(char *input);

int day11_part1(char *input);
int day11_part2(char *input);

int day12_part1(char *input);
int day12_part2(char *input);

int day13_part1(char *input);
int day13_part2(char *input);

int day14_part1(char *input);
int day14_part2(char *input);

int day15_part1(char *input);
int day15_part2(char *input);

int day16_part1(char *input);
int day16_part2(char *input);

int day17_part1(char *input);
int day17_part2(char *input);

int day18_part1(char *input);
int day18_part2(char *input);

int day19_part1(char *input);
int day19_part2(char *input);

int day20_part1(char *input);
int day20_part2(char *input);

int day21_part1(char *input);
int day21_part2(char *input);

int day22_part1(char *input);
int day22_part2(char *input);

int day23_part1(char *input);
int day23_part2(char *input);

int day24_part1(char *input);
int day24_part2(char *input);

int day25_part1(char *input);
int day25_part2(char *input);

int day26_part1(char *input);
int day26_part2(char *input);

int day27_part1(char *input);
int day27_part2(char *input);

int day28_part1(char *input);
int day28_part2(char *input);

int day29_part1(char *input);
int day29_part2(char *input);

int day30_part1(char *input);
int day30_part2(char *input);

int day31_part1(char *input);
int day31_part2(char *input);

#endif
