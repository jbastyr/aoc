#include <string.h>
#include <stdio.h>
#include "lib.h"

int day2_part1(char *input) {
  const char *delim = "\n";
  char *token = NULL;
  int result = 0;
  int l = 0, w = 0, h = 0;

  token = strtok(input, delim);
  while(token != NULL) {
    if (DEBUG) printf("parsing %s\n", token);
    if (sscanf(token, "%dx%dx%d", &l, &w, &h) == 3) {
      if (DEBUG) printf("found %d x %d x %d\n", l, w, h);

      int lw = l * w, lh = l * h, wh = w * h;
      if (DEBUG) printf("found face area lw %d, lh %d, wh %d\n", lw, lh, wh);

      int surface = 2 * lw + 2 * lh + 2 * wh;

      int least = min(min(lw, lh), wh);
      int total = surface + least;
      if (DEBUG) printf("found surface %d min %d total %d\n", surface, least, total);

      result += total;
    } else {
      fputs("unexpected token\n", stderr);
    }
    token = strtok(NULL, delim); 
  }

  return result;
}

int day2_part2(char *input) {
  const char *delim = "\n";
  char *token = NULL;
  int result = 0;
  int l = 0, w = 0, h = 0;


  token = strtok(input, delim);
  while(token != NULL) {
    if (DEBUG) printf("parsing %s\n", token);
    if (sscanf(token, "%dx%dx%d", &l, &w, &h) == 3) {
      if (DEBUG) printf("found %d x %d x %d\n", l, w, h);

      int lw = 2 * (l + w), lh = 2 * (l + h), wh = 2 * (w + h);
      if (DEBUG) printf("found face perim lw %d, lh %d, wh %d\n", lw, lh, wh);

      int least = min(min(lw, lh), wh);

      int volume = l * w * h;
      int total = least + volume;
      if (DEBUG) printf("found volume %d least %d total %d\n", volume, least, total);

      result += total;
    } else {
      fputs("unexpected token\n", stderr);
    }
    token = strtok(NULL, delim); 
  }

  return result;
}

