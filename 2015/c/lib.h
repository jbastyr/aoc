#ifndef LIB_H
#define LIB_H

#include <stdio.h>

#define DEBUG 0

int min(int a, int b);
int max(int a, int b);

int sum(int a, int b);

typedef int (*reducerInt)(int, int);
typedef int (*dayFn)(char *);

typedef struct str {
  char *data;
  size_t len;
} str;

str s(char *input);

#endif
