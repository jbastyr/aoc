#include "days.h"
#include "lib.h"

int day8_part1(char *input) {
  str inStr = s(input);
  size_t total = 0;

  size_t index = 0;
  while (index <= inStr.len) {
    char curr = inStr.data[index];
    if (curr == '\n') {
      index++;
      continue;
    }

    if (curr == '\"') {
      total++;
    } else if (curr == '\\') {
      char next = inStr.data[index + 1];
      // skip the escape char, total will be incremented below
      if (next == '\\' || next == '\"') {
        total++;
        index++;
      } else if (next == 'x') {
        // assuming we will have valid excaped data after it
        total += 3;
        index += 3;
      } else {
        fputs("found unrecognized escape sequence: ", stderr);
        char err[3];
        snprintf(err, 3, "%c\n", next);
        fputs(err, stderr);
        return -1;
      }
    }

    index++;
  }

  return total;
}

int day8_part2(char *input) {
  str inStr = s(input);
  size_t total = 0;

  size_t index = 0;
  while (index <= inStr.len) {
    char curr = inStr.data[index];

    if (curr == '\n' || curr == '\0') {
      total += 2;
    } else if (curr == '\\' || curr == '\"') {
      total += 1;
    }

    index++;
  }

  return total;
}

