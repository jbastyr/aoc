int day1_part1(char *input) {
  // walk through input
  // on every (, add one to counter
  // on every ), remove one from counter
  // return result
  int index = 0;
  int result = 0;
  do {
    if (input[index] == '(') {
      result += 1;
    } else if (input[index] == ')') {
      result -= 1;
    }
  } while (input[index++]);

  return result;
}

int day1_part2(char *input) {
  // walk through input as above
  // as soon as result is negative, return i+1
  int index = 0;
  int result = 0;
  do {
    if (input[index] == '(') {
      result += 1;
    } else if (input[index] == ')') {
      result -= 1;
    }
    if (result < 0) {
      return index + 1;
    }
  } while (input[index++]);
  return -1;
}

