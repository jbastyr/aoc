#include "lib.h"
#include <string.h>

int min(int a, int b) {
  return a < b ? a : b;
}

int max(int a, int b) {
  return a < b ? b : a;
}

int sum(int a, int b) {
  return a + b;
}

str s(char *input) {
  return (str){
    .data = input,
    .len = strlen(input)
  };
}

