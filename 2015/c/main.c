#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "main.h"
#include "days.h"
#include "lib.h"

// this is probably not a great way to structure this lol
int main(void) {
//  day1();
//  day2();
//  day3();
//  day4();
//  day5();
//  day6();
//  day7();
  day8();
//  day9();
//  day10();
//  day11();
//  day12();
//  day13();
//  day14();
//  day15();
//  day16();
//  day17();
//  day18();
//  day19();
//  day20();
//  day21();
//  day22();
//  day23();
//  day24();
//  day25();
//  day26();
//  day27();
//  day28();
//  day29();
//  day30();
//  day31();
}

void day8() {
  printf("Running day 8\n");
  char *p1_inputs[5] = {"\"\"", "\"abc\"", "\"aaa\\\"aaa\"", "\"\\x27\"", "\"\"\n\"abc\"\n\"aaa\\\"aaa\"\n\"\\x27\""};
  int p1_expected[5] = {2, 2, 3, 5, 12};
  for (int i = 0; i < 5; i++) {
    int result = day8_part1(p1_inputs[i]);
    printf("[%d] expected: %d, actual: %d\n", i, p1_expected[i], result);
  }

  char *final = readFile("8_final.txt");
  int p1_result = day8_part1(final);
  printf("Part 1: %d\n", p1_result);

  char *p2_inputs[5] = {"\"\"", "\"abc\"", "\"aaa\\\"aaa\"", "\"\\x27\"", "\"\"\n\"abc\"\n\"aaa\\\"aaa\"\n\"\\x27\""};
  int p2_expected[5] = {4, 4, 6, 5, 19};
  for (int i = 0; i < 5; i++) {
    int result = day8_part2(p2_inputs[i]);
    printf("[%d] expected: %d, actual: %d\n", i, p2_expected[i], result);
  }

  int p2_result = day8_part2(final);
  printf("Part 2: %d\n", p2_result);
}

void day7() {
  printf("Running day 7\n");
  char *p1_inputs[6] = {"123 -> z", "5 -> x\nx LSHIFT 2 -> z", "123 -> x\nNOT x -> z", "123 -> x\n5 -> z\n", "123 -> x\n5 -> y\nx AND y -> z", "x OR y -> z\n123 -> x\n5 -> y"};
  int p1_expected[6] = {123, 20, 65412, 5, 1, 127};
  for (int i = 0; i < 6; i++) {
    int result = day7_part1(p1_inputs[i], "z");
    printf("[%d] expected: %d, actual: %d\n", i, p1_expected[i], result);
  }

  char *final = readFile("7_final.txt");
  int p1_result = day7_part1(final, "a");
  printf("Part 1: %d\n", p1_result);

  int p2_result = day7_part2(final, "a", "b");
  printf("Part 2: %d\n", p2_result);
}

void day6() {
  printf("Running day 6\n");
  char *p1_inputs[3] = {"turn on 0,0 through 999,999", "turn on 0,0 through 999,999\ntoggle 0,0 through 999,0", "turn on 0,0 through 999,999\ntoggle 0,0 through 999,0\nturn off 499,499 through 500,500"};
  int p1_expected[3] = {1000000, 999000, 998996};
  for (int i = 0; i < 3; i++) {
    int result = day6_part1(p1_inputs[i]);
    printf("[%d] expected: %d, actual: %d\n", i, p1_expected[i], result);
  }

  char *final = readFile("6_final.txt");
  int final_len = strlen(final);

  char *p1_final = malloc(final_len * sizeof(char) + 1);
  strncpy(p1_final, final, final_len);
  int p1_result = day6_part1(p1_final);
  printf("Part 1: %d\n", p1_result);
  free(p1_final);
  p1_final = NULL;

  char *p2_inputs[2] = {"turn on 0,0 through 0,0", "toggle 0,0 through 999,999"};
  int p2_expected[2] = {1, 2000000};
  for (int i = 0; i < 2; i++) {
    int result = day6_part2(p2_inputs[i]);
    printf("[%d] expected: %d, actual: %d\n", i, p2_expected[i], result);
  }

  char *p2_final = malloc((final_len + 1) * sizeof(char));
  strncpy(p2_final, final, final_len);
  int p2_result = day6_part2(p2_final);
  printf("Part 2: %d\n", p2_result);
  free(p2_final);
  p2_final = NULL;

}

void day5() {
  printf("Running day 5\n");
  int p1_count = 5;
  char *p1_inputs[5] = {"ugknbfddgicrmopn", "aaa", "jchzalrnumimnmhp", "haegwjzuvuyypxyu", "dvszwmarrgswjxmb"};
  int p1_expected[5] = {1, 1, 0, 0, 0};

  for (int i = 0; i < p1_count; i++) {
    int result = day5_part1(p1_inputs[i]);
    printf("[%d] expected: %d, actual: %d\n", i, p1_expected[i], result);
  }

  char *final = readFile("5_final.txt");
  int final_len = strlen(final);

  // copy input since strtok will eat it
  char *p1_final = malloc(sizeof(char) * final_len + 1);
  strncpy(p1_final, final, final_len);
  int p1_result = runMany(p1_final, day5_part1, sum, 0);
  printf("Part 1: %d\n", p1_result);
  free(p1_final);
  p1_final = NULL;


  int p2_count = 5;
  char *p2_inputs[5] = {"qjhvhtzxzqqjkmpb", "xxyxx", "uurcxstgmygtbstg", "ieodomkazucvgmuy", "aaa"};
  int p2_expected[5] = {1, 1, 0, 0, 0};

  for (int i = 0; i < p2_count; i++) {
    int result = day5_part2(p2_inputs[i]);
    printf("[%d] expected %d, actual %d\n", i, p2_expected[i], result);
  }

  char *p2_final = malloc(sizeof(char) * final_len + 1);
  strncpy(p2_final, final, final_len);
  int p2_result = runMany(p2_final, day5_part2, sum, 0);
  printf("Part 2: %d\n", p2_result);
  free(p2_final);
  p2_final = NULL;

}

void day4() {
  printf("Running day 4\n");
  int p1_count = 2;
  char *p1_inputs[2] = {"abcdef", "pqrstuv"};
  int p1_expected[2] = {609043, 1048970};

  for (int i = 0; i < p1_count; i++) {
    int result = day4_part1(p1_inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, p1_expected[i], result);
  }

  char *final = readFile("4_final.txt");
  int p1_result = day4_part1(final);
  printf("Part 1: %d\n", p1_result);

  int p2_result = day4_part2(final);
  printf("Part 2: %d\n", p2_result);
}

void day3(void) {
  printf("Running day 3\n");
  int p1_count = 3;
  char *p1_inputs[3] = {">", "^>v<", "^v^v^v^v^v"};
  int p1_expected[3] = {2, 4, 2};

  for (int i = 0; i < p1_count; i++) {
    int result = day3_part1(p1_inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, p1_expected[i], result);
  }

  char *final = readFile("3_final.txt");
  int p1_result = day3_part1(final);
  printf("Part 1: %d\n", p1_result);

  int p2_count = 3;
  char *p2_inputs[3] = {"^v", "^>v<", "^v^v^v^v^v"};
  int p2_expected[3] = {3, 3, 11};

  for (int i = 0; i < p2_count; i++) {
    int result = day3_part2(p2_inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, p2_expected[i], result);
  }

  int p2_result = day3_part2(final);
  printf("Part 2: %d\n", p2_result);
}

void day2(void) {
  printf("Running day 2\n");
  int p1_count = 2;
  char *inputs[2] = {"2x3x4", "1x1x10"};
  int expected[2] = {58, 43};

  for (int i = 0; i < p1_count; i++) {
    int result = day2_part1(inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, expected[i], result);
  }

  char *final = readFile("2_final.txt");
  int final_len = strlen(final);
  printf("final_len %d\n", final_len);

  // have to copy the input because strtok eats it when converting elim to \0
  //    maybe need simple simple tokenizer instead that doesn't modify
  char *p1_final = malloc(sizeof(char) * final_len + 1);
  strncpy(p1_final, final, final_len);
  int result = day2_part1(p1_final);
  printf("Part 1: %d\n", result);
  free(p1_final);
  p1_final = NULL;

  int p2_count = 2;
  char *p2_inputs[2] = {"2x3x4", "1x1x10"};
  int p2_expected[2] = {34, 14};

  for (int i = 0; i < p2_count; i++) {
    int result = day2_part2(p2_inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, p2_expected[i], result);
  }

  char *p2_final = malloc(sizeof(char) * final_len + 1);
  strncpy(p2_final, final, final_len);
  result = day2_part2(p2_final);
  printf("Part 2: %d\n", result);
  free(p2_final);
  p2_final = NULL;

  free(final);
}

void day1(void) {
  printf("Running day 1\n");
  int p1_count = 9;
  char *inputs[9] = {"(())", "()()", "(((", "(()(()(", "))(((((", "())", "))(", ")))", ")())())"};
  int expected[9] = {0, 0, 3, 3, 3, -1, -1, -3, -3};

  for (int i = 0; i < p1_count; i++) {
    int result = day1_part1(inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, expected[i], result);
  }

  char *final = readFile("1_final.txt");

  int result = day1_part1(final);
  printf("Part 1: %d\n", result);

  int p2_count = 2;
  char *p2_inputs[2] = {")", "()())"};
  int p2_expected[2] = {1, 5};

  for (int i = 0; i < p2_count; i++) {
    int result = day1_part2(p2_inputs[i]);
    printf("index %d, expected: %d, actual: %d\n", i, p2_expected[i], result);
  }

  result = day1_part2(final);
  printf("Part 2: %d\n", result);

  free(final);
}

int runMany(char *input, dayFn day, reducerInt reducer, int init) {
  if (DEBUG) printf("input %s\n", input);
  int final_result = init;
   
  const char *delim = "\n";
  char *token = NULL;
  token = strtok(input, delim);
  while(token != NULL) {
    int result = day(token);
    if (DEBUG) printf("parsed %s result %d\n", token, result);
    final_result = reducer(final_result, result);

    // advance to next token
    token = strtok(NULL, delim); 
    if (DEBUG) printf("next token %s\n", token);
  }

  return final_result;
}

char *readFile(char *fileName) {
  char path[32];
  snprintf(path, sizeof(path), "../input/%s", fileName);
  printf("%s\n", path);

  char *source = NULL;
  FILE *file = fopen(path, "r");
  if (file != NULL) {
    if (fseek(file, 0L, SEEK_END) == 0) {
      long fileSize = ftell(file);
      if (fileSize == -1) {
        fputs("Error getting file size\n", stderr);
        fclose(file);
        return NULL;
      }

      source = malloc(sizeof(char) * (fileSize + 1));

      if (fseek(file, 0L, SEEK_SET) != 0) {
        fputs("Error returning to file start", stderr);
        fclose(file);
        free(source);
        return NULL;
      }

      size_t newLength = fread(source, sizeof(char), fileSize, file);

      if (ferror(file) != 0) {
        fputs("Error reading file\n", stderr);
        fclose(file);
        free(source);
        return NULL;
      } else {
        source[newLength++] = '\0';

        // strip newline if it is at the end of the file
        //    some inputs have it, most don't
        int index = newLength;
        while (index > 0 && source[index--] == '\0') {
          if (source[index] == '\n') {
            source[index] = '\0';
            break;
          }
        }
      }
    }
    fclose(file);
    return source;
  }
  fputs("Error opening file\n", stderr);
  return NULL;
}

