#include "lib.h"
#include "days.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef enum OperandType {
  LITERAL,
  VARIABLE,
} OperandType;

typedef struct Operand {
  union {
    char variable[4];
    unsigned short value;
  };
  OperandType type;
} Operand;

typedef enum Operator {
  ASSIGN,
  NOT,
  OR,
  AND,
  RSHIFT,
  LSHIFT,
} Operator;

typedef struct Result {
  char variable[4];
  unsigned short value;
} Result;

typedef struct Operation {
  struct {
    char variable[4];
    unsigned short value;
  } result;
  Operand left;
  Operand right;
  Operator op;
} Operation;

typedef struct ParsedOperations {
  Operation *ops;
  size_t count;
} ParsedOperations;

int sortOperations(const void *a, const void *b) {
  const char *varA = ((const Operation *)a)->result.variable;
  const char *varB = ((const Operation *)b)->result.variable;
  
  size_t lenA = strlen(varA);
  size_t lenB = strlen(varB);

  if (lenA != lenB) {
    return lenA - lenB;
  }

  return strncmp(
    varA,
    varB,
    4
  );
}

void printOperation(const Operation *op) {
  char *operator;
  switch (op->op) {
    case ASSIGN: operator = "ASSIGN"; break;
    case NOT: operator = "NOT"; break;
    case OR: operator = "OR"; break;
    case AND: operator = "AND"; break;
    case RSHIFT: operator = "RSHIFT"; break;
    case LSHIFT: operator = "LSHIFT"; break;
  }

  char left[32];
  switch (op->op) {
    case OR:
    case AND:
    case RSHIFT:
    case LSHIFT:
      switch (op->left.type) {
        case VARIABLE: snprintf(left, 32, "%s", op->left.variable); break;
        case LITERAL: snprintf(left, 32, "%d", op->left.value); break;
      };
      break;
    default:
      snprintf(left, 32, "");
  }

  char right[32];
  switch (op->right.type) {
    case VARIABLE: snprintf(right, 32, "%s", op->right.variable); break;
    case LITERAL: snprintf(right, 32, "%d", op->right.value); break;
  }


  printf("%s %s %s -> %s (%d)\n", left, operator, right, op->result.variable, op->result.value);
}

size_t scanIndexOf(ParsedOperations *parsed, char *variable) {
  size_t lastIndex = -1;
  for (size_t i = 0; i < parsed->count; i++) {
    if (strncmp(variable, parsed->ops[i].result.variable, strlen(variable)) == 0 && strlen(variable) == strlen(parsed->ops[i].result.variable)) {
      if (DEBUG) printf("found index %ld for %s\n", i, variable);
      lastIndex = i;
    }
  }
  return lastIndex;
}

ParsedOperations parseInput(char *input) {
  str inStr = s(input);
  str buff = { .data = (char[32]){0}, .len = 0 };
  size_t index = 0;

  Operation ops[1024];
  size_t opsCount = 0;

  if (DEBUG) printf("input len %ld\n", inStr.len);

  while (index <= inStr.len) {
    if (DEBUG) printf("reading %c\n", inStr.data[index]);
    if (inStr.data[index] == '\n' || inStr.data[index] == '\0') {
      if (buff.len <= 0) {
        if (DEBUG) printf("breaking, buff is empty and at newline\n");
        break;
      }

      if (DEBUG) printf("parsing operation [%s]\n", buff.data);

      Operation op = {};

      char operator[8];
      char leftMember[4];
      char rightMember[4];
      char resultVariable[4];

      if (sscanf(buff.data, "%s -> %s", rightMember, resultVariable) == 2) {
        op.op = ASSIGN;

        unsigned short value;
        if (sscanf(rightMember, "%hd", &value) == 1) {
          op.right = (Operand){ .type = LITERAL, .value = value };
        } else {
          op.right = (Operand){ .type = VARIABLE };
          strncpy(op.right.variable, rightMember, sizeof(rightMember));
        }

        strncpy(op.result.variable, resultVariable, sizeof(resultVariable));
        ops[opsCount++] = op;
      } else if (sscanf(buff.data, "NOT %s -> %s", rightMember, resultVariable) == 2) {
        op.op = NOT;

        unsigned short value;
        if (sscanf(rightMember, "%hd", &value) == 1) {
          op.right = (Operand){ .type = LITERAL, .value = value };
        } else {
          op.right = (Operand){ .type = VARIABLE };
          strncpy(op.right.variable, rightMember, sizeof(rightMember));
        }
        strncpy(op.result.variable, resultVariable, sizeof(resultVariable));
        ops[opsCount++] = op;
      } else if (sscanf(buff.data, "%s %s %s -> %s", leftMember, operator, rightMember, resultVariable)) {
        if (strncmp(operator, "OR", 2) == 0) {
          op.op = OR;
        } else if (strncmp(operator, "AND", 3) == 0) {
          op.op = AND;
        } else if (strncmp(operator, "RSHIFT", 6) == 0) {
          op.op = RSHIFT;
        } else if (strncmp(operator, "LSHIFT", 6) == 0) {
          op.op = LSHIFT;
        } else {
          fputs("Unexpected operator: ", stderr);
          fputs(operator, stderr);
          fputs("\n", stderr);
          return (ParsedOperations){ .count = 0 };
        }

        unsigned short value;
        if (sscanf(leftMember, "%hd", &value) == 1) {
          op.left = (Operand){ .type = LITERAL, .value = value };
        } else {
          op.left = (Operand){ .type = VARIABLE };
          strncpy(op.left.variable, leftMember, sizeof(leftMember));
        }

        if (sscanf(rightMember, "%hd", &value) == 1) {
          op.right = (Operand){ .type = LITERAL, .value = value };
        } else {
          op.right = (Operand){ .type = VARIABLE };
          strncpy(op.right.variable, rightMember, sizeof(rightMember));
        }

        strncpy(op.result.variable, resultVariable, sizeof(resultVariable));
        ops[opsCount++] = op;
      } else {
        fputs("Unexpected input: ", stderr);
        fputs(buff.data, stderr);
        fputs("\n", stderr);
        return (ParsedOperations){ .count = 0 };
      }
      if (DEBUG) printf("found op %d\n", op.op);

      // reset buffer length and advance past the newline
      buff = (str){ .data = (char[32]){0}, .len = 0 };
      index++;
    } else {
      // push the current char into the buffer and advance both indexes
      buff.data[buff.len++] = inStr.data[index++];
    }
  }

  return (ParsedOperations){
    .ops = ops,
    .count = opsCount,
  };
}

void evaluateOperations(ParsedOperations *parsed) {
  if (DEBUG) printf("evaluating all-------------------\n");
  for (size_t i = 0; i < parsed->count; i++) {
    Operation *op = &parsed->ops[i];

    if (DEBUG) printf("\tleft %d %s %d\n", op->left.type, op->left.variable, parsed->ops[scanIndexOf(parsed, op->left.variable)].result.value);
    if (DEBUG) printf("\tright %d %s %d\n", op->right.type, op->right.variable, parsed->ops[scanIndexOf(parsed, op->right.variable)].result.value);

    unsigned short leftValue;
    if (op->left.type == VARIABLE) {
      leftValue = parsed->ops[scanIndexOf(parsed, op->left.variable)].result.value;
    } else {
      leftValue = op->left.value;
    }

    unsigned short rightValue;
    if (op->right.type == VARIABLE) {
      rightValue = parsed->ops[scanIndexOf(parsed, op->right.variable)].result.value;
    } else {
      rightValue = op->right.value;
    }

    switch (op->op) {
      case ASSIGN: {
        if (DEBUG) printf("%s = %d\n", op->result.variable, rightValue);
        op->result.value = rightValue;
      } break;
      case NOT: {
        if (DEBUG) printf("%s = ~%d\n", op->result.variable, rightValue);
        op->result.value = ~rightValue;
      } break;
      case OR: {
        if (DEBUG) printf("%s = %d | %d\n", op->result.variable, leftValue, rightValue);
        op->result.value = leftValue | rightValue;
      } break;
      case AND: {
        if (DEBUG) printf("%s = %d & %d\n", op->result.variable, leftValue, rightValue);
        op->result.value = leftValue & rightValue;
      } break;
      case RSHIFT: {
        if (DEBUG) printf("%s = %d >> %d\n", op->result.variable, leftValue, rightValue);
        op->result.value = leftValue >> rightValue;
      } break;
      case LSHIFT: {
        if (DEBUG) printf("%s = %d << %d\n", op->result.variable, leftValue, rightValue);
        op->result.value = leftValue << rightValue;
      } break;
    }
  }
  if (DEBUG) printf("done evaluating-------------------\n");

  for (size_t i = 0; i < parsed->count; i++) {
    if (DEBUG) printOperation(&parsed->ops[i]);
  }
}

// make sure target is the very last step
//    could avoid this if we built a tree of dependent calculations probably
void bubbleTarget(ParsedOperations *parsed, char *target) {
  size_t targetIndex = scanIndexOf(parsed, target);
  Operation targetOp = parsed->ops[targetIndex];
  for (size_t i = targetIndex + 1; i < parsed->count; i++) {
    parsed->ops[i - 1] = parsed->ops[i];
  }
  parsed->ops[parsed->count - 1] = targetOp;
}

// find and update target assignment with new value
void overrideAssignment(ParsedOperations *parsed, char *overrideTarget, unsigned short overrideValue) {
  size_t targetIndex = scanIndexOf(parsed, overrideTarget);
  Operation *target = &parsed->ops[targetIndex];
  target->right.value = overrideValue;
}

// every result.value gets reset to 0
void resetOperations(ParsedOperations *parsed) {
  for (size_t i = 0; i < parsed->count; i++) {
    parsed->ops[i].result.value = 0;
  }
}

int day7_part1(char *input, char *target) {
  ParsedOperations parsed = parseInput(input);

  // sort the operations by the result variable name
  qsort(parsed.ops, parsed.count, sizeof(Operation), sortOperations);
  
  bubbleTarget(&parsed, target);

  evaluateOperations(&parsed);

  return parsed.ops[scanIndexOf(&parsed, target)].result.value;
}

int day7_part2(char *input, char *target, char *overrideTarget) {
  ParsedOperations parsed = parseInput(input);

  // sort the operations by the result variable name
  qsort(parsed.ops, parsed.count, sizeof(Operation), sortOperations);
  
  bubbleTarget(&parsed, target);

  evaluateOperations(&parsed);

  unsigned short initValue = parsed.ops[scanIndexOf(&parsed, target)].result.value;

  overrideAssignment(&parsed, overrideTarget, initValue);

  resetOperations(&parsed);

  evaluateOperations(&parsed);

  return parsed.ops[scanIndexOf(&parsed, target)].result.value;
}

