#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "lib.h"


int day3_part1(char *input) {
  int length_input = strlen(input);
  if (DEBUG) printf("found %d steps\n", length_input);
  
  short *visited_x = malloc(sizeof(short) * length_input + 1); // horizontal
  short *visited_y = malloc(sizeof(short) * length_input + 1); // vertical
  // add initial location
  visited_x[0] = 0;
  visited_y[0] = 0;

  int index = 0;
  int x_curr = 0, y_curr = 0;
  int visited = 1;
  char step;
  while ((step = input[index++]) != '\0') {
    // get next location
    switch (step) {
      case '>': x_curr++; break;
      case '<': x_curr--; break;
      case '^': y_curr++; break;
      case 'v': y_curr--; break;
    }

    // check if location already visited (n^2 :/)
    int seen = 0;
    for (int i = 0; i < visited; i++) {
      if (visited_x[i] == x_curr && visited_y[i] == y_curr) {
        seen = 1;
        break;
      }
    }

    // if not, add and increment total visted
    if (!seen) {
      visited_x[visited] = x_curr;
      visited_y[visited] = y_curr;
      visited++;
    }
  }

  free(visited_x);
  free(visited_y);

  return visited;
}

int day3_part2(char *input) {
  int length_input = strlen(input);
  if (DEBUG) printf("found %d steps\n", length_input);
  
  short *visited_x = malloc(sizeof(short) * length_input + 1); // horizontal
  short *visited_y = malloc(sizeof(short) * length_input + 1); // vertical
  // add initial location
  visited_x[0] = 0;
  visited_y[0] = 0;

  int index = 0;
  int x_curr = 0, y_curr = 0, rx_curr = 0, ry_curr = 0;
  int r_turn = 0;
  int visited = 1;
  char step;

  while ((step = input[index++]) != '\0') {
    // get next location
    switch (step) {
      case '>': r_turn ? rx_curr++ : x_curr++; break;
      case '<': r_turn ? rx_curr-- : x_curr--; break;
      case '^': r_turn ? ry_curr++ : y_curr++; break;
      case 'v': r_turn ? ry_curr-- : y_curr--; break;
    }

    // check if location already visited (n^2 :/)
    int seen = 0;
    for (int i = 0; i < visited; i++) {
      if (visited_x[i] == (r_turn ? rx_curr : x_curr) && visited_y[i] == (r_turn ? ry_curr : y_curr)) {
        seen = 1;
        break;
      }
    }

    // if not, add and increment total visted
    if (!seen) {
      visited_x[visited] = (r_turn ? rx_curr : x_curr);
      visited_y[visited] = (r_turn ? ry_curr: y_curr);
      visited++;
    }

    // switch to robo and real santa after each step
    r_turn = !r_turn;
  }

  free(visited_x);
  free(visited_y);

  return visited;
}

