#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "openssl/md5.h"

#include "days.h"
#include "lib.h"

void md5(const char *str, unsigned char *digest);
void print_md5(unsigned char *digest);
int starts_with(unsigned char *digest, unsigned char *target);

int day4_part1(char *input) {
  unsigned char digest[MD5_DIGEST_LENGTH];
  int suffix = 0;
  int match_found = 0;

  size_t max_len = (strlen(input) + 20) + 1;

  char *new_input = malloc(max_len * sizeof(char));

  do {
    snprintf(new_input, max_len, "%s%d", input, suffix);

    md5(new_input, digest);
    if (digest[0] == 0 && digest[1] == 0 && digest[2] < 15) {
      match_found = 1;
    }

    if (DEBUG) {
      if (suffix % 100000 == 0) {
        printf("latest [%s]", new_input);
        print_md5(digest);
      }

      if (suffix == 609043 || suffix == 1048970) {
        printf("[%s | %d]: %d %d %d \n", new_input, suffix, digest[0], digest[1], digest[2]);
        print_md5(digest);
      }
    }

    // some upper limit to not infinitely search
    if (suffix > 10000000) {
      return 0;
    }

    if (!match_found) {
      suffix++;
    }

  } while(!match_found);

  print_md5(digest);
  free(new_input);

  return suffix;
}

int day4_part2(char *input) {
  unsigned char digest[MD5_DIGEST_LENGTH];
  int suffix = 0;
  int match_found = 0;

  size_t max_len = (strlen(input) + 20) + 1;

  char *new_input = malloc(max_len * sizeof(char));

  do {
    snprintf(new_input, max_len, "%s%d", input, suffix);

    md5(new_input, digest);
    if (digest[0] == 0 && digest[1] == 0 && digest[2] == 0) {
      match_found = 1;
    }

    if (DEBUG) {
      if (suffix % 10000000 == 0) {
        printf("latest [%s]", new_input);
        print_md5(digest);
      }
    }

    // some upper limit to not infinitely search
    if (suffix > 100000000) {
      return 0;
    }

    if (!match_found) {
      suffix++;
    }

  } while(!match_found);

  print_md5(digest);
  free(new_input);

  return suffix;
}

void md5(const char *str, unsigned char *digest) {
  MD5((unsigned char *)str, strlen(str), digest);
}

void print_md5(unsigned char *digest) {
  for (int i = 0; i < MD5_DIGEST_LENGTH; i++) {
    printf("%02x", digest[i]);
  }
  printf("\n");
}
