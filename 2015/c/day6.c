#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "lib.h"
#include "days.h"

int day6_part1(char *input) {
  char *inputCopy = calloc(strlen(input), sizeof(char) + 1);
  strncpy(inputCopy, input, strlen(input));
  

  int **grid = calloc(1000, sizeof(int *));
  for (int i = 0; i < 1000; i++) {
    grid[i] = calloc(1000, sizeof(int));
  }

  const char *delim = "\n";
  char *token = NULL;

  int x1, y1, x2, y2;
  token = strtok(inputCopy, delim);
  while (token != NULL) {
    if (DEBUG) printf("parsing %s\n", token);
    if (sscanf(token, "turn on %d,%d through %d,%d", &x1, &y1, &x2, &y2) == 4) {
      for (int i = x1; i <= x2; i++) {
        for (int j = y1; j <= y2; j++) {
          grid[i][j] = 1;
        }
      }
    } else if (sscanf(token, "turn off %d,%d through %d,%d", &x1, &y1, &x2, &y2) == 4) {
      for (int i = x1; i <= x2; i++) {
        for (int j = y1; j <= y2; j++) {
          grid[i][j] = 0;
        }
      }
    } else if (sscanf(token, "toggle %d,%d through %d,%d", &x1, &y1, &x2, &y2) == 4) {
      for (int i = x1; i <= x2; i++) {
        for (int j = y1; j <= y2; j++) {
          grid[i][j] = !grid[i][j];
        }
      }
    } else {
      fputs("unexpected token\n", stderr);
    }
    token = strtok(NULL, delim);
  }

  int result = 0;
  for (int i = 0; i < 1000; i++) {
    for (int j = 0; j < 1000; j++) {
      result += grid[i][j];
    }
  }

  for (int i = 0; i < 1000; i++) {
    free(grid[i]);
  }
  free(grid);

  free(inputCopy);

  return result;
}

int day6_part2(char *input) {
  char *inputCopy = calloc(strlen(input), sizeof(char) + 1);
  strncpy(inputCopy, input, strlen(input));
  

  int **grid = calloc(1000, sizeof(int *));
  for (int i = 0; i < 1000; i++) {
    grid[i] = calloc(1000, sizeof(int));
  }

  const char *delim = "\n";
  char *token = NULL;

  int x1, y1, x2, y2;
  token = strtok(inputCopy, delim);
  while (token != NULL) {
    if (DEBUG) printf("parsing %s\n", token);
    if (sscanf(token, "turn on %d,%d through %d,%d", &x1, &y1, &x2, &y2) == 4) {
      for (int i = x1; i <= x2; i++) {
        for (int j = y1; j <= y2; j++) {
          grid[i][j] += 1;
        }
      }
    } else if (sscanf(token, "turn off %d,%d through %d,%d", &x1, &y1, &x2, &y2) == 4) {
      for (int i = x1; i <= x2; i++) {
        for (int j = y1; j <= y2; j++) {
          grid[i][j] = max(0, grid[i][j] - 1);
        }
      }
    } else if (sscanf(token, "toggle %d,%d through %d,%d", &x1, &y1, &x2, &y2) == 4) {
      for (int i = x1; i <= x2; i++) {
        for (int j = y1; j <= y2; j++) {
          grid[i][j] += 2;
        }
      }
    } else {
      fputs("unexpected token\n", stderr);
    }
    token = strtok(NULL, delim);
  }

  int result = 0;
  for (int i = 0; i < 1000; i++) {
    for (int j = 0; j < 1000; j++) {
      if (DEBUG) printf("%d ", grid[i][j]);
      result += grid[i][j];
    }
    if (DEBUG) printf("\n");
  }

  for (int i = 0; i < 1000; i++) {
    free(grid[i]);
  }
  free(grid);

  free(inputCopy);

  return result;
}
