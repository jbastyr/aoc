#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "days.h"
#include "lib.h"

int day5_part1(char *input) {
  char vowels[5] = {'a', 'e', 'i', 'o', 'u'};
  char *blacklist[4] = {"ab", "cd", "pq", "xy"};
  char previous_char = 0;
  int vowel_count = 0, has_double = 0;

  int index = 0;
  char curr_char;
  if (DEBUG) printf("testing %s\n", input);
  while ((curr_char = input[index]) != '\0') {
    // previous_char will be set
    if (index > 0) {
      if (!has_double && previous_char == curr_char) {
        has_double = 1;
      }

      for (int i = 0; i < 4; i++) {
        // if (DEBUG) printf("comparing %c%c to %c%c\n", blacklist[i][0], blacklist[i][1], previous_char, curr_char);
        if (blacklist[i][0] == previous_char && blacklist[i][1] == curr_char) {
          // exit early for found blacklist entry
          return 0;
        }
      }
    }

    if (vowel_count < 3) {
      for (int i = 0; i < 5; i++) {
        if (vowels[i] == curr_char) {
          vowel_count++;
          break;
        }
      }
    }

    previous_char = curr_char;
    index++;
  }

  if (DEBUG) printf("vowels %d, has_double %d\n", vowel_count, has_double);
  return vowel_count >= 3 && has_double;
}

int day5_part2(char *input) {
  size_t input_length = strlen(input);
  size_t num_pairs = input_length - 1;
  size_t size_pairs = num_pairs * 2;
  // could malloc char **, but keeping them in pairs is easy enough by pos 
  char *seen_pairs = malloc((size_pairs + 1) * sizeof(char));
  int seen_pairs_len = 0, has_previous_pair = 0, has_split_repeat = 0;

  char previous_chars[2];

  int index = 0;
  char curr_char;
  if (DEBUG) printf("testing %s\n", input);
  while((curr_char = input[index]) != '\0') {
    if (index > 0 && !has_previous_pair) {
      // check if any other pair matches prev, curr
      // push prev, curr into list
      // len - 1 because we need to skip the very last pair since we dont want to overlap eg 'aaa' is not a match since the 'aa' and 'aa' overlap
      for (int i = 0; i < seen_pairs_len - 1; i++) {
        if (seen_pairs[i + 0] == previous_chars[0] && seen_pairs[i + 1] == curr_char) {
          has_previous_pair = 1;
          break;
        }
      }

      seen_pairs[seen_pairs_len + 0] = previous_chars[0];
      seen_pairs[seen_pairs_len + 1] = curr_char;
      seen_pairs_len++;
    }

    if (index > 1 && !has_split_repeat) {
      // check if previous_chars[1] == curr_char
      if (previous_chars[1] == curr_char) {
        has_split_repeat = 1;
      }
    }

    // push back previous onto previous-previous
    if (index > 0) previous_chars[1] = previous_chars[0];
    // push back current onto previous
    previous_chars[0] = curr_char;
    index++;
  }

  return has_previous_pair && has_split_repeat;
}


