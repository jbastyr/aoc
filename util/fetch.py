#!/usr/bin/python3

import configparser
import argparse
import sys
import os
import requests

parser = argparse.ArgumentParser(
    description='Fetch AOC input per year and day')
parser.add_argument('year', type=int, help='Year')
parser.add_argument('day', type=int, help='Day')
args = parser.parse_args()

if args.year <= 2014 or args.year > 2024:
    print('Year must be between 2015 and 2024 inclusive')
    sys.exit(1)

if args.day <= 0 or args.day > 31:
    print('Day must be between 1 and 31 inclusive')
    sys.exit(1)

script_dir = os.path.dirname(os.path.abspath(__file__))
config_path = os.path.join(script_dir, 'config.ini')

config = configparser.ConfigParser()
config_files = config.read(config_path)

if not config_files:
    print('config.ini does not exist, copy from config.ini.example first')
    sys.exit(1)

if not config.has_section('Session') or not config.has_option('Session', 'token'):
    print('Session and token not defined in config.ini')
    sys.exit(1)

session_token = config['Session']['token']

if not session_token or session_token == '01234567890abcdef':
    print('Session token appears to be missing or invalid, ensure it is updated from the AOC site')
    sys.exit(1)

url = f'https://adventofcode.com/{args.year}/day/{args.day}/input'

input_relative_path = f'../{args.year}/input/{args.day}_final.txt'
input_target_path = os.path.join(script_dir, input_relative_path)

os.makedirs(os.path.dirname(input_target_path), exist_ok=True)

response = requests.get(url, cookies={'session': session_token})
if response.status_code == 200:
    with open(input_target_path, 'w', encoding='utf-8') as file:
        file.write(response.text)
    print(f'Saved data to {input_target_path}')
    sys.exit(0)
else:
    print(f'Failed to fetch {url} [{response.status_code}]')
    sys.exit(1)
