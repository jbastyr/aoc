import { group, sum } from "../util/arr.ts";
import { doAlways, Input, NestedSectionParser, Parser, Section, Take, Unchanged } from "../util/input.ts";
import { readLines } from "../util/read.ts";

enum Strategy {
  SINGLE,
  TRIPLE
}

const letters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

const getScore = (letter: string): number => {
  return letters.indexOf(letter) + 1;
}

const findCommonLetter = (words: string[]): string => {
  const findCommonLetters = (first: string, second: string): string => {
    let common = '';
    first.split('').forEach(c => {
      // console.log(`finding ${c} in ${second}`);
      if (second.includes(c) && !common.includes(c)) {
        // console.log('found');
        common = common.concat(c);
      }
    });
    // console.log(`common: ${common}`);
    return common;
  }

  // console.log(words);

  return words.reduce((acc, cur) => findCommonLetters(acc, cur));
}

const findCommonLetterScores = async (file: string, strategy: Strategy): Promise<number> => {
  const input = new Input(await readLines(file))
    .parseSection(
      new Section<string[]>(
        <Parser<string[]>>{
          parse: Unchanged
        },
        doAlways
      )
    );
  
  const comparisonSets: string[][] =
          strategy === Strategy.SINGLE ? 
          input.map(pack => [pack.substring(0, pack.length/2), pack.substring(pack.length / 2)])
          : group(input, 3);

  // console.log(input);
  // console.log(comparisonSets)
  
  return sum(comparisonSets.map(findCommonLetter).map(getScore));
}

console.log(`expected: 157, actual: ${await findCommonLetterScores('given.txt', Strategy.SINGLE)}`);
console.log(`chall: ${await findCommonLetterScores('input.txt', Strategy.SINGLE)}`);

console.log(`expected: 70, actual: ${await findCommonLetterScores('given.txt', Strategy.TRIPLE)}`);
console.log(`chall: ${await findCommonLetterScores('input.txt', Strategy.TRIPLE)}`);