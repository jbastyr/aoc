import { readLines } from "../util/read.ts";

const something = (data: string[]): unknown => {
  
}

console.log(`expected: 0, actual: ${something(await readLines('given.txt'))}`);
console.log(`chall: ${something(await readLines('input.txt'))}`);