import { readLines } from "../util/read.ts";
import { Stack } from "../util/coll.ts";

interface Move {
  src: number,
  dest: number,
  count: number
}

const moveItemsReverseOrder = (src: Stack<string>, dest: Stack<string>, count: number): void => {
  for (let i = 0; i < count; i++) {
    dest.push(src.next());
  }
}

const moveItemsInOrder = (src: Stack<string>, dest: Stack<string>, count: number) => {
  const itemsToMove: string[] = [];
  for(let i = 0; i < count; i++) {
    itemsToMove.unshift(src.next());
  }
  itemsToMove.forEach(item => dest.push(item));
}

const readStacks = (lines: string[]): Stack<string>[] => {
  const stackCount = (lines[0].length + 1) / 4;
  console.log('stack count', stackCount);

  const stacks: Stack<string>[] = [];

  for (let i = 0; i < lines.length; i++) {
    // console.log('in i');
    for (let j = 0; j < stackCount; j++) {
      if (stacks.length <= j) {
        // console.log('push new stack', j);
        stacks.push(new Stack<string>());
      }
      
      // console.log('in j');
      const val = lines[i][1+j*4];
      // console.log('val: ', val);
      if (val === ' ') {
        continue;
      }

      stacks[j].push(val);
    }
  }

  return stacks;
}

const printStacks = (stacks: Stack<string>[]): void => {
  let i = 1;
  stacks.forEach(s => console.log(`${i++}: ${s.print()}`)); 
}

const moveCrates = async (input: string, performMove: (src: Stack<string>, dest: Stack<string>, count: number) => void): Promise<string> => {
  const data = await readLines(input);

  let stacksRead = false;
  const stackLines: string[] = [];
  const moves: Move[] = [];

  data.forEach(line => {
    if (line.length === 0) {
      return;
    }

    if (!stacksRead) {
      if (!line.includes('[')) {
        stacksRead = true;
        return;
      }

      stackLines.unshift(line);
      return;
    }
    const nums = [...line.matchAll(/[0-9]+/g)].map(e => parseInt(e.toString()));

    moves.push({src: nums[1], dest: nums[2], count: nums[0]});
  });

  const stacks = readStacks(stackLines);

  console.log('read');
  printStacks(stacks);

  moves.forEach(move => {
    // console.log(`moving ${move.count} from ${move.src} to ${move.dest}`);
    performMove(stacks[move.src-1], stacks[move.dest-1], move.count);
  });
  console.log('moved');
  printStacks(stacks);

  return stacks.map(s => s.peek()).reduce((acc, cur) => acc.concat(cur));
}

console.log(`expected: CMZ, actual: ${await moveCrates('given.txt', moveItemsReverseOrder)}`);
console.log(`chall: ${await moveCrates('input.txt', moveItemsReverseOrder)}`);

console.log(`expected: MCD, actual: ${await moveCrates('given.txt', moveItemsInOrder)}`);
console.log(`chall: ${await moveCrates('input.txt', moveItemsInOrder)}`);

