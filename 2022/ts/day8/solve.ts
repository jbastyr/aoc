import { readLines } from "../util/read.ts";
import { desc } from "../util/arr.ts";

const findVisibleTrees = async (input: string): Promise<string> => {
  const data = await readLines(input);
  const trees: number[][] = [];
  const viewScores: number[] = [];
  let totalVisible = 0;

  data.forEach(line => {
    const treeRow = line.split('').map(t => parseInt(t));
    trees.push(treeRow);
  });

  // todo:jeremy instead of n*m*(m+n), consider passing only once
  //    over left,right and top,bottom. keep track of steps up and down
  for (let i = 0; i < trees.length; i++) {
    for (let j = 0; j < trees[0].length; j++) {
      const currentTree = trees[i][j];

      let leftVisible = true;
      let leftViewScore = 0;
      for (let left = j-1; left >= 0; left--) {
        leftViewScore += 1;
        if (trees[i][left] >= currentTree) {
          // console.log(`found left blocking tree for ${i},${j}:${currentTree} at ${i},${left}:${trees[i][left]}`);
          leftVisible = false;
          break;
        }
      } 

      let rightVisible = true;
      let rightViewScore = 0;
      for (let right = j+1; right < trees[0].length; right++) {
        rightViewScore += 1;
        if (trees[i][right] >= currentTree) {
          // console.log(`found right blocking tree for ${i},${j}:${currentTree} at ${i},${right}:${trees[i][right]}`);
          rightVisible = false;
          break;
        }
      }

      let topVisible = true;
      let topViewScore = 0;
      for (let top = i-1; top >= 0; top--) {
        topViewScore += 1;
        if (trees[top][j] >= currentTree) {
          // console.log(`found top blocking tree for ${i},${j}:${currentTree} at ${top},${j}:${trees[top][j]}`);
          topVisible = false;
          break;
        }
      }

      let bottomVisible = true;
      let bottomViewScore = 0;
      for (let bottom = i+1; bottom < trees.length; bottom++) {
        bottomViewScore += 1;
        if (trees[bottom][j] >= currentTree) {
          // console.log(`found bottom blocking tree for ${i},${j}:${currentTree} at ${bottom},${j}:${trees[bottom][j]}`);
          bottomVisible = false;
          break;
        }
      }

      if (leftVisible || rightVisible || topVisible || bottomVisible) {
        totalVisible += 1;
      }

      // console.log(leftViewScore, rightViewScore, topViewScore, bottomViewScore)
      viewScores.push(leftViewScore * rightViewScore * topViewScore * bottomViewScore);
    }
  }

  return `${totalVisible}, ${viewScores.sort(desc)[0]}`;
}

console.log(`expected: 21, 8. actual: ${await findVisibleTrees('given.txt')}`);
console.log(`chall: ${await findVisibleTrees('input.txt')}`);
