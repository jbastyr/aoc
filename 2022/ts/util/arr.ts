export const asc = (a: number, b: number) => a - b;
export const desc = (a: number, b: number) => b - a;
export const sum = (arr: number[]) => arr.reduce((acc, cur) => acc + cur);
export const prod = (arr: number[]) => arr.reduce((acc, cur) => acc * cur);
export const min = (arr: number[]) => arr.reduce((acc, cur) => Math.min(cur, acc));
export const max = (arr: number[]) => arr.reduce((acc, cur) => Math.max(cur, acc));
export const group = <T>(arr: T[], count: number): T[][] => {
  let taken = 0;
  return arr.reduce((acc, cur) => {
    if (taken === 0) {
      acc.push([]);
    }
    acc[acc.length-1].push(cur);
    if (++taken >= count) {
      taken = 0;
    }
    return acc;
  }, <T[][]>[])
}