export class Input {
  private data: string[];
  private line: number;

  constructor(data: string[]) {
    this.data = data;
    this.line = 0;
  }

  parseSection<T>(section: Section<T>): T {
    while (
      this.line < this.data.length
      && section.wants(this.data[this.line])
    ) {
      section.collect(this.data[this.line++]);
    }
    return section.parse();
  }
}

export interface Parser<T> {
  parse: (lines: string[]) => T
}

export interface Rule<T> {
  passes: (value: T) => boolean
}

export class Section<T> {
  private parser;
  private lines: string[];
  private captureIf: Rule<string>;

  constructor(parser: Parser<T>, captureIf: Rule<string>) {
    this.parser = parser;
    this.lines = [];
    this.captureIf = captureIf;
  }

  collect(line: string): void {
    this.lines.push(line);
  }

  wants(line: string): boolean {
    return this.captureIf.passes(line);
  }

  hasData(): boolean {
    return this.lines.length > 0;
  }

  parse(): T {
    return this.parser.parse(this.lines);
  }
}

export class NestedSectionParser<T extends U[], U extends unknown[]> implements Parser<T> {
  private parser: Parser<U>;
  private captureIf: Rule<string>;
  private newSectionOn: Rule<string>;

  constructor(parser: Parser<U>, captureIf: Rule<string>, newSectionOn: Rule<string>) {
    this.parser = parser;
    this.captureIf = captureIf;
    this.newSectionOn = newSectionOn;
  }

  parse(lines: string[]): T {
    let currentSection = new Section<U>(this.parser, this.captureIf);
    const results: U[] = []; // U

    for (const line of lines) {
      if (this.newSectionOn.passes(line)) {
        results.push(currentSection.parse());
        currentSection = new Section<U>(this.parser, this.captureIf);
      } else if (currentSection.wants(line)){
        currentSection.collect(line);
      } else {
        console.warn('unhandled line:', line);
      }
    }

    // read to end of file and need to handle last group
    if (currentSection.hasData()) {
      results.push(currentSection.parse());
    }

    return results as T;
  }
}

export const Empty = (line: string) => line.length <= 0;
export const onEmpty = <Rule<string>>{ passes: Empty };

export const NonEmpty = (line: string) => !Empty(line);
export const onNonEmpty = <Rule<string>>{ passes: NonEmpty };

export const Always = () => true;
export const doAlways = <Rule<string>>{ passes: Always };

export const Never = () => false;
export const doNever = <Rule<string>>{ passes: Never };

export const Unchanged = <T>(val: T) => val;

export class GroupRule<T> implements Rule<T> {
  private taken = 0;
  private amount;

  constructor(amount: number) {
    this.amount = amount;
  }

  public passes(_: T) {
    if (++this.taken < this.amount) {
      return true;
    } else {
      this.taken = 0;
      return false;
    }
  }
}

export const Take = <T>(num: number) => new GroupRule(num);
