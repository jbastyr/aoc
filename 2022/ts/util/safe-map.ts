// point of this class is to have a map of k,v pairs with a known equality check
//    instead of just relying on reference equality
export default class SafeMap<Key, Value> {
  private equals: (key1: Key, key2: Key) => boolean;
  private internal: Map<Key, Value>;

  constructor(equals: (key1: Key, key2: Key) => boolean) {
    this.equals = equals;
    this.internal = new Map<Key, Value>();
  }

  public keys(): Key[] {
    return Array.from(this.internal.keys());
  }

  public has(key: Key): boolean {
    try {
      this.getInternalKey(key);
      return true;
    } catch {
      return false;
    }
  }

  public get(key: Key): Value {
    return this.internal.get(this.getInternalKey(key))!;
  }

  public set(key: Key, value: Value): void {
    try {
      const actual = this.getInternalKey(key);
      this.internal.set(actual, value);
    } catch {
      this.internal.set(key, value);
    }
  }

  public size(): number {
    return this.internal.size;
  }

  public each(perform: (key: Key, value: Value) => void): void {
    this.internal.forEach((v, k) => perform(k, v));
  }
  
  private getInternalKey(key: Key): Key {
    for (const candidate of this.internal.keys()) {
      if (this.equals(candidate, key)) {
        return candidate;
      }
    }
    throw 'not found';
  }
}