export const readLines = async (path: string): Promise<string[]> => {
  const data = await Deno.readTextFile(path);
  return data.split('\r\n');
}