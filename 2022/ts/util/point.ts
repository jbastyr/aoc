export interface Point {
  x: number,
  y: number
}

export const equals = (a: Point, b: Point) => a.x === b.x && a.y === b.y;

export const incX = (p: Point) => <Point>{ x: p.x + 1, y: p.y };
export const incY = (p: Point) => <Point>{ x: p.x, y: p.y + 1};
export const decX = (p: Point) => <Point>{ x: p.x - 1, y: p.y };
export const decY = (p: Point) => <Point>{ x: p.x, y: p.y - 1};

export const print = (p: Point) => `{x: ${p.x}, y: ${p.y}}`;
