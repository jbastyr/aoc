export default class Tuple<T,U> {
  public left: T;
  public right: U;

  constructor(left: T, right: U) {
    this.left = left;
    this.right = right;
  }

  public asArray(): [T, U] {
    return [this.left, this.right];
  }
}