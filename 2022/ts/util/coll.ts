abstract class Coll<T> {
  protected data: T[];

  constructor(init: T[] = []) {
    this.data = init;
  }

  public size(): number {
    return this.data.length;
  }

  public includes(item: T, equals: (a: T, b: T) => boolean = (a, b) => a == b): boolean {
    return this.data.some(d => equals(d, item));
  }

  public peek(): T {
    if (this.empty()) {
      throw 'no remaining items';
    }
    return this.data[0];
  }

  public empty(): boolean {
    return this.data.length === 0;
  }

  public updateEach(perform: (t: T) => T): void {
    for (let i = 0; i < this.data.length; i++) {
      this.data[i] = perform(this.data[i]);
    }
  }

  public print(): string {
    return this.data.map(val => `[${val}]`).join(' ');
  }

  public printMap(map: (t: T) => string) {
    return this.data.map(val => `[${map(val)}]`).join(' ');
  }

  public next(): T {
    if (this.empty()) {
      throw 'no remaining items';
    }
    return this.data.shift()!;
  }

  public abstract push(item: T): void;
}

export class Queue<T> extends Coll<T> {
  public push(item: T): void {
    this.data.push(item);
  }
}

export class Stack<T> extends Coll<T> {
  public push(item: T): void {
    this.data.unshift(item);
  }
}