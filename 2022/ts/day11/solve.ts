import { readLines } from "../util/read.ts";
import { desc, prod } from "../util/arr.ts";

interface Monkey {
  items: number[],
  update: (item: number) => number,
  test: number,
  whenTrue: number,
  whenFalse: number,
  inspected: number
}

const parseOperation = (op: string): (item: number) => number => {
  const symbol = op.split(' ')[0];
  const target = op.split(' ')[1];
  const useItem = target === 'old';
  switch (symbol) {
    case '*':
      return (item) => item * (useItem ? item : parseInt(target));
    case '+':
      return (item) => item + (useItem ? item : parseInt(target));
    case '-':
      return (item) => item - (useItem ? item : parseInt(target));
    case '/':
      return (item) => item / (useItem ? item : parseInt(target));
  }
  throw `unsupported operation ${op}`;
}

const monkeyBusiness = (data: string[], getBored: boolean, rounds: number): unknown => {
  const monkeys: Monkey[] = [];
  for (let i = 0; i < data.length; i+=7) {
    const startingItems = data[i+1].split(': ')[1].split(', ').map(i => parseInt(i));
    const operation = parseOperation(data[i+2].split('new = old ')[1]);
    const test = parseInt(data[i+3].split('divisible by ')[1]);
    const whenTrueTarget = parseInt(data[i+4].split(' monkey ')[1]);
    const whenFalseTarget = parseInt(data[i+5].split(' monkey ')[1]);

    monkeys.push({
      items: startingItems,
      update: operation,
      test: test,
      whenTrue: whenTrueTarget,
      whenFalse: whenFalseTarget,
      inspected: 0
    });
  }

  // each round
  for (let r = 0; r < rounds; r++) {
    // console.log(`\nround: ${r+1}`)
    // each monkey
    for (let m = 0; m < monkeys.length; m++) {
      const monkey = monkeys[m];
      // console.log(`monkey ${m}`);
      // each item
      while (monkey.items.length > 0) {
        monkey.inspected += 1;
        let item = monkey.items.shift()!;
        // console.log(`\tinspecting ${item}`)
        // before worrying, transform item such that it is still able to be divisible checked by all other monkeys
        item = item % monkeys.map(m => m.test).reduce(prod);
        // worry
        item = Math.floor(monkey.update(item));
        // console.log(`\t\tworried to ${item}`);
        // bored
        if (getBored) {
          item = Math.floor(item / 3);
        }
        // console.log(`\t\tbored to ${item}`);
        // test
        const target = item % monkey.test === 0 ? monkey.whenTrue : monkey.whenFalse;
        // console.log(`\t\ttest: ${item % monkey.test === 0}, target: ${target}`);
        // throw
        monkeys[target].items.push(item);
      }
    }

  }

  // let i = 0;
  // monkeys.forEach(m => console.log(`${i++}: ${m.inspected}`));
  const activeMonkeys = monkeys.map(m => m.inspected).sort(desc);
  return activeMonkeys[0] * activeMonkeys[1];
}

console.log(`expected: 10605, actual: ${monkeyBusiness(await readLines('given.txt'), true, 20)}`);
console.log(`chall: ${monkeyBusiness(await readLines('input.txt'), true, 20)}`);

console.log(`expected: 24, actual: ${monkeyBusiness(await readLines('given.txt'), false, 1)}`);
console.log(`expected: 10197, actual: ${monkeyBusiness(await readLines('given.txt'), false, 20)}`);
console.log(`expected: 27019168, actual: ${monkeyBusiness(await readLines('given.txt'), false, 1000)}`);
console.log(`expected: 108263829, actual: ${monkeyBusiness(await readLines('given.txt'), false, 2000)}`);
console.log(`expected: 2713310158, actual: ${monkeyBusiness(await readLines('given.txt'), false, 10000)}`);
console.log(`chall: ${monkeyBusiness(await readLines('input.txt'), false, 10000)}`);