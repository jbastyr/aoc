import { readLines } from "../util/read.ts";
import { decX, decY, equals, incX, incY, Point, print } from "../util/point.ts";
import { max } from "../util/arr.ts";
import { Queue } from "../util/coll.ts";

const findPathToHighestPoint = (data: string[], reverse: boolean = false): number => {
  // point within map of elevations
  const inbounds = (point: Point): boolean => {
    if (point.x < 0 || point.y < 0) {
      return false;
    }
  
    if (point.x >= elevations[0].length || point.y >= elevations.length) {
      return false;
    }
  
    return true;
  }

  // points within 1 space up
  const canVisit = (curr: Point, candidate: Point): boolean => {
    const currEl = elevations[curr.y][curr.x];
    const candEl = elevations[candidate.y][candidate.x];
    return candEl - currEl <= 1;
  }

  const printVisited = () => {
    const maxStep = visited.flat().reduce(max);
    const digits = Math.floor(Math.log10(maxStep)) + 1;
    const padded = visited.map(r => r.map(e => {
      if (e === -1) {
        return `${' '.repeat(digits - 1)}.`
      }
      const pad = digits - (Math.floor(Math.log10(e)) + 1);
      return `${' '.repeat(pad)}${e}`;
    }));
    padded.forEach(l => console.log(l.join(' ')));
  }

  const elevations: number[][] = [];
  let start: Point = {x: 0, y: 0};
  let end: Point = {x: 0, y: 0};

  for (const line of data) {
    const row: number[] = [];
    for (let i = 0; i < line.length; i++) {
      const currentChar = line.charCodeAt(i);
      
      if (currentChar === 83) {
        start = { x: i, y: elevations.length };
        row.push(0);
      } else if (currentChar === 69) {
         end = { x: i, y: elevations.length };
         row.push(27);
      } else {
        row.push(currentChar - 96);
      }
    }
    elevations.push(row);
  }

  console.log(`from ${print(start)} to ${print(end)}`);

  const visited = Array.from({ length: elevations.length}, () => Array.from({ length: elevations[0].length }, () => -1));
  const toVisit = new Queue<Point>(reverse ? [end] : [start]);
  
  let steps = 0;
  while (!toVisit.empty()) {
    steps += 1;
    const breadth = toVisit.size();
    console.log(`${steps} steps, queue size: ${toVisit.size()}`);
    // visited.forEach(row => {
    //   console.log(row.map(e => e === -1 ? '.' : e).join(' '));
    // });
    // size tracks breadth at current depth
    for (let i = 0; i < breadth; i++) {
      const curr = toVisit.next();
      // console.log(`at ${print(curr)}`);
      visited[curr.y][curr.x] = steps;

      const around = [decY(curr), incY(curr), decX(curr), incX(curr)];
      for (const p of around) {
        if (!inbounds(p)) {
          continue;
        }
        // console.log(`checking ${print(p)}`);

        if (reverse ? canVisit(p, curr) : canVisit(curr, p)) {
          if (reverse ? elevations[p.y][p.x] === 1 : equals(p, end)) {
            printVisited();

            return steps;
          }

          const existingSteps = visited[p.y][p.x];
          if (existingSteps !== -1) {
            // im not sure this will ever happen if we are searching breadth first
            if (existingSteps > steps) {
              if (!toVisit.includes(p, equals)) {
                toVisit.push(p);
              }
            }
          } else {
            if (!toVisit.includes(p, equals)) {
              toVisit.push(p);
            } 
          }
        } else {
          // console.log('too far to step to');
        }
      }
    }
  }

  printVisited();
  return 0;
}

console.log(`expected: 31, actual: ${findPathToHighestPoint(await readLines('given.txt'))}`);
console.log(`chall: ${findPathToHighestPoint(await readLines('input.txt'))}`);

console.log(`expected: 29, actual: ${findPathToHighestPoint(await readLines('given.txt'), true)}`);
console.log(`chall: ${findPathToHighestPoint(await readLines('input.txt'), true)}`);