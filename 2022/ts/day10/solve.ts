import { readLines } from "../util/read.ts";

const calculateSignalStrengths = (data: string[]): unknown => {
  const stops = [20,60,100,140,180,220];
  let registerX = 1;
  let signal = 0;
  
  let pc = 0;
  let cycle = 0;
  let pendingChange = 0;
  let crtLine = '';
  while (pc < data.length) {
    // update cycle and signal if we are at a stop
    cycle++;
    if (stops.includes(cycle)) {
      signal += (cycle * registerX);
    }

    // draw the current pixel on the crt line, and flush it if we are at the end of a row
    // console.log(`cycle: ${cycle}, reg: ${registerX}, printing: ${Math.abs((cycle % 40) - registerX) <= 1 ? '#' : '.'}`);
    if (Math.abs(((cycle-1) % 40) - registerX) <= 1) {
      crtLine += '#';
    } else {
      crtLine += '.';
    }
    if (cycle % 40 === 0) {
      console.log(crtLine);
      crtLine = '';
    }
    
    if (pendingChange !== 0) {
      registerX += pendingChange;
      pendingChange = 0;
    } else {
      const line = data[pc++];
      if (line.startsWith('addx')) {
        pendingChange = parseInt(line.split(' ')[1]);
      }
    }
  }

  return signal;
}

console.log(`expected: 13140, actual: ${calculateSignalStrengths(await readLines('given.txt'))}`);
console.log(`chall: ${calculateSignalStrengths(await readLines('input.txt'))}`);