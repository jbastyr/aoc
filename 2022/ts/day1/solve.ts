import { desc, sum } from "../util/arr.ts";
import { Input,Section,NestedSectionParser,Parser, onNonEmpty, onEmpty, doAlways } from "../util/input.ts";
import { readLines } from "../util/read.ts";

const findLargestNGroups = async (file: string, n: number): Promise<number> => {
  const data = new Input(await readLines(file))
    .parseSection(
      new Section<number[][]>(
        new NestedSectionParser(
          <Parser<number[]>>{
            parse: (lines: string[]) => lines.flatMap(l => parseInt(l))
          },
          onNonEmpty,
          onEmpty
        ),
        doAlways
    ));

  const mapped = data.map(group => sum(group));
  mapped.sort(desc);

  return sum(mapped.slice(0, n));
}

console.log(`expected: 24000, actual: ${await findLargestNGroups('given.txt', 1)}`);
console.log(`chall: ${await findLargestNGroups('input.txt', 1)}`);

console.log(`expected: 45000, actual: ${await findLargestNGroups('given.txt', 3)}`);
console.log(`chall: ${await findLargestNGroups('input.txt', 3)}`);