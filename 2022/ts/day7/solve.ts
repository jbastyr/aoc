import { asc, sum } from "../util/arr.ts";
import { readLines } from "../util/read.ts";
import { Stack } from "../util/coll.ts";

interface File {
  name: string;
  size: number;
}

interface Dir {
  name: string;
  size: number;
  subdirs: Dir[];
  files: File[];
}

const THRESHHOLD = 100000;
const TOTAL = 70000000;
const REQUIRED = 30000000;

const getTotal = (items: File[] | Dir[]): number => {
  // console.log(items);
  if (items.length > 0) {
    return items.map(n => n.size).reduce(sum);
  }
  return 0;
}

const calculateTotalSmallDirSize = async (input: string): Promise<string> => {
  const data = await readLines(input);
  const dirStack = new Stack<Dir>();
  let totalSmallDirs = 0;
  let smallestToDelete = 0;
  const allDirSizes: number[] = [];

  data.forEach(line =>  {
    if (dirStack.empty()) {
      dirStack.push(<Dir>{ name: '/', size: 0, files: [], subdirs: []});
      return;
    }

    const currentDir = dirStack.peek();

    if (line.startsWith('$ ls')) {
      return;
    }
  
    if (line.startsWith('$ cd')) {
      const arg = line.split('$ cd ')[1];
      if (arg === '..') {
        currentDir.size = getTotal(currentDir.files) + getTotal(currentDir.subdirs);

        if (currentDir.size <= THRESHHOLD) {
          totalSmallDirs += currentDir.size;
        }
        allDirSizes.push(currentDir.size);
        dirStack.next();
      } else {
        dirStack.push(currentDir.subdirs.find(d => d.name === arg)!);
      } 
    } else if (line.startsWith('dir')) {
      currentDir.subdirs.push(<Dir>{ name: line.split('dir ')[1], size: 0, files: [], subdirs: []});
    } else {
      currentDir.files.push(<File>{ name: line.split(' ')[1], size: parseInt(line.split(' ')[0])});
    }
  });

  let lastDir: Dir;
  while (!dirStack.empty()) {
    const currentDir = dirStack.peek();
    currentDir.size = getTotal(currentDir.files) + getTotal(currentDir.subdirs);
    if (currentDir.size <= THRESHHOLD) {
      totalSmallDirs += currentDir.size;
    }
    allDirSizes.push(currentDir.size);
    lastDir = dirStack.next();
  }

  const needToFree = REQUIRED - (TOTAL - lastDir!.size);
  console.log(lastDir!.size, needToFree);
  smallestToDelete = allDirSizes.sort(asc).filter(s => s >= needToFree)[0];

  return `${totalSmallDirs}, ${smallestToDelete}`;
}

console.log(`expected: 95437, 24933642, actual: ${await calculateTotalSmallDirSize('given.txt')}`);
console.log(`chall: ${await calculateTotalSmallDirSize('input.txt')}`);
