import { readLines } from "../util/read.ts";

const allCharsUnique = (chars: string[]): boolean => {
  return chars.every(c => chars.filter(ch => ch ===c).length === 1);
}

const findMarker = async (input: string, packetLength: number): Promise<number> => {
  const [data] = await readLines(input);
  for (let i = 0; i < data.length; i++) {
    if (allCharsUnique(data.substring(i, i+packetLength).split(''))) {
      return i+packetLength;
    }
  }
  return 0;
}

console.log(`expected: 11, actual: ${await findMarker('given.txt', 4)}`);
console.log(`chall: ${await findMarker('input.txt', 4)}`);

console.log(`expected: 26, actual: ${await findMarker('given.txt', 14)}`);
console.log(`chall: ${await findMarker('input.txt', 14)}`);