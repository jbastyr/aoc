import { readLines } from "../util/read.ts";

interface Section {
  min: number,
  max: number
}

const isFullyContained = (a: Section, b: Section): boolean => {
  return (a.min >= b.min && a.max <= b.max)
    || (b.min >= a.min && b.max <= a.max);
}

const isOverlappingAny = (a: Section, b: Section): boolean => {
  return (a.min >= b.min && a.min <= b.max)
    || (a.max >= b.min && a.max <= b.max)
    || (b.min >= a.min && b.min <= a.max)
    || (b.max >= a.min && b.max <= a.max);
}

const countOverlaps = async (input: string, doesOverlap: (a: Section, b: Section) => boolean): Promise<number> => {
  const data = await readLines(input);
  // const pairs = [];
  let fullyContained = 0;
  data.forEach(line => {
    const pair = line.split(',')
      .map(e => e.split('-'))
      .map(s => <Section>{min: parseInt(s[0]), max: parseInt(s[1])});
    
    if (doesOverlap(pair[0], pair[1])) {
      fullyContained++;
    }
  });

  return fullyContained;
}

console.log(`expected: 2, actual: ${await countOverlaps('given.txt', isFullyContained)}`);
console.log(`chall: ${await countOverlaps('input.txt', isFullyContained)}`);

console.log(`expected: 4, actual: ${await countOverlaps('given.txt', isOverlappingAny)}`);
console.log(`chall: ${await countOverlaps('input.txt', isOverlappingAny)}`);