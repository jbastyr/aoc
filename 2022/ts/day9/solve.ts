import { max, min } from "../util/arr.ts";
import { Point } from "../util/point.ts";
import { readLines } from "../util/read.ts";
import SafeMap from "../util/safe-map.ts";

const addOrUpdate = (visited: SafeMap<Point, number>, pos: Point): void => {
  if (visited.has(pos)) {
    visited.set(pos, visited.get(pos)!+1);
  } else {
    visited.set(pos, 1);
  }
}

const withinOneSpace = (curr: Point, ahead: Point): boolean => {
  return Math.abs(curr.x - ahead.x) <= 1 && Math.abs(curr.y - ahead.y) <= 1;
}

const performMove = (old: Point, dir: string): Point => {
  // console.log(`moving head ${dir}`)
  switch (dir) {
    case 'U':
      return {x: old.x, y: old.y + 1};
    case 'L':
      return {x: old.x - 1, y: old.y};
    case 'D':
      return {x: old.x, y: old.y - 1};
    case 'R':
      return {x: old.x + 1, y: old.y};
  }
  throw 'direction not recognized';
}

const moveToward = (cur: Point, ahead: Point): Point => {
  const changeX = ahead.x === cur.x ? 0 : ahead.x > cur.x ? 1 : -1;
  const changeY = ahead.y === cur.y ? 0 : ahead.y > cur.y ? 1 : -1;
  return {x: cur.x+changeX, y: cur.y+changeY};
}

const printMap = (visited: SafeMap<Point, number>): void => {
  const adjustX = 0 - visited.keys().map(p => p.x).reduce(min);
  const adjustY = 0 - visited.keys().map(p => p.y).reduce(min);

  const boundX = adjustX + visited.keys().map(p => p.x).reduce(max) + 1;
  const boundY = adjustY + visited.keys().map(p => p.y).reduce(max) + 1;

  // console.log(`${adjustX} - ${adjustY}, ${boundX} - ${boundY}`);

  const map = Array.from({ length: boundY }, () => Array.from({ length: boundX }, () => '.'));

  for (const k of visited.keys()) {
    // console.log(`attempting to mark map[${k.x},${k.y}] => map[${k.x+adjustX},${k.y+adjustY}]`)
    map[k.y+adjustY][k.x+adjustX] = '#';
  }

  map.reverse();
  for (const y of map) {
    let row = '';
    for (const x of y) {
      row += x;
    }
    console.log(row);
  }
}

const countTailPositions = (data: string[], tailLength: number): unknown => {
  const visited = new SafeMap<Point, number>((p1,p2) => p1.x === p2.x && p1.y === p2.y);
  
  let head: Point = {x: 0, y: 0};
  const tail: Point[] = Array.from({ length: tailLength }, () => ({x: 0, y: 0}));
  // let lastPos: Point = {x: 0, y: 0};

  addOrUpdate(visited, {x: 0, y: 0});

  for (const line of data) {
    const [dir, num] = line.split(' ');
    const count = parseInt(num);

    for (let i = 0; i < count; i++) {
      // record original head location

      // move head
      head = performMove(head, dir);
      // console.log(`after move, head: ${head.x},${head.y}, lastPos: ${lastPos.x},${lastPos.y}`);

      // move tail segments if the next position is further than 1 away
      for (let j = 0; j < tail.length; j++) {

        // current position of the next knot
        const ahead = j === 0 ? head : tail[j-1];
        if (!withinOneSpace(tail[j], ahead)) {
          // console.log(`moving ${j} from ${tail[j].x},${tail[j].y} to ${lastPos.x},${lastPos.y}`);

          // originally thought the tail segments followed directly, but they actually move
          //      independently relative to the next, diagonally individually if needed
          tail[j] = moveToward(tail[j], ahead);

          // only update the map of positions we've moved to if we are the last knot in the tail
          if (j === tail.length - 1) {
            addOrUpdate(visited, tail[j]);
          }
        }
      }
    }
  }

  // visited.each((k,v) => console.log(`${k.x},${k.y}: ${v}`));
  // printMap(visited);

  return visited.size();
}

console.log(`expected: 13, actual: ${countTailPositions(await readLines('given.txt'), 1)}`);
console.log(`chall: ${countTailPositions(await readLines('input.txt'), 1)}`);

console.log(`expected: 1, actual: ${countTailPositions(await readLines('given.txt'), 9)}`);
console.log(`expected: 36, actual: ${countTailPositions(await readLines('given_larger.txt'), 9)}`);
console.log(`chall: ${countTailPositions(await readLines('input.txt'), 9)}`);