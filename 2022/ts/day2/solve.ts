import { sum } from "../util/arr.ts";
import { doAlways, Input, Parser, Section } from "../util/input.ts";
import { readLines } from "../util/read.ts";

interface Play {
  letter: string,
  score: number,
  beats: string,
  ties: string,
  losesTo: string
}

const plays: Play[] = [
  {
    letter: 'X',
    score: 1,
    beats: 'C',
    ties: 'A',
    losesTo: 'B'
  },
  {
    letter: 'Y',
    score: 2,
    beats: 'A',
    ties: 'B',
    losesTo: 'C'
  },
  {
    letter: 'Z',
    score: 3,
    beats: 'B',
    ties: 'C',
    losesTo: 'A'
  }
];

const getPlayAsGuess = (_: string, player: string): Play => {
  // console.log(plays, player);
  return plays.find(p => p.letter === player)!;
}

const getPlayForOutcome = (opponent: string, player: string): Play => {
  if (player === 'X') {
    return plays.find(p => p.losesTo === opponent)!;
  } else if (player === 'Y') {
    return plays.find(p => p.ties === opponent)!;
  } else {
    return plays.find(p => p.beats === opponent)!;
  }
}

const getScore = (play: Play, letter: string): number => {
  return play.score + (
      play.beats === letter ? 6
      : play.ties === letter ? 3
      : 0
    );
}

const calculateScoreWithStrategyGuide = async (file: string, strategy: (opponent: string, player: string) => Play): Promise<number> => {
  const data = new Input(await readLines(file))
    .parseSection(
      new Section<string[][]>(
        <Parser<string[][]>>{
          parse: (lines: string[]) => lines.map(l => l.split(' '))
        },
        doAlways)
    );
  
  const scores = data.map(choices =>
      getScore(strategy(choices[0], choices[1]), choices[0])
    );

  return sum(scores);
} 

console.log(`expected: 15, actual: ${await calculateScoreWithStrategyGuide('given.txt', getPlayAsGuess)}`);
console.log(`chall: ${await calculateScoreWithStrategyGuide('input.txt', getPlayAsGuess)}`);

console.log(`expected: 12, actual: ${await calculateScoreWithStrategyGuide('given.txt', getPlayForOutcome)}`);
console.log(`chall: ${await calculateScoreWithStrategyGuide('input.txt', getPlayForOutcome)}`);