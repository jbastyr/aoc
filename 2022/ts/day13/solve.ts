import { readLines } from "../util/read.ts";
import Tuple from "../util/tuple.ts";

type PacketData = number | PacketData[]; // maybe not
type Result = boolean | undefined;

const debugLog = false;

const clog = (msg: unknown) => {
  if (debugLog) {
    console.log(msg);
  }
}


const isValidOrder = (left: PacketData[], right: PacketData[]): Result => {
  for (let i = 0; i < left.length; i++) {
    // right side runs out of numbers, out of order
    if (i >= right.length) {
      clog(`before: right ran out, out of order`)
      return false;
    }

    const leftIsNumber = typeof left[i] === 'number';
    const rightIsNumber = typeof right[i] === 'number';
    if (leftIsNumber && rightIsNumber)  {
      // left number lower than right, in order
      if (left[i] < right[i]) {
        clog(`num: left lower than right, in order`);
        return true;
      }
      // right number lower than left, out of order
      if (right[i] < left[i]) {
        clog(`num: right lower than left, out of order`);
        return false;
      }
      clog(`num: right equal left, skip`);
      // values are the same, check next
    } else {
      // if both are not numbes, one or both are lists
      // convert numbes to list and check recursively
      const leftData = leftIsNumber ? <PacketData[]>[left[i]] : <PacketData[]>left[i];
      const rightData = rightIsNumber ? <PacketData[]>[right[i]] : <PacketData[]>right[i];

      const result = isValidOrder(leftData, rightData);
      clog(`list: leftIsNumber: ${leftIsNumber}, rightIsNumber: ${rightIsNumber}`);
      clog(`list: result ${result}`);
      if (result !== undefined) {
        return result;
      }
    }
  }

  // completed checking left but right has more values, in order
  if (right.length > left.length) {
    clog(`after: left ran out, in order`);
    return true;
  }

  // completed checking list but all values are the same, keep checking
  clog(`after: both lists equal probably, skipping`);
  return undefined;
}

const getPackets = (data: string[]): Tuple<PacketData[], PacketData[]>[] => {
  const packets: Tuple<PacketData[], PacketData[]>[] = [];
  for (let i = 0; i < data.length; i+=3) {
    const left: PacketData[] = JSON.parse(data[i]);
    const right: PacketData[] = JSON.parse(data[i+1]);
    packets.push(new Tuple(left, right));
  }
  return packets;
}

const decoderIndexProduct = (data: string[]): number => {
  const firstKey = <PacketData[]>[[2]];
  const secondKey = <PacketData[]>[[6]];

  const packets = getPackets(data).map(p => p.asArray()).flat();
  packets.push(firstKey);
  packets.push(secondKey);
  packets.sort((a, b) => isValidOrder(a,b) ? -1 : 1);

  packets.forEach(e => clog(e));

  const firstKeyIndex = packets.indexOf(firstKey)+1;
  const secondKeyIndex = packets.indexOf(secondKey)+1;

  return firstKeyIndex * secondKeyIndex;
}

const sumOfPacketIndexesInOrder = (data: string[]): number => {
  const packets = getPackets(data);

  let validIndexSum = 0;
  for (let i = 0; i < packets.length; i++) {
    if (isValidOrder(packets[i].left, packets[i].right)) {
      clog(`${(i + 1)} pair in order`);

      validIndexSum += (i + 1);
    } else {
      clog(`${(i + 1)} pair out of order`);
      clog(packets[i].left);
      clog(packets[i].right);
    }
  }

  return validIndexSum;
}

console.log(`expected: 13, actual: ${sumOfPacketIndexesInOrder(await readLines('given.txt'))}`);
console.log(`chall: ${sumOfPacketIndexesInOrder(await readLines('input.txt'))}`);

console.log(`expected: 140, actual: ${decoderIndexProduct(await readLines('given.txt'))}`);
console.log(`chall: ${decoderIndexProduct(await readLines('input.txt'))}`);