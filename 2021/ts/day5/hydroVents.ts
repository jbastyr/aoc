type Point = {
  x: number,
  y: number
}

type Line = {
  from: Point,
  to: Point
}

const given: Line[] = [
  { from: {x:0, y:9}, to: { x:5, y:9} },
  { from: {x:8, y:0}, to: { x:0, y:8} },
  { from: {x:9, y:4}, to: { x:3, y:4} },
  { from: {x:2, y:2}, to: { x:2, y:1} },
  { from: {x:7, y:0}, to: { x:7, y:4} },
  { from: {x:6, y:4}, to: { x:2, y:0} },
  { from: {x:0, y:9}, to: { x:2, y:9} },
  { from: {x:3, y:4}, to: { x:1, y:4} },
  { from: {x:0, y:0}, to: { x:8, y:8} },
  { from: {x:5, y:5}, to: { x:8, y:2} }
];
const expected = 5;
const expectedWithDiagonals = 12;

const challengeInput: Line[] = [
  { from: { x:105, y:697}, to: { x:287, y:697} },
  { from: { x:705, y:62}, to: { x:517, y:250} },
  { from: { x:531, y:627}, to: { x:531, y:730} },
  { from: { x:21, y:268}, to: { x:417, y:268} },
  { from: { x:913, y:731}, to: { x:271, y:89} },
  { from: { x:214, y:697}, to: { x:82, y:697} },
  { from: { x:376, y:661}, to: { x:376, y:177} },
  { from: { x:519, y:859}, to: { x:977, y:859} },
  { from: { x:782, y:98}, to: { x:184, y:98} },
  { from: { x:612, y:179}, to: { x:515, y:179} },
  { from: { x:340, y:772}, to: { x:352, y:784} },
  { from: { x:111, y:863}, to: { x:111, y:298} },
  { from: { x:944, y:73}, to: { x:594, y:73} },
  { from: { x:465, y:21}, to: { x:970, y:21} },
  { from: { x:122, y:592}, to: { x:111, y:592} },
  { from: { x:975, y:975}, to: { x:16, y:16} },
  { from: { x:327, y:532}, to: { x:561, y:532} },
  { from: { x:811, y:618}, to: { x:811, y:945} },
  { from: { x:623, y:437}, to: { x:623, y:202} },
  { from: { x:380, y:591}, to: { x:871, y:591} },
  { from: { x:278, y:514}, to: { x:125, y:667} },
  { from: { x:797, y:946}, to: { x:953, y:946} },
  { from: { x:325, y:61}, to: { x:484, y:61} },
  { from: { x:450, y:422}, to: { x:450, y:862} },
  { from: { x:923, y:972}, to: { x:119, y:972} },
  { from: { x:813, y:141}, to: { x:69, y:885} },
  { from: { x:926, y:834}, to: { x:926, y:687} },
  { from: { x:137, y:564}, to: { x:595, y:106} },
  { from: { x:415, y:566}, to: { x:274, y:566} },
  { from: { x:726, y:354}, to: { x:251, y:829} },
  { from: { x:889, y:236}, to: { x:470, y:236} },
  { from: { x:282, y:376}, to: { x:282, y:193} },
  { from: { x:343, y:248}, to: { x:932, y:248} },
  { from: { x:790, y:918}, to: { x:790, y:528} },
  { from: { x:532, y:369}, to: { x:222, y:369} },
  { from: { x:15, y:378}, to: { x:820, y:378} },
  { from: { x:279, y:507}, to: { x:279, y:719} },
  { from: { x:641, y:68}, to: { x:220, y:68} },
  { from: { x:340, y:270}, to: { x:340, y:680} },
  { from: { x:939, y:364}, to: { x:32, y:364} },
  { from: { x:686, y:106}, to: { x:568, y:106} },
  { from: { x:919, y:365}, to: { x:255, y:365} },
  { from: { x:870, y:236}, to: { x:879, y:227} },
  { from: { x:322, y:397}, to: { x:397, y:322} },
  { from: { x:984, y:980}, to: { x:350, y:980} },
  { from: { x:392, y:864}, to: { x:31, y:864} },
  { from: { x:846, y:975}, to: { x:243, y:372} },
  { from: { x:253, y:981}, to: { x:500, y:734} },
  { from: { x:98, y:193}, to: { x:280, y:11} },
  { from: { x:477, y:460}, to: { x:350, y:460} },
  { from: { x:690, y:833}, to: { x:48, y:191} },
  { from: { x:469, y:409}, to: { x:218, y:409} },
  { from: { x:321, y:532}, to: { x:321, y:106} },
  { from: { x:868, y:341}, to: { x:223, y:986} },
  { from: { x:185, y:174}, to: { x:801, y:790} },
  { from: { x:256, y:658}, to: { x:800, y:658} },
  { from: { x:808, y:576}, to: { x:931, y:576} },
  { from: { x:959, y:913}, to: { x:959, y:785} },
  { from: { x:976, y:969}, to: { x:47, y:40} },
  { from: { x:891, y:931}, to: { x:572, y:612} },
  { from: { x:600, y:804}, to: { x:866, y:804} },
  { from: { x:149, y:368}, to: { x:680, y:899} },
  { from: { x:799, y:882}, to: { x:157, y:882} },
  { from: { x:803, y:214}, to: { x:803, y:668} },
  { from: { x:53, y:900}, to: { x:940, y:13} },
  { from: { x:424, y:800}, to: { x:424, y:261} },
  { from: { x:985, y:924}, to: { x:80, y:19} },
  { from: { x:158, y:194}, to: { x:158, y:281} },
  { from: { x:683, y:237}, to: { x:683, y:341} },
  { from: { x:493, y:482}, to: { x:493, y:921} },
  { from: { x:664, y:195}, to: { x:664, y:824} },
  { from: { x:689, y:405}, to: { x:616, y:478} },
  { from: { x:946, y:873}, to: { x:846, y:873} },
  { from: { x:977, y:988}, to: { x:28, y:39} },
  { from: { x:305, y:892}, to: { x:662, y:892} },
  { from: { x:891, y:27}, to: { x:891, y:440} },
  { from: { x:136, y:897}, to: { x:35, y:897} },
  { from: { x:948, y:458}, to: { x:935, y:458} },
  { from: { x:569, y:100}, to: { x:599, y:100} },
  { from: { x:542, y:292}, to: { x:974, y:724} },
  { from: { x:501, y:825}, to: { x:104, y:428} },
  { from: { x:875, y:872}, to: { x:875, y:441} },
  { from: { x:631, y:924}, to: { x:43, y:336} },
  { from: { x:874, y:846}, to: { x:874, y:389} },
  { from: { x:947, y:932}, to: { x:81, y:66} },
  { from: { x:75, y:480}, to: { x:75, y:403} },
  { from: { x:211, y:622}, to: { x:211, y:482} },
  { from: { x:344, y:904}, to: { x:699, y:549} },
  { from: { x:227, y:508}, to: { x:698, y:508} },
  { from: { x:677, y:774}, to: { x:385, y:774} },
  { from: { x:279, y:267}, to: { x:391, y:155} },
  { from: { x:294, y:801}, to: { x:547, y:801} },
  { from: { x:717, y:446}, to: { x:614, y:549} },
  { from: { x:490, y:903}, to: { x:490, y:225} },
  { from: { x:872, y:751}, to: { x:278, y:751} },
  { from: { x:580, y:163}, to: { x:61, y:163} },
  { from: { x:198, y:800}, to: { x:389, y:800} },
  { from: { x:147, y:728}, to: { x:516, y:728} },
  { from: { x:675, y:417}, to: { x:675, y:752} },
  { from: { x:147, y:544}, to: { x:134, y:544} },
  { from: { x:977, y:70}, to: { x:164, y:883} },
  { from: { x:349, y:976}, to: { x:349, y:23} },
  { from: { x:897, y:10}, to: { x:14, y:893} },
  { from: { x:602, y:349}, to: { x:602, y:354} },
  { from: { x:326, y:332}, to: { x:355, y:332} },
  { from: { x:53, y:331}, to: { x:34, y:331} },
  { from: { x:617, y:333}, to: { x:466, y:333} },
  { from: { x:661, y:537}, to: { x:661, y:131} },
  { from: { x:985, y:18}, to: { x:20, y:983} },
  { from: { x:953, y:580}, to: { x:953, y:124} },
  { from: { x:70, y:363}, to: { x:74, y:363} },
  { from: { x:448, y:38}, to: { x:141, y:38} },
  { from: { x:957, y:175}, to: { x:957, y:634} },
  { from: { x:88, y:316}, to: { x:88, y:899} },
  { from: { x:231, y:94}, to: { x:857, y:720} },
  { from: { x:643, y:566}, to: { x:643, y:832} },
  { from: { x:724, y:955}, to: { x:243, y:474} },
  { from: { x:368, y:521}, to: { x:537, y:521} },
  { from: { x:649, y:245}, to: { x:406, y:245} },
  { from: { x:92, y:304}, to: { x:399, y:304} },
  { from: { x:978, y:491}, to: { x:819, y:491} },
  { from: { x:99, y:637}, to: { x:765, y:637} },
  { from: { x:243, y:159}, to: { x:803, y:719} },
  { from: { x:139, y:756}, to: { x:305, y:756} },
  { from: { x:815, y:226}, to: { x:79, y:962} },
  { from: { x:317, y:562}, to: { x:491, y:562} },
  { from: { x:783, y:95}, to: { x:783, y:277} },
  { from: { x:207, y:321}, to: { x:133, y:321} },
  { from: { x:752, y:136}, to: { x:185, y:703} },
  { from: { x:752, y:990}, to: { x:752, y:433} },
  { from: { x:282, y:841}, to: { x:466, y:841} },
  { from: { x:314, y:31}, to: { x:314, y:829} },
  { from: { x:637, y:873}, to: { x:637, y:854} },
  { from: { x:60, y:746}, to: { x:563, y:243} },
  { from: { x:646, y:566}, to: { x:119, y:39} },
  { from: { x:260, y:475}, to: { x:124, y:339} },
  { from: { x:603, y:647}, to: { x:327, y:647} },
  { from: { x:990, y:202}, to: { x:342, y:202} },
  { from: { x:981, y:620}, to: { x:606, y:620} },
  { from: { x:475, y:352}, to: { x:313, y:352} },
  { from: { x:184, y:497}, to: { x:143, y:497} },
  { from: { x:130, y:929}, to: { x:329, y:929} },
  { from: { x:779, y:111}, to: { x:779, y:975} },
  { from: { x:892, y:960}, to: { x:11, y:79} },
  { from: { x:37, y:984}, to: { x:919, y:102} },
  { from: { x:589, y:794}, to: { x:589, y:548} },
  { from: { x:665, y:668}, to: { x:385, y:668} },
  { from: { x:668, y:301}, to: { x:281, y:301} },
  { from: { x:860, y:122}, to: { x:623, y:122} },
  { from: { x:18, y:914}, to: { x:782, y:150} },
  { from: { x:691, y:150}, to: { x:25, y:150} },
  { from: { x:117, y:439}, to: { x:462, y:439} },
  { from: { x:926, y:695}, to: { x:926, y:651} },
  { from: { x:907, y:644}, to: { x:708, y:644} },
  { from: { x:545, y:120}, to: { x:229, y:120} },
  { from: { x:181, y:659}, to: { x:181, y:820} },
  { from: { x:362, y:543}, to: { x:575, y:330} },
  { from: { x:603, y:531}, to: { x:603, y:142} },
  { from: { x:754, y:404}, to: { x:754, y:678} },
  { from: { x:703, y:551}, to: { x:450, y:551} },
  { from: { x:794, y:137}, to: { x:581, y:137} },
  { from: { x:866, y:288}, to: { x:327, y:827} },
  { from: { x:676, y:613}, to: { x:676, y:470} },
  { from: { x:874, y:130}, to: { x:23, y:981} },
  { from: { x:132, y:288}, to: { x:360, y:288} },
  { from: { x:706, y:147}, to: { x:706, y:433} },
  { from: { x:734, y:646}, to: { x:588, y:500} },
  { from: { x:641, y:386}, to: { x:598, y:343} },
  { from: { x:743, y:726}, to: { x:79, y:62} },
  { from: { x:308, y:192}, to: { x:859, y:192} },
  { from: { x:858, y:125}, to: { x:603, y:125} },
  { from: { x:694, y:199}, to: { x:653, y:240} },
  { from: { x:251, y:407}, to: { x:79, y:407} },
  { from: { x:254, y:337}, to: { x:254, y:310} },
  { from: { x:586, y:850}, to: { x:17, y:281} },
  { from: { x:937, y:989}, to: { x:17, y:69} },
  { from: { x:503, y:784}, to: { x:584, y:784} },
  { from: { x:17, y:97}, to: { x:906, y:986} },
  { from: { x:909, y:987}, to: { x:23, y:101} },
  { from: { x:11, y:465}, to: { x:953, y:465} },
  { from: { x:645, y:862}, to: { x:251, y:862} },
  { from: { x:741, y:488}, to: { x:856, y:488} },
  { from: { x:488, y:123}, to: { x:488, y:641} },
  { from: { x:720, y:775}, to: { x:79, y:775} },
  { from: { x:228, y:105}, to: { x:702, y:105} },
  { from: { x:344, y:804}, to: { x:873, y:275} },
  { from: { x:953, y:848}, to: { x:669, y:564} },
  { from: { x:188, y:76}, to: { x:524, y:76} },
  { from: { x:473, y:852}, to: { x:137, y:852} },
  { from: { x:515, y:14}, to: { x:515, y:183} },
  { from: { x:362, y:654}, to: { x:362, y:335} },
  { from: { x:76, y:73}, to: { x:969, y:966} },
  { from: { x:987, y:743}, to: { x:468, y:743} },
  { from: { x:912, y:28}, to: { x:912, y:31} },
  { from: { x:464, y:247}, to: { x:380, y:331} },
  { from: { x:171, y:20}, to: { x:171, y:863} },
  { from: { x:855, y:653}, to: { x:855, y:941} },
  { from: { x:505, y:415}, to: { x:505, y:808} },
  { from: { x:947, y:543}, to: { x:947, y:821} },
  { from: { x:907, y:365}, to: { x:726, y:365} },
  { from: { x:475, y:563}, to: { x:475, y:63} },
  { from: { x:927, y:679}, to: { x:773, y:679} },
  { from: { x:938, y:77}, to: { x:26, y:989} },
  { from: { x:345, y:909}, to: { x:299, y:909} },
  { from: { x:46, y:22}, to: { x:972, y:948} },
  { from: { x:197, y:735}, to: { x:288, y:735} },
  { from: { x:552, y:748}, to: { x:756, y:952} },
  { from: { x:946, y:180}, to: { x:946, y:695} },
  { from: { x:956, y:779}, to: { x:216, y:779} },
  { from: { x:120, y:105}, to: { x:950, y:935} },
  { from: { x:924, y:902}, to: { x:35, y:13} },
  { from: { x:530, y:49}, to: { x:451, y:128} },
  { from: { x:491, y:693}, to: { x:340, y:693} },
  { from: { x:533, y:774}, to: { x:623, y:864} },
  { from: { x:177, y:618}, to: { x:177, y:123} },
  { from: { x:543, y:114}, to: { x:637, y:114} },
  { from: { x:503, y:585}, to: { x:344, y:585} },
  { from: { x:34, y:836}, to: { x:34, y:625} },
  { from: { x:618, y:802}, to: { x:212, y:396} },
  { from: { x:863, y:678}, to: { x:349, y:678} },
  { from: { x:26, y:850}, to: { x:768, y:108} },
  { from: { x:99, y:67}, to: { x:988, y:956} },
  { from: { x:11, y:902}, to: { x:871, y:42} },
  { from: { x:658, y:749}, to: { x:507, y:900} },
  { from: { x:967, y:178}, to: { x:218, y:927} },
  { from: { x:671, y:247}, to: { x:671, y:525} },
  { from: { x:421, y:985}, to: { x:541, y:865} },
  { from: { x:279, y:639}, to: { x:754, y:164} },
  { from: { x:627, y:747}, to: { x:627, y:290} },
  { from: { x:77, y:66}, to: { x:977, y:966} },
  { from: { x:177, y:282}, to: { x:617, y:722} },
  { from: { x:400, y:444}, to: { x:451, y:393} },
  { from: { x:540, y:152}, to: { x:540, y:888} },
  { from: { x:521, y:196}, to: { x:36, y:196} },
  { from: { x:32, y:590}, to: { x:32, y:537} },
  { from: { x:145, y:613}, to: { x:279, y:747} },
  { from: { x:45, y:428}, to: { x:45, y:12} },
  { from: { x:785, y:956}, to: { x:785, y:728} },
  { from: { x:205, y:507}, to: { x:205, y:539} },
  { from: { x:117, y:12}, to: { x:117, y:221} },
  { from: { x:395, y:17}, to: { x:479, y:17} },
  { from: { x:104, y:881}, to: { x:933, y:52} },
  { from: { x:918, y:716}, to: { x:570, y:716} },
  { from: { x:121, y:621}, to: { x:937, y:621} },
  { from: { x:516, y:773}, to: { x:516, y:917} },
  { from: { x:311, y:605}, to: { x:311, y:168} },
  { from: { x:611, y:185}, to: { x:611, y:976} },
  { from: { x:373, y:80}, to: { x:373, y:295} },
  { from: { x:987, y:295}, to: { x:515, y:295} },
  { from: { x:416, y:717}, to: { x:416, y:121} },
  { from: { x:251, y:508}, to: { x:196, y:453} },
  { from: { x:498, y:824}, to: { x:428, y:754} },
  { from: { x:956, y:818}, to: { x:153, y:15} },
  { from: { x:266, y:272}, to: { x:266, y:748} },
  { from: { x:769, y:312}, to: { x:769, y:387} },
  { from: { x:604, y:766}, to: { x:184, y:766} },
  { from: { x:656, y:934}, to: { x:520, y:934} },
  { from: { x:224, y:771}, to: { x:162, y:771} },
  { from: { x:588, y:395}, to: { x:133, y:395} },
  { from: { x:219, y:489}, to: { x:219, y:948} },
  { from: { x:67, y:42}, to: { x:979, y:954} },
  { from: { x:684, y:109}, to: { x:920, y:345} },
  { from: { x:168, y:895}, to: { x:762, y:301} },
  { from: { x:761, y:953}, to: { x:59, y:953} },
  { from: { x:583, y:408}, to: { x:592, y:399} },
  { from: { x:129, y:48}, to: { x:931, y:48} },
  { from: { x:694, y:76}, to: { x:404, y:76} },
  { from: { x:808, y:380}, to: { x:808, y:886} },
  { from: { x:643, y:165}, to: { x:643, y:757} },
  { from: { x:714, y:543}, to: { x:714, y:913} },
  { from: { x:258, y:550}, to: { x:295, y:550} },
  { from: { x:400, y:857}, to: { x:400, y:38} },
  { from: { x:267, y:573}, to: { x:267, y:779} },
  { from: { x:124, y:182}, to: { x:255, y:51} },
  { from: { x:399, y:981}, to: { x:552, y:981} },
  { from: { x:197, y:803}, to: { x:197, y:275} },
  { from: { x:791, y:706}, to: { x:791, y:373} },
  { from: { x:500, y:664}, to: { x:924, y:664} },
  { from: { x:177, y:171}, to: { x:177, y:935} },
  { from: { x:703, y:43}, to: { x:696, y:43} },
  { from: { x:265, y:849}, to: { x:889, y:225} },
  { from: { x:847, y:324}, to: { x:661, y:324} },
  { from: { x:369, y:965}, to: { x:369, y:780} },
  { from: { x:169, y:965}, to: { x:935, y:199} },
  { from: { x:742, y:540}, to: { x:742, y:355} },
  { from: { x:210, y:854}, to: { x:204, y:854} },
  { from: { x:58, y:281}, to: { x:954, y:281} },
  { from: { x:858, y:793}, to: { x:666, y:793} },
  { from: { x:276, y:156}, to: { x:733, y:613} },
  { from: { x:537, y:538}, to: { x:80, y:81} },
  { from: { x:985, y:10}, to: { x:14, y:981} },
  { from: { x:79, y:31}, to: { x:692, y:644} },
  { from: { x:77, y:41}, to: { x:77, y:502} },
  { from: { x:684, y:150}, to: { x:17, y:817} },
  { from: { x:295, y:785}, to: { x:920, y:785} },
  { from: { x:171, y:579}, to: { x:171, y:16} },
  { from: { x:763, y:754}, to: { x:763, y:86} },
  { from: { x:719, y:573}, to: { x:719, y:71} },
  { from: { x:183, y:708}, to: { x:227, y:708} },
  { from: { x:826, y:952}, to: { x:835, y:952} },
  { from: { x:124, y:914}, to: { x:975, y:63} },
  { from: { x:807, y:704}, to: { x:653, y:704} },
  { from: { x:140, y:468}, to: { x:140, y:874} },
  { from: { x:408, y:330}, to: { x:408, y:291} },
  { from: { x:501, y:958}, to: { x:501, y:302} },
  { from: { x:834, y:505}, to: { x:686, y:357} },
  { from: { x:267, y:76}, to: { x:267, y:526} },
  { from: { x:18, y:88}, to: { x:863, y:933} },
  { from: { x:147, y:188}, to: { x:147, y:454} },
  { from: { x:922, y:733}, to: { x:277, y:733} },
  { from: { x:509, y:259}, to: { x:957, y:259} },
  { from: { x:614, y:765}, to: { x:238, y:765} },
  { from: { x:77, y:54}, to: { x:77, y:252} },
  { from: { x:591, y:532}, to: { x:591, y:384} },
  { from: { x:539, y:574}, to: { x:729, y:384} },
  { from: { x:347, y:158}, to: { x:347, y:10} },
  { from: { x:389, y:988}, to: { x:989, y:988} },
  { from: { x:696, y:571}, to: { x:662, y:605} },
  { from: { x:656, y:207}, to: { x:656, y:883} },
  { from: { x:802, y:446}, to: { x:802, y:693} },
  { from: { x:121, y:35}, to: { x:121, y:66} },
  { from: { x:967, y:738}, to: { x:949, y:738} },
  { from: { x:12, y:86}, to: { x:809, y:883} },
  { from: { x:96, y:167}, to: { x:758, y:829} },
  { from: { x:790, y:42}, to: { x:790, y:549} },
  { from: { x:14, y:987}, to: { x:986, y:15} },
  { from: { x:363, y:689}, to: { x:363, y:386} },
  { from: { x:148, y:148}, to: { x:807, y:807} },
  { from: { x:891, y:899}, to: { x:891, y:710} },
  { from: { x:445, y:678}, to: { x:445, y:464} },
  { from: { x:649, y:426}, to: { x:649, y:452} },
  { from: { x:641, y:378}, to: { x:967, y:378} },
  { from: { x:580, y:220}, to: { x:300, y:220} },
  { from: { x:376, y:789}, to: { x:376, y:572} },
  { from: { x:770, y:551}, to: { x:647, y:428} },
  { from: { x:651, y:692}, to: { x:399, y:692} },
  { from: { x:432, y:385}, to: { x:432, y:835} },
  { from: { x:242, y:48}, to: { x:512, y:48} },
  { from: { x:955, y:612}, to: { x:955, y:520} },
  { from: { x:926, y:568}, to: { x:938, y:556} },
  { from: { x:626, y:836}, to: { x:626, y:266} },
  { from: { x:973, y:982}, to: { x:39, y:48} },
  { from: { x:64, y:32}, to: { x:64, y:653} },
  { from: { x:503, y:444}, to: { x:641, y:444} },
  { from: { x:593, y:306}, to: { x:11, y:888} },
  { from: { x:287, y:138}, to: { x:287, y:891} },
  { from: { x:529, y:886}, to: { x:529, y:826} },
  { from: { x:217, y:320}, to: { x:217, y:875} },
  { from: { x:11, y:988}, to: { x:989, y:10} },
  { from: { x:291, y:30}, to: { x:488, y:30} },
  { from: { x:864, y:945}, to: { x:113, y:194} },
  { from: { x:550, y:501}, to: { x:550, y:89} },
  { from: { x:269, y:474}, to: { x:269, y:40} },
  { from: { x:953, y:394}, to: { x:908, y:394} },
  { from: { x:451, y:983}, to: { x:451, y:293} },
  { from: { x:135, y:121}, to: { x:455, y:121} },
  { from: { x:30, y:35}, to: { x:915, y:920} },
  { from: { x:31, y:451}, to: { x:31, y:936} },
  { from: { x:300, y:715}, to: { x:42, y:973} },
  { from: { x:577, y:459}, to: { x:577, y:700} },
  { from: { x:291, y:539}, to: { x:456, y:539} },
  { from: { x:373, y:449}, to: { x:855, y:449} },
  { from: { x:222, y:136}, to: { x:358, y:136} },
  { from: { x:206, y:14}, to: { x:206, y:211} },
  { from: { x:977, y:577}, to: { x:977, y:535} },
  { from: { x:183, y:723}, to: { x:183, y:900} },
  { from: { x:888, y:905}, to: { x:821, y:905} },
  { from: { x:51, y:301}, to: { x:388, y:301} },
  { from: { x:859, y:594}, to: { x:859, y:227} },
  { from: { x:767, y:343}, to: { x:767, y:472} },
  { from: { x:36, y:897}, to: { x:565, y:897} },
  { from: { x:450, y:481}, to: { x:855, y:481} },
  { from: { x:137, y:401}, to: { x:137, y:643} },
  { from: { x:771, y:276}, to: { x:771, y:61} },
  { from: { x:767, y:144}, to: { x:767, y:562} },
  { from: { x:212, y:111}, to: { x:978, y:877} },
  { from: { x:841, y:117}, to: { x:234, y:724} },
  { from: { x:975, y:104}, to: { x:263, y:104} },
  { from: { x:839, y:408}, to: { x:839, y:588} },
  { from: { x:122, y:50}, to: { x:911, y:839} },
  { from: { x:748, y:208}, to: { x:748, y:929} },
  { from: { x:230, y:305}, to: { x:645, y:305} },
  { from: { x:107, y:324}, to: { x:175, y:256} },
  { from: { x:726, y:339}, to: { x:726, y:968} },
  { from: { x:780, y:127}, to: { x:664, y:11} },
  { from: { x:392, y:148}, to: { x:392, y:133} },
  { from: { x:228, y:607}, to: { x:228, y:689} },
  { from: { x:469, y:379}, to: { x:739, y:379} },
  { from: { x:797, y:851}, to: { x:841, y:895} },
  { from: { x:896, y:494}, to: { x:896, y:568} },
  { from: { x:351, y:950}, to: { x:566, y:950} },
  { from: { x:593, y:387}, to: { x:492, y:488} },
  { from: { x:939, y:664}, to: { x:843, y:664} },
  { from: { x:463, y:159}, to: { x:197, y:159} },
  { from: { x:164, y:265}, to: { x:164, y:16} },
  { from: { x:164, y:147}, to: { x:510, y:493} },
  { from: { x:989, y:988}, to: { x:11, y:10} },
  { from: { x:98, y:676}, to: { x:693, y:676} },
  { from: { x:118, y:384}, to: { x:118, y:544} },
  { from: { x:220, y:502}, to: { x:220, y:593} },
  { from: { x:530, y:437}, to: { x:802, y:437} },
  { from: { x:321, y:29}, to: { x:321, y:819} },
  { from: { x:438, y:118}, to: { x:438, y:531} },
  { from: { x:268, y:128}, to: { x:802, y:128} },
  { from: { x:602, y:770}, to: { x:602, y:183} },
  { from: { x:841, y:58}, to: { x:846, y:63} },
  { from: { x:582, y:371}, to: { x:592, y:361} },
  { from: { x:174, y:163}, to: { x:296, y:163} },
  { from: { x:927, y:268}, to: { x:927, y:391} },
  { from: { x:579, y:280}, to: { x:12, y:847} },
  { from: { x:52, y:951}, to: { x:52, y:772} },
  { from: { x:645, y:203}, to: { x:985, y:203} },
  { from: { x:725, y:119}, to: { x:725, y:367} },
  { from: { x:155, y:112}, to: { x:779, y:736} },
  { from: { x:988, y:44}, to: { x:320, y:712} },
  { from: { x:438, y:463}, to: { x:914, y:463} },
  { from: { x:193, y:948}, to: { x:292, y:948} },
  { from: { x:217, y:398}, to: { x:638, y:398} },
  { from: { x:70, y:553}, to: { x:465, y:158} },
  { from: { x:271, y:262}, to: { x:867, y:262} },
  { from: { x:964, y:576}, to: { x:442, y:54} },
  { from: { x:253, y:67}, to: { x:972, y:67} },
  { from: { x:537, y:507}, to: { x:290, y:260} },
  { from: { x:537, y:645}, to: { x:213, y:321} },
  { from: { x:366, y:130}, to: { x:913, y:677} },
  { from: { x:834, y:283}, to: { x:834, y:523} },
  { from: { x:858, y:825}, to: { x:858, y:391} },
  { from: { x:146, y:60}, to: { x:146, y:701} },
  { from: { x:865, y:909}, to: { x:162, y:206} },
  { from: { x:503, y:628}, to: { x:326, y:628} },
  { from: { x:49, y:101}, to: { x:583, y:101} },
  { from: { x:692, y:17}, to: { x:692, y:218} },
  { from: { x:704, y:744}, to: { x:210, y:744} },
  { from: { x:144, y:434}, to: { x:587, y:434} },
  { from: { x:630, y:393}, to: { x:630, y:870} },
  { from: { x:606, y:616}, to: { x:606, y:330} },
  { from: { x:41, y:83}, to: { x:916, y:958} },
  { from: { x:80, y:341}, to: { x:706, y:967} },
  { from: { x:426, y:683}, to: { x:426, y:173} },
  { from: { x:919, y:962}, to: { x:499, y:962} },
  { from: { x:442, y:49}, to: { x:442, y:970} },
  { from: { x:740, y:378}, to: { x:498, y:378} },
  { from: { x:563, y:196}, to: { x:563, y:442} },
  { from: { x:222, y:76}, to: { x:614, y:76} },
  { from: { x:398, y:451}, to: { x:851, y:451} },
  { from: { x:62, y:50}, to: { x:243, y:50} },
  { from: { x:775, y:114}, to: { x:775, y:234} },
  { from: { x:650, y:901}, to: { x:650, y:195} },
  { from: { x:164, y:10}, to: { x:164, y:149} },
  { from: { x:127, y:751}, to: { x:67, y:751} },
  { from: { x:122, y:674}, to: { x:780, y:674} },
  { from: { x:325, y:652}, to: { x:70, y:652} },
  { from: { x:944, y:908}, to: { x:99, y:63} },
  { from: { x:40, y:985}, to: { x:977, y:48} },
  { from: { x:946, y:21}, to: { x:126, y:841} },
  { from: { x:872, y:906}, to: { x:872, y:136} },
  { from: { x:365, y:288}, to: { x:827, y:750} },
  { from: { x:348, y:935}, to: { x:244, y:935} },
  { from: { x:371, y:963}, to: { x:499, y:963} },
  { from: { x:816, y:595}, to: { x:392, y:171} },
  { from: { x:953, y:673}, to: { x:953, y:585} },
  { from: { x:223, y:612}, to: { x:223, y:362} },
  { from: { x:327, y:423}, to: { x:553, y:649} },
  { from: { x:661, y:693}, to: { x:258, y:693} },
  { from: { x:10, y:838}, to: { x:10, y:859} },
  { from: { x:985, y:814}, to: { x:985, y:25} },
  { from: { x:331, y:529}, to: { x:87, y:529} },
  { from: { x:611, y:460}, to: { x:355, y:460} },
  { from: { x:928, y:426}, to: { x:748, y:426} },
  { from: { x:540, y:172}, to: { x:365, y:347} },
  { from: { x:57, y:45}, to: { x:57, y:129} },
  { from: { x:20, y:861}, to: { x:628, y:253} },
  { from: { x:460, y:474}, to: { x:297, y:311} },
  { from: { x:549, y:876}, to: { x:131, y:876} },
  { from: { x:748, y:197}, to: { x:287, y:658} },
  { from: { x:639, y:137}, to: { x:741, y:137} },
  { from: { x:917, y:35}, to: { x:917, y:273} },
  { from: { x:482, y:333}, to: { x:975, y:826} },
  { from: { x:176, y:817}, to: { x:89, y:730} },
  { from: { x:894, y:418}, to: { x:806, y:418} },
  { from: { x:555, y:227}, to: { x:349, y:433} },
  { from: { x:317, y:33}, to: { x:432, y:148} },
  { from: { x:93, y:988}, to: { x:93, y:479} },
  { from: { x:635, y:300}, to: { x:870, y:300} },
  { from: { x:301, y:437}, to: { x:301, y:760} },
  { from: { x:660, y:548}, to: { x:660, y:909} },
  { from: { x:696, y:18}, to: { x:60, y:18} },
  { from: { x:231, y:787}, to: { x:165, y:787} },
  { from: { x:500, y:242}, to: { x:371, y:242} },
  { from: { x:88, y:126}, to: { x:405, y:126} },
  { from: { x:983, y:941}, to: { x:61, y:19} },
  { from: { x:242, y:519}, to: { x:242, y:489} },
  { from: { x:519, y:957}, to: { x:926, y:550} },
  { from: { x:606, y:181}, to: { x:606, y:432} },
  { from: { x:873, y:216}, to: { x:851, y:194} },
  { from: { x:880, y:924}, to: { x:880, y:844} },
  { from: { x:321, y:119}, to: { x:801, y:599} },
  { from: { x:963, y:392}, to: { x:726, y:155} },
  { from: { x:190, y:655}, to: { x:190, y:305} },
  { from: { x:542, y:676}, to: { x:542, y:819} }
];

function max(a: number, b: number): number {
  return a >= b ? a : b;
}

function min(a: number, b: number): number {
  return a <= b ? a : b;
}

function printBoard(board: number[][]): void {
  console.log("===============================");
  board.forEach(row => {
    let line = "| ";
    row.forEach(col => {
      line += ` ${col} `;
    });
    line += " |\n";
    console.log(line);
  });
  console.log("===============================");
}

function createBoard(lines: Line[]): number[][] {
  const lastCoordinate = findBoardDimensions(lines);

  return Array.from(Array(lastCoordinate.y + 1), _ => Array(lastCoordinate.x + 1).fill(0));
}

function findBoardDimensions(lines: Line[]): Point {
  let maxX = 0, maxY = 0;
  lines.forEach(line => {
    maxX = line.from.x > maxX ? line.from.x : maxX;
    maxX = line.to.x > maxX ? line.to.x : maxX;

    maxY = line.from.y > maxY ? line.from.y : maxY;
    maxY = line.to.y > maxY ? line.to.y: maxY;
  });

  return <Point>{ x: maxX, y: maxY };
}

function addLineToBoard(board: number[][], line: Line, includeDiagonals: boolean): void {
  // console.log(`trying to add line`);
  // console.log(line);
  if (line.from.x !== line.to.x && line.from.y === line.to.y) {
    const start = min(line.from.x, line.to.x);
    const end = max(line.from.x, line.to.x);

    // console.log(`found X discrepancy from ${start} to ${end}`);
    
    for (let i = start; i <= end; i++) {
      // console.log(`adding point at (${line.from.y}, ${i})`);
      board[line.from.y][i] += 1;
    }
  } else if (line.from.y !== line.to.y && line.from.x === line.to.x) {
    const start = min(line.from.y, line.to.y);
    const end = max(line.from.y, line.to.y);

    // console.log(`found Y discrepancy from ${start} to ${end}`);
    
    for (let i = start; i <= end; i++) {
      // console.log(`adding point at (${i}, ${line.from.x})`);
      board[i][line.from.x] += 1;
    }
  } else  if (includeDiagonals) {
    

    for (let x = line.from.x, y = line.from.y; x !== line.to.x && y !== line.to.y;) {
      // console.log(`adding point at (${x}, ${y})`);
      board[y][x] += 1;

      if (x < line.to.x) {
        x += 1;
      } else {
        x -= 1;
      }

      if (y < line.to.y) {
        y += 1;
      } else {
        y -= 1;
      }
    }

    // add final point at the end
    board[line.to.y][line.to.x] += 1;
  }
}

function countLineOverlaps(board: number[][]): number {
  return board.flatMap(i => i).filter(i => i > 1).length;
}

function calculateLineOverlaps(lines: Line[], includeDiagonals = false): number {
  const board = createBoard(lines);

  // printBoard(board);

  lines.forEach(line => {
    addLineToBoard(board, line, includeDiagonals);
  });

  // printBoard(board);

  return countLineOverlaps(board);
}

console.log(`expected: ${expected}, actual: ${calculateLineOverlaps(given)}`);

console.log(`challenge: ${calculateLineOverlaps(challengeInput)}`);

console.log(`expected: ${expectedWithDiagonals}, actual: ${calculateLineOverlaps(given, true)}`);

console.log(`challenge: ${calculateLineOverlaps(challengeInput, true)}`);