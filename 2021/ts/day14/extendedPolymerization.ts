type Rule = {
  match: string;
  insert: string;
}

const givenPolymer = "NNCB";
const givenRules = <Rule[]>[
  { match: "CH", insert: "B" },
  { match: "HH", insert: "N" },
  { match: "CB", insert: "H" },
  { match: "NH", insert: "C" },
  { match: "HB", insert: "C" },
  { match: "HC", insert: "B" },
  { match: "HN", insert: "C" },
  { match: "NN", insert: "C" },
  { match: "BH", insert: "H" },
  { match: "NC", insert: "B" },
  { match: "NB", insert: "B" },
  { match: "BN", insert: "B" },
  { match: "BB", insert: "N" },
  { match: "BC", insert: "B" },
  { match: "CC", insert: "N" },
  { match: "CN", insert: "C" }
];
const expectedRangeAfter10Steps = 1588;
const expectedRangeAfter40Steps = 2188189693529;


const challengePolymer = "VHCKBFOVCHHKOHBPNCKO";
const challengeRules = <Rule[]>[
  { match: "SO", insert: "F" },
  { match: "OP", insert: "V" },
  { match: "NF", insert: "F" },
  { match: "BO", insert: "V" },
  { match: "BH", insert: "S" },
  { match: "VB", insert: "B" },
  { match: "SV", insert: "B" },
  { match: "BK", insert: "S" },
  { match: "KC", insert: "N" },
  { match: "SP", insert: "O" },
  { match: "CP", insert: "O" },
  { match: "VN", insert: "O" },
  { match: "HO", insert: "S" },
  { match: "PC", insert: "B" },
  { match: "CS", insert: "O" },
  { match: "PO", insert: "K" },
  { match: "KF", insert: "B" },
  { match: "BP", insert: "K" },
  { match: "VO", insert: "O" },
  { match: "HB", insert: "N" },
  { match: "PH", insert: "O" },
  { match: "FF", insert: "O" },
  { match: "FB", insert: "K" },
  { match: "CC", insert: "H" },
  { match: "FK", insert: "F" },
  { match: "HV", insert: "P" },
  { match: "CO", insert: "S" },
  { match: "OC", insert: "N" },
  { match: "KV", insert: "V" },
  { match: "SS", insert: "O" },
  { match: "FC", insert: "O" },
  { match: "NP", insert: "B" },
  { match: "OH", insert: "B" },
  { match: "OF", insert: "K" },
  { match: "KB", insert: "K" },
  { match: "BN", insert: "C" },
  { match: "OK", insert: "C" },
  { match: "NC", insert: "O" },
  { match: "NO", insert: "O" },
  { match: "FS", insert: "C" },
  { match: "VP", insert: "K" },
  { match: "KP", insert: "S" },
  { match: "VS", insert: "B" },
  { match: "VV", insert: "N" },
  { match: "NN", insert: "P" },
  { match: "KH", insert: "P" },
  { match: "OB", insert: "H" },
  { match: "HP", insert: "H" },
  { match: "KK", insert: "H" },
  { match: "FH", insert: "F" },
  { match: "KS", insert: "V" },
  { match: "BS", insert: "V" },
  { match: "SN", insert: "H" },
  { match: "CB", insert: "B" },
  { match: "HN", insert: "K" },
  { match: "SB", insert: "O" },
  { match: "OS", insert: "K" },
  { match: "BC", insert: "H" },
  { match: "OV", insert: "N" },
  { match: "PN", insert: "B" },
  { match: "VH", insert: "N" },
  { match: "SK", insert: "C" },
  { match: "PV", insert: "K" },
  { match: "VC", insert: "N" },
  { match: "PF", insert: "S" },
  { match: "NB", insert: "B" },
  { match: "PP", insert: "S" },
  { match: "NS", insert: "F" },
  { match: "PB", insert: "B" },
  { match: "CV", insert: "C" },
  { match: "HK", insert: "P" },
  { match: "PK", insert: "S" },
  { match: "NH", insert: "B" },
  { match: "SH", insert: "V" },
  { match: "KO", insert: "H" },
  { match: "NV", insert: "B" },
  { match: "HH", insert: "V" },
  { match: "FO", insert: "O" },
  { match: "CK", insert: "O" },
  { match: "VK", insert: "F" },
  { match: "HF", insert: "O" },
  { match: "BF", insert: "C" },
  { match: "BV", insert: "P" },
  { match: "KN", insert: "K" },
  { match: "VF", insert: "C" },
  { match: "FN", insert: "V" },
  { match: "ON", insert: "C" },
  { match: "SF", insert: "F" },
  { match: "SC", insert: "C" },
  { match: "OO", insert: "S" },
  { match: "FP", insert: "K" },
  { match: "PS", insert: "C" },
  { match: "NK", insert: "O" },
  { match: "BB", insert: "V" },
  { match: "HC", insert: "H" },
  { match: "FV", insert: "V" },
  { match: "CH", insert: "N" },
  { match: "HS", insert: "V" },
  { match: "CF", insert: "F" },
  { match: "CN", insert: "S" },
];

function logIfDebug(...data: any[]): void {
  if (debug) {
    console.log(...data);
  }
}

function calculateFrequencies(polymer: string[]): Map<string, number> {
  const frequencyMap: Map<string, number> = new Map();

  polymer.forEach(e => frequencyMap.set(e, frequencyMap.has(e) ? frequencyMap.get(e) + 1 : 1));

  return frequencyMap;
}

function processRules(polymer: string[], rules: Rule[]): string[] {
  const workingPolymer: string[] = [];
  for (let i = 0; i + 1 < polymer.length; i++) {
    let pair = polymer[i] + polymer[i+1];

    // if we find a matching row, add the inserting element
    if (rules.some(r => r.match === pair)) {
      const matchingRule = rules.filter(r => r.match === pair)[0];
      pair = pair[0] + matchingRule.insert;
    }

    pair.split("").forEach(e => workingPolymer.push(e));
  }

  // add the last element back also
  workingPolymer.push(polymer[polymer.length - 1]);

  return workingPolymer;
}

function calculateElementFrequencyRange(frequencies: Map<string, number>): number {
  let max = 0, min = 0;
  for (let freq of frequencies) {
    if (max === 0 && min === 0) {
      max = freq[1];
      min = freq[1];
    } else {
      if (max < freq[1]) {
        max = freq[1];
      }

      if (min > freq[1]) {
        min = freq[1];
      }
    }
  }

  return max - min;
}

function calculateFrequencyRange(polymer: string, rules: Rule[], steps: number): number {
  let workingPolymer = polymer.split("");

  for (let i = 0; i < steps; i++) {
    // logIfDebug(`for step ${i}, polymer: `, workingPolymer);
    workingPolymer = processRules(workingPolymer, rules);
  }

  const frequencies = calculateFrequencies(workingPolymer);


  return calculateElementFrequencyRange(frequencies);
}

function copyMap(map: Map<string, number>): Map<string, number> {
  const workingMap: Map<string, number> = new Map();
  for (let iter of map) {
    workingMap.set(iter[0], iter[1]);
  }
  return workingMap;
}

function parsePolymer(polymer: string, pairs: Map<string, number>): Map<string, number> {
  const elements = polymer.split("");
  
  const workingPairs = copyMap(pairs);

  for (let i = 0; i+1 < elements.length; i++) {
    const pair = elements[i] + elements[i+1];
    workingPairs.set(pair, workingPairs.get(pair) + 1)
  }

  return workingPairs;
}

function processRulesOfPairMap(pairs: Map<string, number>, rules: Rule[]): Map<string, number> {
  
  // copy map so we dont change it under ourselves
  const workingPairs = copyMap(pairs);
  logIfDebug(workingPairs);

  // loop over original, this and copy ensures we are only counting the original pairs for our map
  //      and not created/removed ones during this step
  for(let iter of pairs) {
    
    // if our current pair has no occurrences, then we dont need to worry about processing it. go to the next
    if (iter[1] === 0) {
      continue;
    }

    logIfDebug(`found instance of pair: ${iter[0]} with freq: ${iter[1]}`);

    const pair = iter[0];
    const count = iter[1];
    
    const insertingElement = rules.filter(r => r.match === pair)[0].insert;

    logIfDebug(`inserting ${insertingElement}`);

    const firstNewPair = pair[0] + insertingElement;
    const secondNewPair = insertingElement + pair[1];

    logIfDebug(`decrementing ${pair} from ${workingPairs.get(pair)}`);

    // decrement the current pair by one since we will be splitting it into two new ones
    workingPairs.set(pair, workingPairs.get(pair) - count);

    // increment the count for each new pair that is created
    logIfDebug(`incrementing ${firstNewPair} from ${workingPairs.get(firstNewPair)}`);
    workingPairs.set(firstNewPair, workingPairs.get(firstNewPair) + count);
    logIfDebug(`incrementing ${secondNewPair} from ${workingPairs.get(secondNewPair)}`);
    workingPairs.set(secondNewPair, workingPairs.get(secondNewPair) + count);
  }

  logIfDebug(workingPairs);

  return workingPairs;
}

function calculateFrequenciesOfPairMap(pairs: Map<string, number>): Map<string, number> {
  const elements: Map<string, number> = new Map();

  for (let iter of pairs) {
    const pair = iter[0];
    const count = iter[1];

    // increase the count of each element of the pair by the number of times the pair occurs,
    //      or set it to this count if it doesnt exist already
    elements.set(pair[0], elements.has(pair[0]) ? elements.get(pair[0]) + count : count);
    elements.set(pair[1], elements.has(pair[1]) ? elements.get(pair[1]) + count : count);
  }

  return elements;
}

// cut all values in half since each original pair would have overlapped with another
function deduplicateFrequencyOccurrences(frequencies: Map<string, number>): Map<string, number> {
  const workingFrequencies = copyMap(frequencies);
  for (let iter of workingFrequencies) {
    workingFrequencies.set(iter[0], iter[1] / 2);
  }
  return workingFrequencies;
}

function correctFrequenciesForPairsAndExtremities(frequencies: Map<string, number>, polymer: string): Map<string, number> {
  const workingFrequencies = copyMap(frequencies);

  logIfDebug("workingFrequencies", workingFrequencies);

  const firstPolymerCharacter = polymer[0];
  const lastPolymerCharacter = polymer[polymer.length - 1];

  // decrement one instance from the first and last character/pair of the polymer so that we can account
  // for only the pairs that overlap when we de-duplicate
  workingFrequencies.set(firstPolymerCharacter, workingFrequencies.get(firstPolymerCharacter) - 1)
  workingFrequencies.set(lastPolymerCharacter, workingFrequencies.get(lastPolymerCharacter) - 1)

  logIfDebug("workingFrequencies", workingFrequencies);

  const deduplicatedFrequenciesFromPairs = deduplicateFrequencyOccurrences(workingFrequencies);

  logIfDebug("deduplicatedFrequenciesFromPairs", deduplicatedFrequenciesFromPairs);

  // add back in the first and last instance since we still need these occurrences to calculate the frequency range
  deduplicatedFrequenciesFromPairs.set(firstPolymerCharacter, deduplicatedFrequenciesFromPairs.get(firstPolymerCharacter) + 1)
  deduplicatedFrequenciesFromPairs.set(lastPolymerCharacter, deduplicatedFrequenciesFromPairs.get(lastPolymerCharacter) + 1)

  logIfDebug("deduplicatedFrequenciesFromPairs after re-increment", deduplicatedFrequenciesFromPairs);

  return deduplicatedFrequenciesFromPairs;
}

function calculateFrequencyRangeFaster(polymer: string, rules: Rule[], steps: number): number {
  let pairs: Map<string, number> = new Map();

  // load all rules into the map with 0 occurrences
  rules.forEach(r => pairs.set(r.match, 0));

  
  logIfDebug("parsePolymer NCNBCHB", parsePolymer("NCNBCHB", pairs));
  logIfDebug("parsePolymer NBCCNBBBCBHCB", parsePolymer("NBCCNBBBCBHCB", pairs));
  logIfDebug("parsePolymer NBBBCNCCNBBNBNBBCHBHHBCHB", parsePolymer("NBBBCNCCNBBNBNBBCHBHHBCHB", pairs));

  // parse the current polymer into the map
  pairs = parsePolymer(polymer, pairs);

  

  // for each step, spread the map? and adjust the counts of each item
  //    -1 for the current rule, +1 for first pair of rule's created triple, +1 for second pair
  for (let i = 0; i < steps; i++) {
    pairs = processRulesOfPairMap(pairs, rules);
  }

  const frequencies = calculateFrequenciesOfPairMap(pairs);
  logIfDebug("frequencies: ", frequencies);

  const adjustedFrequencies = correctFrequenciesForPairsAndExtremities(frequencies, polymer)
  logIfDebug("adjustedFrequencies: ", frequencies);
    
  const range = calculateElementFrequencyRange(adjustedFrequencies);
  logIfDebug("range: ", frequencies);

  return range;
}

const debug = false;

console.log(`expected after 10 steps: ${expectedRangeAfter10Steps}, actual: ${calculateFrequencyRange(givenPolymer, givenRules, 10)}`);
console.log(`expected after 10 steps: ${expectedRangeAfter10Steps}, actual (but faster): ${calculateFrequencyRangeFaster(givenPolymer, givenRules, 10)}`);

console.log(`challenge after 10 steps: ${calculateFrequencyRange(challengePolymer, challengeRules, 10)}`);

console.log(`challenge after 10 steps (but faster): ${calculateFrequencyRangeFaster(challengePolymer, challengeRules, 10)}`);

/*

// FIXME:
console.log(`expected after 40 steps: ${expectedRangeAfter40Steps}, actual: ${calculateFrequencyRange(givenPolymer, givenRules, 40)}`);

runs out of heap space as is, because we are creating a polymer so long its well past the max array size
need to think about how to re-work this functionality to allow for an arbitrary number of steps

there must be some way to do it without constructing the entire polymer. Thinking back to how I used an array to hold counts earlier instead
    of building it entirely...

thinking that maybe we can extrapolate a relationship between the rules and the resulting pairs that would be created after inserting

it would be good to check if all possible combinations of the characters would be present in a rule
    it seems this is true as pairs are n * n rules, 4 for the given -> 16, 10 for the challenge -> 100

potential solution:
instead of keeping a list of all polymer elements, we could keep a list of all pairs and their occurences
    my concern here is accurately counting the overlapping elements
        every element except the first and last would belong to 2 pairs though, so maybe it would be easy enough to keep track of these at the beginning
        and then at the end, subtract them from the totals, divide by two, and add them back?

    also, when processing rules, for each match, we would need to remove that match, and add 1 each for either side of the new triple

*/
console.log(`expected after 40 steps: ${expectedRangeAfter40Steps}, actual: ${calculateFrequencyRangeFaster(givenPolymer, givenRules, 40)}`);

console.log(`challenge after 40 steps: ${calculateFrequencyRangeFaster(challengePolymer, challengeRules, 40)}`);