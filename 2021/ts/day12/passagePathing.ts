enum CaveType {
  START = "START" ,
  END = "END",
  BIG = "BIG",
  SMALL = "SMALL"
}

type Cave = {
  code: string;
  type: CaveType;
  paths: Cave[]
}

type CavePath = {
  a: string;
  b: string;
}

const givenMinimal = [
  { a: "start", b: "end" }
];
const expectedMinimal = 1;

const givenTiny = [
  { a: "start", b: "A" },
  { a: "A", b: "end" }
]
const expectedTiny = 1;

const givenMini = [
  { a: "start", b: "a" },
  { a: "start", b: "b" },
  { a: "a", b: "b" },
  { a: "b", b: "end" }
];
const expectedMini = 2;
const expectedMiniRevisit = 3;

const givenSmall = [
  { a: "start", b: "A" },
  { a: "start", b: "b" },
  { a: "A", b: "c" },
  { a: "A", b: "b" },
  { a: "b", b: "d" },
  { a: "A", b: "end" },
  { a: "b", b: "end" }
];
const expectedSmall = 10;
const expectedSmallRevisit = 36;

const givenLarge = [
  { a: "dc", b: "end" },
  { a: "HN", b: "start" },
  { a: "start", b: "kj" },
  { a: "dc", b: "start" },
  { a: "dc", b: "HN" },
  { a: "LN", b: "dc" },
  { a: "HN", b: "end" },
  { a: "kj", b: "sa" },
  { a: "kj", b: "HN" },
  { a: "kj", b: "dc" }
];
const expectedLarge = 19;
const expectedLargeRevisit = 103;

const givenHuge = [
  { a: "fs", b: "end" },
  { a: "he", b: "DX" },
  { a: "fs", b: "he" },
  { a: "start", b: "DX" },
  { a: "pj", b: "DX" },
  { a: "end", b: "zg" },
  { a: "zg", b: "sl" },
  { a: "zg", b: "pj" },
  { a: "pj", b: "he" },
  { a: "RW", b: "he" },
  { a: "fs", b: "DX" },
  { a: "pj", b: "RW" },
  { a: "zg", b: "RW" },
  { a: "start", b: "pj" },
  { a: "he", b: "WI" },
  { a: "zg", b: "he" },
  { a: "pj", b: "fs" },
  { a: "start", b: "RW" }
];
const expectedHuge = 226;
const expectedHugeRevisit = 3509;

const challenge = [
  { a: "zs", b: "WO" },
  { a: "zs", b: "QJ" },
  { a: "WO", b: "zt" },
  { a: "zs", b: "DP" },
  { a: "WO", b: "end" },
  { a: "gv", b: "zt" },
  { a: "iu", b: "SK" },
  { a: "HW", b: "zs" },
  { a: "iu", b: "WO" },
  { a: "gv", b: "WO" },
  { a: "gv", b: "start" },
  { a: "gv", b: "DP" },
  { a: "start", b: "WO" },
  { a: "HW", b: "zt" },
  { a: "iu", b: "HW" },
  { a: "gv", b: "HW" },
  { a: "zs", b: "SK" },
  { a: "HW", b: "end" },
  { a: "zs", b: "end" },
  { a: "DP", b: "by" },
  { a: "DP", b: "iu" },
  { a: "zt", b: "start" }
];

function wait(seconds: number): void {
  var waitTill = new Date(new Date().getTime() + seconds * 1000);
  while(waitTill > new Date()){}
}

function printCaveSystem(cave: Cave): void {
  function walkCaveSystem(cave: Cave, caves: Cave[]): void {
    logIfDebug(`Cave: ${cave.code}`);
    caves.push(cave);

    for (let sub of cave.paths) {
      logIfDebug(` -> ${sub.code}`);
    }

    for (let sub of cave.paths) {
      if (caves.indexOf(sub) < 0) {
        walkCaveSystem(sub, caves);
      }
    }
  }

  const caves: Cave[] = [];
  walkCaveSystem(cave, caves);
}

function printPaths(paths: Cave[][]): void {
  for (let path of paths) {
    logIfDebug('Path: ');
    let pathText = "";
    for (let cave of path) {
      pathText +=  " -> " + cave.code;
    }
    logIfDebug(pathText);
  }
}

function determineCaveType(code: string): CaveType {
  if (code === "start") {
    return CaveType.START;
  }else if (code === "end") {
    return CaveType.END;
  } else if (code.toUpperCase() === code) {
    return CaveType.BIG;
  } else {
    return CaveType.SMALL;
  }
}

// build cave system and return the start node
function buildCaveSystem(input: CavePath[]): Cave {
  function createCaveOrReturnExisting(code: string, caves: Cave[]): Cave {
    const existingCaveIndex = caves.map(cave => cave.code).indexOf(code);
    if (existingCaveIndex < 0) {
      // logIfDebug(`creating cave for ${code}`);
      const newCave = {code: code, type: determineCaveType(code), paths: []};
      caves.push(newCave);
      return newCave;
    } else {
      // logIfDebug(`found existing cave for ${code}`);
      return caves[existingCaveIndex];
    }
  }

  const start = <Cave>{code: "start", type: CaveType.START, paths: []};
  const end = <Cave>{code: "end", type: CaveType.END, paths: []};

  const caves: Cave[] = [];
  caves.push(start);
  caves.push(end);

  for (let cave of input) {
    // logIfDebug("building, adding:", cave);
    const aCave = createCaveOrReturnExisting(cave.a, caves);
    const bCave = createCaveOrReturnExisting(cave.b, caves);
    aCave.paths.push(bCave);
    bCave.paths.push(aCave);

    // logIfDebug("aCave:", aCave);
    // logIfDebug("bCave:", bCave);
  }

  return start;
}

function pathContainsSmallRevisit(path: Cave[]): boolean {
  const smallCaveCodes = path.filter(cave => cave.type === CaveType.SMALL).map(cave => cave.code);
  logIfDebug(smallCaveCodes);
  // create set with cave codes to detect duplicates with based on sizes
  return (new Set(smallCaveCodes)).size !== smallCaveCodes.length;
}

function pathContainsCave(path: Cave[], cave: Cave): boolean {
  return path.some(c => c.code === cave.code);
}

function logIfDebug(...items: any[]): void {
  if (debug) {
    console.log(items)
  }
}

function addChildCaves(currentPath: Cave[], children: Cave[], allCavePaths: Cave[][], withRevisit: boolean): void {
  for (let child of children) {
    // early continue on start to simplify later checks
    if (child.type === CaveType.START) {
      continue;
    }

    if (pathContainsCave(currentPath, child)) {
      logIfDebug(`pathContainsCave`, child);
      if (child.type === CaveType.BIG) {
        tryWalkCavePathRecursively(child, [...currentPath], allCavePaths, withRevisit);
      }

      if (child.type === CaveType.SMALL) {
        if (withRevisit && !pathContainsSmallRevisit(currentPath)) {
          logIfDebug(` withRevisit but no existing revisit`);
          // if we are allowing revisits, but have not already encountered one, we can add it again
          tryWalkCavePathRecursively(child, [...currentPath], allCavePaths, withRevisit);
        }
      }
    } else {
      logIfDebug(`path does not contain cave: `, child, `adding it probably `)
      tryWalkCavePathRecursively(child, [...currentPath], allCavePaths, withRevisit);
    }
  }
}

function tryWalkCavePathRecursively(cave: Cave, currentPath: Cave[], allCavePaths: Cave[][], withRevisit: boolean): void {
  // wait(1);

  logIfDebug(`adding cave ${cave.code} to current path`);
  printPaths([currentPath]);
  
  currentPath.push(cave);
  switch (cave.type) {
    case CaveType.START:
    case CaveType.BIG:
    case CaveType.SMALL: {
      
      addChildCaves(currentPath, cave.paths, allCavePaths, withRevisit);
    }break;
    case CaveType.END: {
      logIfDebug(currentPath);
      allCavePaths.push(currentPath);
    }break;
  }
}

function walkAllCavePaths(start: Cave, withRevisit: boolean): Cave[][] {
  const cavePaths: Cave[][] = [];
  tryWalkCavePathRecursively(start, [], cavePaths, withRevisit);
  return cavePaths;
}

function countCavePaths(input: CavePath[], withRevisit: boolean = false): number {
  const startingCave = buildCaveSystem(input);
  printCaveSystem(startingCave);

  const paths = walkAllCavePaths(startingCave, withRevisit);

  printPaths(paths);

  return paths.length;
}

const debug = false;
console.log(`expected minimal: ${expectedMinimal}, actual: ${countCavePaths(givenMinimal)}`);
console.log(`expected tiny: ${expectedTiny}, actual: ${countCavePaths(givenTiny)}`);
console.log(`expected mini: ${expectedMini}, actual: ${countCavePaths(givenMini)}`);
console.log(`expected small: ${expectedSmall}, actual: ${countCavePaths(givenSmall)}`);
console.log(`expected large: ${expectedLarge}, actual: ${countCavePaths(givenLarge)}`);
console.log(`expected huge: ${expectedHuge}, actual: ${countCavePaths(givenHuge)}`);

console.log(`challenge: ${countCavePaths(challenge)}`);

console.log(`expected mini with revisit: ${expectedMiniRevisit}, actual: ${countCavePaths(givenMini, true)}`);
console.log(`expected small with revisit: ${expectedSmallRevisit}, actual: ${countCavePaths(givenSmall, true)}`);
console.log(`expected large with revisit: ${expectedLargeRevisit}, actual: ${countCavePaths(givenLarge, true)}`);
console.log(`expected huge with revisit: ${expectedHugeRevisit}, actual: ${countCavePaths(givenHuge, true)}`);

console.log(`challenge with revisit: ${countCavePaths(challenge, true)}`);