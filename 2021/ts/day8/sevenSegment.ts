type Entry = {
  input: string[];
  output: string[];
}

const given = <Entry[]>[
  { input: ["be", "cfbegad", "cbdgef", "fgaecd", "cgeb", "fdcge", "agebfd", "fecdb", "fabcd", "edb"], output: ["fdgacbe", "cefdb", "cefbgd", "gcbe"] },
  { input: ["edbfga", "begcd", "cbg", "gc", "gcadebf", "fbgde", "acbgfd", "abcde", "gfcbed", "gfec"], output: ["fcgedb", "cgb", "dgebacf", "gc"] },
  { input: ["fgaebd", "cg", "bdaec", "gdafb", "agbcfd", "gdcbef", "bgcad", "gfac", "gcb", "cdgabef"], output: ["cg", "cg", "fdcagb", "cbg"] },
  { input: ["fbegcd", "cbd", "adcefb", "dageb", "afcb", "bc", "aefdc", "ecdab", "fgdeca", "fcdbega"], output: ["efabcd", "cedba", "gadfec", "cb"] },
  { input: ["aecbfdg", "fbg", "gf", "bafeg", "dbefa", "fcge", "gcbea", "fcaegb", "dgceab", "fcbdga"], output: ["gecf", "egdcabf", "bgf", "bfgea"] },
  { input: ["fgeab", "ca", "afcebg", "bdacfeg", "cfaedg", "gcfdb", "baec", "bfadeg", "bafgc", "acf"], output: ["gebdcfa", "ecba", "ca", "fadegcb"] },
  { input: ["dbcfg", "fgd", "bdegcaf", "fgec", "aegbdf", "ecdfab", "fbedc", "dacgb", "gdcebf", "gf"], output: ["cefg", "dcbef", "fcge", "gbcadfe"] },
  { input: ["bdfegc", "cbegaf", "gecbf", "dfcage", "bdacg", "ed", "bedf", "ced", "adcbefg", "gebcd"], output: ["ed", "bcgafe", "cdgba", "cbgef"] },
  { input: ["egadfb", "cdbfeg", "cegd", "fecab", "cgb", "gbdefca", "cg", "fgcdab", "egfdb", "bfceg"], output: ["gbdfcae", "bgc", "cg", "cgb"] },
  { input: ["gcafb", "gcf", "dcaebfg", "ecagb", "gf", "abcdeg", "gaef", "cafbge", "fdbac", "fegbdc"], output: ["fgae", "cfgab", "fg", "bagce"] }   
];
const expected = 26;
const expectedSum = 61229;

const challengeInput = [
  { input: ["abdfce", "bedag", "acdefgb", "cg", "febcga", "fbdac", "fcdg", "cabdg", "bcg", "bgacdf"], output: ["fdcab", "adbcf", "gcb", "acdebf"] },
  { input: ["bgeacdf", "dgebca", "gbc", "fbgd", "fceba", "fecdg", "bcgef", "cfgbde", "gb", "fegacd"], output: ["dbfg", "abecf", "dfgb", "gaecdf"] },
  { input: ["dfega", "bedag", "af", "agf", "fdagcb", "cbaedgf", "feac", "dfecg", "dcegfa", "gcfdbe"], output: ["fag", "edcgf", "fga", "bfgcad"] },
  { input: ["ca", "afc", "bdafe", "bgface", "afdebc", "dcab", "cgedf", "fcaed", "cdebfga", "fabged"], output: ["fcdea", "abdc", "fdcbea", "fdbcega"] },
  { input: ["gabde", "beacg", "cgafe", "gcfeab", "gebdcf", "fedagc", "edfgcba", "bc", "acfb", "bcg"], output: ["fagedc", "bgc", "bgace", "cbg"] },
  { input: ["df", "cfd", "geabfc", "ebgadcf", "dfae", "dcgfe", "decgb", "agdcfb", "aecdfg", "cgfae"], output: ["fd", "fd", "ecfgd", "gfbcea"] },
  { input: ["fcebdga", "cf", "faebgd", "fecgbd", "bcdfg", "egbdf", "cgadfe", "bgadc", "gcf", "cefb"], output: ["fcg", "gfc", "fgc", "dfcega"] },
  { input: ["abedcg", "afdcgb", "ae", "fcgad", "fbcge", "gea", "dfae", "deafgc", "efgca", "cagebfd"], output: ["gea", "fgcbade", "ea", "ega"] },
  { input: ["dc", "egcbf", "dgbce", "abdge", "acdg", "dbfeac", "dbeagf", "gecfbda", "dec", "cgdabe"], output: ["agebcdf", "cadg", "agbfed", "cd"] },
  { input: ["bag", "ecgfb", "beagf", "ga", "edbaf", "aegc", "bfgdac", "bedcgfa", "gabcfe", "fdebgc"], output: ["debacfg", "bga", "degcfb", "bacgfde"] },
  { input: ["gebcfa", "efbcgda", "fbgdae", "egcdb", "aecbg", "dfegac", "eagbf", "fbca", "ac", "aec"], output: ["gaebdf", "cabf", "aegbdf", "fegba"] },
  { input: ["bedg", "abcdg", "dcagef", "gd", "cfgba", "fdaebc", "cdg", "badgec", "bcaed", "egcadbf"], output: ["gabced", "gd", "bcdag", "ebgd"] },
  { input: ["gbdfca", "agedfc", "acedg", "fcgeb", "dbacfeg", "ab", "cba", "cbadeg", "daeb", "gbace"], output: ["adbfgc", "ecfdbga", "ebfcg", "ab"] },
  { input: ["bfdag", "cbegafd", "dfabeg", "bge", "ge", "bdcae", "gabfdc", "faeg", "gefdbc", "gdeba"], output: ["dbega", "dgecbfa", "adbce", "fecbadg"] },
  { input: ["edbf", "efcabg", "edfbgc", "gdcbf", "ecgdf", "egdacf", "cbf", "bf", "cbegfda", "gadcb"], output: ["dfbe", "gfdbc", "fcb", "bf"] },
  { input: ["gcefa", "egdb", "gcfeb", "fdgbace", "dfbcg", "be", "bgfdca", "caedfb", "bec", "cbfgde"], output: ["bec", "dgeb", "ceagf", "egfcbda"] },
  { input: ["eabcd", "ceagfb", "bdgcea", "cgdeb", "bgcfde", "gadc", "fabde", "abc", "ac", "dfgebca"], output: ["agfcbe", "bcadge", "abc", "cab"] },
  { input: ["agfedc", "edabfcg", "bf", "fgcdab", "ecabd", "dfb", "dfagc", "afbegd", "cbfg", "cfabd"], output: ["fbcg", "cfadg", "geafbd", "deabc"] },
  { input: ["afdeg", "ce", "eafbcdg", "bgcfa", "fedagb", "dcgbfe", "aedc", "gcaedf", "fcage", "cge"], output: ["dfcbgae", "ec", "fecga", "caed"] },
  { input: ["bec", "cbgedf", "fagbcd", "abdec", "agbed", "feacdb", "fdcab", "acfe", "dgbcafe", "ce"], output: ["cgfbad", "abdge", "bec", "badcfe"] },
  { input: ["begac", "ade", "egcadb", "dfceagb", "acdg", "degfb", "eafcbg", "da", "bedacf", "dageb"], output: ["debga", "faedcb", "acgeb", "fgdeb"] },
  { input: ["ecdga", "gfab", "adcgb", "cgdebaf", "edcbfa", "ab", "bfcdg", "fcegdb", "acbdfg", "bda"], output: ["dcaeg", "egdfabc", "dagbfc", "dgabcf"] },
  { input: ["agb", "cbgea", "bafcdg", "edbgfca", "egacfd", "edbac", "gacef", "bg", "gefb", "fbaecg"], output: ["aegcbf", "decfga", "gb", "aecgf"] },
  { input: ["bfdec", "fegacd", "cfdbeag", "dba", "fadcb", "bdfgae", "cagdfb", "ab", "acbg", "dcgfa"], output: ["efcdag", "cabg", "geadbf", "ab"] },
  { input: ["dfegcab", "cba", "facdbe", "agdcf", "egadb", "aebdgf", "cb", "cbdga", "gceb", "bedacg"], output: ["bac", "cgeb", "cb", "cfaedb"] },
  { input: ["dfbeg", "cdgabf", "efgbad", "cgfbdae", "dbfag", "eg", "beg", "fdebc", "gbacde", "efag"], output: ["gbe", "eg", "bgfde", "gafe"] },
  { input: ["dafce", "ba", "fbeagcd", "acdfb", "abce", "dab", "cedgaf", "fcedab", "cdbgf", "efgabd"], output: ["bcgafed", "beac", "abd", "caeb"] },
  { input: ["efadg", "ebfag", "cbaf", "bga", "bfecgd", "ab", "gcebda", "becgf", "eagfcbd", "aegcbf"], output: ["dcfegb", "fbgea", "cagdebf", "cefdbg"] },
  { input: ["fgae", "fcgdab", "dacgbef", "gdfba", "eba", "ae", "bdcfe", "befda", "gdabef", "ecbdag"], output: ["afdgecb", "efadb", "daegfbc", "gdfba"] },
  { input: ["edbfac", "begcfd", "acedbg", "decbf", "ead", "ae", "cfedbag", "ebfad", "bdfag", "afec"], output: ["cfae", "dbcef", "ade", "debcgf"] },
  { input: ["bgf", "gcaef", "fgceb", "fgcebad", "fedbc", "gb", "dcegfa", "abcfgd", "geab", "bcgafe"], output: ["fdcabge", "abeg", "fbcagd", "gb"] },
  { input: ["fgbcea", "bdf", "bdcae", "gdef", "fd", "ecfdb", "agfbdc", "fbdaecg", "fgdbce", "bcefg"], output: ["cbgfe", "gefd", "fedg", "fbecg"] },
  { input: ["edcabfg", "fbagce", "ac", "dcagb", "agc", "gbecd", "gdfab", "fbdecg", "dbcgea", "dace"], output: ["fdagb", "deac", "adec", "cga"] },
  { input: ["eafcbg", "dbage", "egbcdf", "eagbfdc", "gf", "edbfc", "gfb", "cafbde", "fdgeb", "gdcf"], output: ["ebdga", "fbegd", "fg", "fbg"] },
  { input: ["ebgaf", "bdegacf", "cbgf", "dcfeab", "fbe", "bgaecf", "cegaf", "egbad", "bf", "eadfcg"], output: ["fb", "bf", "bcfg", "gcfea"] },
  { input: ["cagfde", "eag", "gbcde", "acfebgd", "bcefad", "cafg", "aedcf", "fbdega", "ga", "gcaed"], output: ["cfeda", "cagf", "ag", "dfcea"] },
  { input: ["bdaegf", "bfdgac", "feb", "defa", "abdgcef", "abgfd", "gbfed", "gfbcea", "ef", "dbceg"], output: ["edfbg", "ef", "dbfge", "fe"] },
  { input: ["cefgb", "cegdafb", "begcda", "agb", "fdacg", "ba", "agbcf", "gdefcb", "agcfeb", "ebaf"], output: ["cfegb", "ab", "gfadcbe", "dgbcfe"] },
  { input: ["feabd", "fc", "eafdcbg", "gcaeb", "efagdb", "cbaef", "dcbagf", "ebfcad", "fca", "dcfe"], output: ["cfaedb", "cbgae", "afc", "cdebfa"] },
  { input: ["degfcba", "cga", "bafgce", "eabgd", "dgcba", "bgdcf", "afdc", "ac", "gfbdca", "fegdbc"], output: ["bgcafd", "ca", "cag", "ac"] },
  { input: ["bcdefa", "gf", "fdeabg", "bgf", "bagce", "gabfe", "dgef", "dfcgab", "fdbea", "cfgdeab"], output: ["gf", "egdbaf", "afegb", "egfd"] },
  { input: ["fbed", "gefadc", "dfeabc", "bcdae", "adb", "db", "dfeac", "gbdcfa", "cgeba", "cfegabd"], output: ["cgfead", "dcaef", "dba", "edfb"] },
  { input: ["dfcbe", "gd", "dbafec", "cfgedb", "aecdfg", "cgafb", "cfebagd", "gdc", "egdb", "gcdbf"], output: ["dg", "dg", "gd", "dgc"] },
  { input: ["gcef", "gdbac", "cbadfe", "aedfgb", "ecfda", "ge", "bcdfaeg", "gae", "acedg", "ecgafd"], output: ["aeg", "cedfagb", "bacfged", "eagdc"] },
  { input: ["de", "ebcdfg", "acgebd", "abegc", "becafg", "edb", "abged", "afdgbce", "deac", "gbadf"], output: ["ecfagbd", "gabec", "gebda", "ed"] },
  { input: ["gadebc", "gcfdaeb", "abefg", "ebgcdf", "eagfc", "ab", "gfbed", "eab", "fbda", "baegdf"], output: ["edcbga", "abe", "baecfgd", "dbaf"] },
  { input: ["bfced", "fe", "bfdac", "fgadbe", "fed", "cdgbe", "fcea", "adgfcb", "dfebagc", "adbfce"], output: ["feac", "adcegfb", "gbfdae", "edgafbc"] },
  { input: ["fgcbda", "abfdge", "dbe", "acfbe", "gdcebfa", "dgef", "ed", "gadfb", "gecdab", "eabdf"], output: ["dbgace", "febda", "fdgbac", "dgebac"] },
  { input: ["fabgce", "fceda", "faebdgc", "bfecd", "gbdfea", "degbfc", "bcdg", "bfd", "bd", "bcfeg"], output: ["egbcf", "dfb", "gcefabd", "degcbf"] },
  { input: ["bfdga", "agdbef", "cafdg", "dacgbfe", "afb", "aebd", "bcefga", "fdgeb", "ab", "decgbf"], output: ["fdecbga", "ab", "bfa", "fbegd"] },
  { input: ["cbg", "cfdb", "fabecgd", "acbged", "acefg", "gdbfae", "cb", "dfbegc", "cfegb", "egbdf"], output: ["gdacbe", "gbfed", "cb", "dfbc"] },
  { input: ["cabfeg", "ebdcfag", "cb", "dagfbc", "cgfda", "fbcd", "gcb", "ebgda", "dfcega", "gcabd"], output: ["cdgafb", "dabeg", "fdgcab", "adcfg"] },
  { input: ["gfdba", "fcabd", "ca", "bac", "fbecag", "abdecf", "cead", "acgebfd", "bedcgf", "cebdf"], output: ["dcafb", "gbedcf", "facgedb", "acb"] },
  { input: ["gecfbda", "febda", "fg", "cgdf", "ebacfg", "gbfde", "cdeagb", "gebdcf", "gecbd", "efg"], output: ["egfdbc", "gcefba", "dgbfe", "adcgefb"] },
  { input: ["egbdf", "ged", "aefgb", "dgfbec", "abfgdec", "cbgdf", "de", "gcadef", "dfgacb", "edbc"], output: ["dfecag", "de", "gaecfdb", "gcefda"] },
  { input: ["gacfe", "fbdegca", "gdfb", "edg", "baecfd", "befad", "aedfg", "bdgcae", "gd", "befgda"], output: ["bafegd", "dgfb", "bdafe", "daebgf"] },
  { input: ["becgda", "gd", "gbed", "bdefgac", "agfbce", "cbage", "dcfbga", "edfac", "agedc", "dgc"], output: ["cgbea", "dgc", "gfabedc", "cdg"] },
  { input: ["dgfec", "gaf", "efca", "gcadbf", "faedg", "efdbagc", "af", "ecfbdg", "gacfde", "abged"], output: ["ecfbgd", "cfegd", "fa", "bgdefc"] },
  { input: ["ecbadgf", "efbg", "egcdbf", "edf", "dcgfb", "ecdag", "ecfbda", "agbcfd", "dgcfe", "ef"], output: ["gcefbd", "efacdbg", "acfegbd", "ebgf"] },
  { input: ["dacfbg", "aegfdc", "fcgeabd", "dcgaf", "fdae", "fe", "bgeac", "dbfegc", "aegcf", "ecf"], output: ["fe", "afceg", "fe", "gecdfb"] },
  { input: ["bfdeg", "feagbd", "aebd", "gab", "fdcgeb", "begaf", "ba", "cdeafgb", "ecafg", "dbfacg"], output: ["cgfbda", "agcef", "afgbe", "egcdfb"] },
  { input: ["bafgc", "cgefa", "bdgfa", "gbdacf", "bc", "agefdb", "gbc", "gcfdeba", "bfdegc", "bcad"], output: ["dafbg", "dfbcge", "dagfbc", "fcega"] },
  { input: ["dbfge", "defbga", "gbeadfc", "bgdcfe", "bc", "gceda", "bcg", "cbdge", "gcaefb", "dbfc"], output: ["cb", "bgc", "egfdcb", "abegcf"] },
  { input: ["fda", "fadce", "da", "ebcaf", "adcb", "feacdb", "gedfc", "cbadgfe", "egabdf", "beafgc"], output: ["egfbda", "bfcgade", "ad", "feabgcd"] },
  { input: ["dcegf", "eafdcg", "afgc", "fgbeacd", "debcf", "abgedf", "gec", "eadgcb", "cg", "dgeaf"], output: ["fcged", "afcgde", "dgcafe", "gdbecfa"] },
  { input: ["becafd", "dafcgbe", "feb", "fgab", "fdebg", "bf", "abdefg", "gefacd", "gafed", "bedgc"], output: ["faebdc", "fdagbe", "dgebf", "bgfa"] },
  { input: ["cfgbeda", "ba", "fgacd", "bfca", "ecgfad", "bga", "gdacbf", "bdgac", "bdgfea", "cgbde"], output: ["adfcge", "gfadce", "ab", "bga"] },
  { input: ["dcbgafe", "defga", "edabcg", "cadefg", "dbegf", "facge", "fbaceg", "ead", "da", "fcda"], output: ["begfd", "gfbcae", "dae", "eda"] },
  { input: ["acebg", "bac", "dfbgca", "dcega", "cefabg", "ba", "fgdcbe", "aebf", "fcbdage", "fbcge"], output: ["cdagfeb", "efba", "abc", "ab"] },
  { input: ["acfge", "gde", "gecfbd", "ecgabf", "dg", "daceg", "acdegf", "gfda", "cadbe", "eacfdbg"], output: ["dacfge", "fgabec", "gde", "fecgdab"] },
  { input: ["edagf", "cfdge", "adf", "bdacfe", "ecdgfb", "cagd", "ad", "fdeacgb", "egfba", "gcfeda"], output: ["fdaeg", "efgab", "ceagfd", "efbga"] },
  { input: ["fgabd", "efbdg", "gfcde", "ecfadg", "dfcegb", "gecbad", "be", "efcb", "deb", "acefdgb"], output: ["ecfgd", "bcef", "deb", "dbgaf"] },
  { input: ["abgcf", "dgebaf", "eg", "gfbadc", "ega", "cadfe", "fgaec", "gdceafb", "egbc", "fbagce"], output: ["bgafce", "afecdbg", "fcdae", "egcb"] },
  { input: ["dbaecgf", "abdcgf", "dgbfce", "gdbef", "gcf", "dabegf", "ecfb", "dfcge", "caedg", "cf"], output: ["gdefc", "cgedfab", "gdcbaf", "efcb"] },
  { input: ["gcdeab", "acgbfd", "cefdg", "fbcgd", "aefbgdc", "bcafg", "fcabeg", "db", "bcd", "dbaf"], output: ["cfedgab", "bdfa", "cbd", "cdgbea"] },
  { input: ["gb", "fedbga", "gbdaf", "fbcda", "bdcgea", "gbd", "facdge", "gdceabf", "agefd", "gfeb"], output: ["bgecfda", "gbefda", "gbd", "gcfabed"] },
  { input: ["dga", "defcbg", "eabg", "dbcgfa", "ag", "ebcfagd", "gbecad", "fecad", "cegdb", "cegda"], output: ["acbdfg", "cedagb", "ga", "gdeca"] },
  { input: ["gcefba", "dabgf", "dcgaefb", "cafgd", "aedcg", "dcef", "cbgade", "dgaefc", "cf", "afc"], output: ["cedf", "gbaced", "fc", "dfgecab"] },
  { input: ["egcfa", "fbgce", "cbdfgea", "fgadce", "cbdagf", "edafg", "cade", "ac", "beagfd", "cag"], output: ["fgbce", "febdgca", "adbgfce", "gac"] },
  { input: ["gfdce", "eabdcfg", "ag", "gcadf", "agd", "cdfbge", "acge", "bdafc", "gefabd", "cedagf"], output: ["gad", "gace", "gfeadb", "afdgeb"] },
  { input: ["dfbgea", "bdcea", "cgd", "bcfg", "gc", "cafdge", "bfdga", "bagcfd", "gbdac", "abdgecf"], output: ["cgdab", "dbfage", "fbcg", "dgc"] },
  { input: ["cabdf", "bac", "cdgfa", "cdeb", "eabdf", "fedabc", "adfbecg", "dgefab", "ceafgb", "bc"], output: ["fcabde", "bc", "bdcfa", "abedcf"] },
  { input: ["fedacbg", "gb", "bgf", "bdag", "fecbda", "dacbf", "cbgfa", "agfce", "bcfadg", "dfcgeb"], output: ["gbf", "dbfcea", "gb", "cbfad"] },
  { input: ["fbdcea", "bcafe", "fac", "cfdbe", "cfbdeg", "adgbefc", "fgdcab", "af", "feda", "bgcea"], output: ["cfa", "bfcaed", "cebdaf", "cafdeb"] },
  { input: ["agf", "cdefg", "gcaebf", "cgbdea", "af", "begadf", "badf", "agfed", "dagebfc", "bdeag"], output: ["fcegbad", "gbecda", "begda", "fegbad"] },
  { input: ["fbdec", "gfbace", "egdfb", "acbfe", "dcba", "dcf", "gfdbcae", "decfag", "aecfdb", "dc"], output: ["dgfcbea", "cd", "acfegb", "cdefab"] },
  { input: ["ecfbag", "ba", "gab", "afbc", "cgabe", "bdecagf", "fecag", "facdeg", "bafged", "cebgd"], output: ["fegadb", "gba", "cfage", "ba"] },
  { input: ["abfeg", "bdgfe", "fbecda", "cbfdage", "egcbd", "gfcd", "efd", "gfcdbe", "df", "eabdcg"], output: ["gfdbe", "dgecfb", "bdgeca", "eafgdcb"] },
  { input: ["fc", "debfa", "cfb", "ebgcfa", "fgcbde", "acfbe", "dbgeac", "gaceb", "egabfcd", "facg"], output: ["cgbae", "fc", "cgbaed", "cbf"] },
  { input: ["ac", "egbfcda", "gfbec", "gecbfa", "dgfceb", "fcba", "dcbaeg", "faegc", "gac", "fdeag"], output: ["gcbfae", "ceabfdg", "eadgcfb", "ca"] },
  { input: ["bga", "cefbgda", "gfebc", "eafgbd", "edgacf", "cdba", "fcadg", "bagcf", "ab", "adcfgb"], output: ["bgcafd", "gdbfac", "dbcgaf", "dcab"] },
  { input: ["bdecfa", "gbcfa", "bec", "fbaed", "cbeagd", "cabef", "ec", "egcfbad", "aegbdf", "dcfe"], output: ["edgabf", "cabegdf", "ec", "bdcagef"] },
  { input: ["gcfaedb", "bfgec", "ceagb", "cbdfeg", "eabdcf", "fg", "fbcde", "gef", "gcdf", "bgafed"], output: ["egf", "fgdc", "dcgaefb", "gfceadb"] },
  { input: ["edag", "dab", "fbdag", "da", "gdcfb", "edacbf", "efagb", "abdecgf", "agebdf", "bagecf"], output: ["deag", "fgdba", "bfeagdc", "efacgb"] },
  { input: ["dgfc", "fgcbde", "edbacf", "abgef", "fdgbe", "fd", "dbf", "egcdb", "fbcdage", "aegdcb"], output: ["cbdeg", "bcegd", "cbdage", "bdf"] },
  { input: ["bcgfda", "dcbef", "facgeb", "gadcef", "bfg", "afdgc", "adgb", "gdfcb", "eacdbfg", "gb"], output: ["cbdfaeg", "abgcedf", "egacbf", "becdfga"] },
  { input: ["gafdbc", "gcefa", "egacb", "gaedbc", "fga", "fa", "faebcdg", "befa", "ecdgf", "gecbfa"], output: ["egcab", "facdbg", "edgfc", "af"] },
  { input: ["fagcde", "gef", "eagdf", "gfdba", "cdgfeb", "fadgbec", "eg", "badcef", "dfcea", "geca"], output: ["gdabf", "fge", "adegcbf", "aebdgfc"] },
  { input: ["bedgaf", "cb", "fcabdeg", "dcebfa", "bacgde", "cba", "gcafe", "dabef", "cbeaf", "fcbd"], output: ["cb", "bdfc", "dafebc", "gceadfb"] },
  { input: ["dafbe", "gf", "abdfg", "dfbeagc", "bgacd", "ebfg", "dbcefa", "dafgce", "gdf", "beadfg"], output: ["gcdba", "cfegad", "cdfabe", "fgbe"] },
  { input: ["dfabg", "cfbedg", "dgcbafe", "dceba", "fdagbc", "aedgbf", "fgca", "gc", "dcbag", "bgc"], output: ["dfgceb", "cgb", "cbaedgf", "cgefbad"] },
  { input: ["bdfegca", "dcegb", "fadc", "fegadb", "df", "cfbed", "bfd", "cbegaf", "fecba", "afbedc"], output: ["gfacbde", "efbagd", "egcdb", "fbegdca"] },
  { input: ["bcfgea", "eacgf", "cbdeaf", "fbedgac", "gbcfa", "gcadef", "baf", "gbcfd", "bgea", "ab"], output: ["gaecbfd", "dgfbc", "afb", "dfaebc"] },
  { input: ["bafdgce", "cbgd", "facde", "bgcaf", "dbacf", "db", "acgfdb", "aecgbf", "abd", "ebdgfa"], output: ["fadce", "abd", "feacd", "bdcg"] },
  { input: ["fgcdab", "efcadg", "gfdaebc", "aebfc", "bdefgc", "cefgb", "eg", "dgbe", "cgbfd", "feg"], output: ["eg", "cegfbd", "dabfgc", "eafcb"] },
  { input: ["acge", "bedgafc", "edafgc", "dfgbac", "ca", "bfdega", "cdeaf", "acf", "fdceb", "fdeag"], output: ["facde", "fac", "ca", "ecfadgb"] },
  { input: ["dbagf", "fbdeag", "cgbfad", "dbc", "gcfb", "gbdca", "bc", "ecdbaf", "egacd", "fedcbag"], output: ["cb", "cdbfega", "dbc", "fbcg"] },
  { input: ["dceab", "egfdacb", "bfc", "gfba", "fcagd", "bdcagf", "aefdcg", "cdafb", "decgfb", "fb"], output: ["cbedfg", "acgdf", "bacfd", "fbc"] },
  { input: ["fea", "bgcfe", "af", "fceag", "gbfdea", "fcagbed", "dgafec", "cfad", "dabgce", "aedgc"], output: ["fea", "gceda", "edcagf", "fabdge"] },
  { input: ["dfbga", "abfdce", "fdb", "bdeg", "adebfg", "gcafb", "daegfbc", "bd", "gcadef", "adfge"], output: ["efdagb", "afgecbd", "fdgab", "bged"] },
  { input: ["aebfgd", "gedfa", "bf", "afb", "ecfgda", "fgbe", "aegfcbd", "agdbf", "abdefc", "bcdga"], output: ["gcbfaed", "fadeg", "bf", "cfdeab"] },
  { input: ["cgaebd", "cg", "faecgbd", "fbagd", "bfecad", "aceg", "gefbdc", "bcdea", "cdg", "bcdga"], output: ["gcabd", "cdg", "dabgc", "gbfad"] },
  { input: ["deb", "cfgdeb", "acfebd", "fgbce", "fbdgcea", "bdfg", "cdgae", "dbgec", "bd", "fbaegc"], output: ["ecbfg", "gabefc", "fgecbad", "deb"] },
  { input: ["fca", "dfeab", "bacfgd", "gfdec", "ac", "debcfa", "caeb", "abfedg", "efadc", "bacedfg"], output: ["fadce", "bdaefg", "dbfgca", "dfgbaec"] },
  { input: ["cegfb", "egabd", "df", "adfcgbe", "efd", "bfedga", "feagdc", "bfad", "acbgde", "bfdeg"], output: ["daegb", "fd", "efdgb", "bafd"] },
  { input: ["agcedb", "egbacdf", "defgcb", "aefcgb", "gecab", "cfg", "acbfg", "bfcda", "gfea", "gf"], output: ["adgcfbe", "cdeabfg", "fg", "fgae"] },
  { input: ["fedcb", "caedf", "fgdacb", "dgefb", "cbf", "bc", "gefabd", "gfcaedb", "edgfbc", "bcge"], output: ["cbf", "fcaed", "fbc", "fcebd"] },
  { input: ["decaf", "bgfe", "dfage", "abgcdfe", "bfagd", "gde", "ge", "becagd", "bfgade", "bcdgaf"], output: ["ged", "edg", "fegadb", "gedaf"] },
  { input: ["ecgbf", "fabedc", "ebdcf", "fgc", "gc", "gfdbcae", "cbdfag", "dcbefg", "cegd", "ebgfa"], output: ["cbadfeg", "fgabe", "feagb", "cfg"] },
  { input: ["gfdca", "cgdfe", "gdecfa", "acg", "deag", "befadcg", "agefcb", "ag", "dbafc", "ecbdfg"], output: ["gac", "agc", "edgfc", "acgdf"] },
  { input: ["adfcbe", "fdaec", "agbefc", "gcfed", "cg", "cge", "fgeadc", "bfdge", "gcbefda", "gadc"], output: ["facged", "cg", "acfde", "cge"] },
  { input: ["edcf", "cbfged", "cbadg", "bfegcda", "eadbfg", "gfc", "bfgdc", "cf", "dfgeb", "ecbgfa"], output: ["fc", "caegbdf", "abedfg", "aebgfc"] },
  { input: ["edbafgc", "fegdba", "gadeb", "cdbega", "bfa", "afbcge", "bdfe", "fdagc", "fb", "fagbd"], output: ["dcgbeaf", "agdceb", "bfecag", "bedf"] },
  { input: ["bdefgc", "bcadg", "dfgba", "bedgafc", "edcbga", "cd", "gcd", "acfgbe", "baceg", "dcea"], output: ["gcbadef", "daec", "bdgac", "ebgac"] },
  { input: ["cbdag", "dbeg", "dbgafec", "gd", "cgd", "gabdec", "fcebad", "gbcaf", "eadgcf", "bacde"], output: ["dg", "gdbe", "dbgace", "cgd"] },
  { input: ["dfaebg", "dacgb", "bgfda", "egdbc", "gca", "aecbfg", "dcgfba", "ca", "dafc", "becfdga"], output: ["bdcag", "ca", "ceadbgf", "dfac"] },
  { input: ["edagc", "adbefg", "dgfae", "befcda", "gef", "dfcgeb", "bedfa", "afgdbce", "gf", "afgb"], output: ["afdbe", "bfdgcae", "afbg", "gbfa"] },
  { input: ["dgecba", "fegd", "begafc", "egfca", "agd", "fdgac", "aecfdg", "decfgba", "fdbca", "gd"], output: ["abcfd", "gad", "afcbeg", "gd"] },
  { input: ["ceabgf", "fbeg", "bacge", "fcedag", "adcbg", "eg", "gfecadb", "gce", "fbcae", "fcebda"], output: ["defagbc", "cge", "ecagfb", "ge"] },
  { input: ["aed", "fbcde", "aefbgcd", "beac", "gdfba", "ae", "beadf", "fbcaed", "cefadg", "fbcegd"], output: ["ae", "dbefcga", "efbad", "ade"] },
  { input: ["dbeagcf", "cdgae", "abfedg", "gfbdca", "cgb", "gdcbfe", "cdagb", "fbdag", "bc", "bacf"], output: ["bc", "abcf", "cgb", "bcg"] },
  { input: ["afbegc", "dabfec", "cega", "dbfag", "acfeb", "facgb", "gbc", "bgefdc", "efbacdg", "gc"], output: ["egcbdf", "bgc", "edcfba", "bgc"] },
  { input: ["bcfe", "cdegf", "bgedcf", "bdfga", "ebgcadf", "bfged", "cgedba", "eb", "egacdf", "dbe"], output: ["febc", "cebf", "bacedgf", "fbgedc"] },
  { input: ["fgaeb", "cbg", "bafcdg", "gc", "afgdeb", "eafdcbg", "bceda", "egcf", "acbgef", "cgbea"], output: ["gbc", "gcef", "cbg", "fadgbc"] },
  { input: ["cadbge", "dacfeg", "bfdage", "dgcab", "abfdc", "edgac", "gcbe", "bg", "gecadbf", "dbg"], output: ["bg", "beadgfc", "cbge", "gb"] },
  { input: ["geadfbc", "aegcdb", "cfdgb", "ebcgd", "ecdf", "fdgecb", "fbc", "dbafg", "geafcb", "cf"], output: ["decf", "cf", "fc", "cf"] },
  { input: ["cfagdb", "fbgac", "cgeaf", "dgfaebc", "faebgd", "ab", "bcad", "baf", "fgbcd", "bfcged"], output: ["cgdfb", "fgcbd", "ab", "dbca"] },
  { input: ["fcedg", "dbgcfea", "bg", "bgdf", "fagedc", "efdbgc", "bge", "aecbd", "bcgeaf", "dbegc"], output: ["geb", "dfbg", "bfdg", "cdbea"] },
  { input: ["defag", "bfcgde", "ea", "abfceg", "efbdg", "abde", "fae", "eagbdf", "aebdgfc", "cfgda"], output: ["fea", "cdbfge", "gdcefba", "fdgbe"] },
  { input: ["abef", "af", "gaf", "begfd", "dbfgec", "egcad", "aedfg", "bdaefgc", "bfcagd", "dfegab"], output: ["agebfd", "cfabdg", "ebaf", "afeb"] },
  { input: ["dfecab", "bcdae", "eabfg", "gc", "gdcfba", "agbce", "cbaged", "gedc", "cgb", "ebdafgc"], output: ["cdeg", "cdegab", "dbeca", "gaceb"] },
  { input: ["dbeg", "fecdg", "dcgafe", "abfec", "fdbec", "cfgdeb", "db", "bcgfad", "dbc", "dfgaebc"], output: ["efdbc", "gfdacb", "bdc", "ecfba"] },
  { input: ["dfecbg", "gdb", "gcde", "bfdce", "fgbeda", "gcdabfe", "dg", "gfcbd", "gcfba", "acefbd"], output: ["gd", "efcagdb", "gacbf", "dbefac"] },
  { input: ["fgedac", "dbaec", "febadc", "ag", "egbfc", "age", "agedbc", "dbag", "gfbcaed", "beacg"], output: ["gae", "ga", "bgad", "adbfec"] },
  { input: ["agfedc", "bdcefg", "fcdag", "gfc", "fbagd", "fdace", "agce", "cebdfa", "fcdegab", "cg"], output: ["decfa", "fedca", "efadcb", "fcg"] },
  { input: ["edcag", "ae", "gdecf", "ecba", "gaebdfc", "cdfbag", "daebfg", "eag", "gecbad", "abgcd"], output: ["dcafebg", "ae", "egdabfc", "ae"] },
  { input: ["fbdeag", "daebc", "dgbfac", "fabeg", "aecbgf", "dgfe", "fd", "daf", "bdecgaf", "adefb"], output: ["dbaec", "edfg", "gfed", "aegbf"] },
  { input: ["efacdg", "geb", "be", "gefdba", "eabf", "gcdeafb", "agbecd", "gedbf", "bgfdc", "daefg"], output: ["efba", "gbe", "bfgcd", "degaf"] },
  { input: ["decagfb", "gcfbe", "abdge", "adbgfc", "defagb", "gbdaec", "fdae", "fbgea", "abf", "fa"], output: ["beadgf", "aedf", "bgade", "efda"] },
  { input: ["aedf", "bagdfce", "cagfdb", "dfgac", "ecfgbd", "fe", "fec", "geabc", "cgeaf", "gafced"], output: ["dcagf", "fgebcd", "cef", "bcefadg"] },
  { input: ["dc", "dagbec", "fgbadce", "cde", "dacf", "dfecb", "fbgde", "afcgeb", "fbcdae", "fbeca"], output: ["bcfde", "cd", "dc", "befdg"] },
  { input: ["ebac", "ebfacgd", "cfe", "ec", "agbfe", "egcfab", "fgdcb", "cefgda", "egafbd", "ebfcg"], output: ["cdgfb", "gdcafe", "bfgce", "acfedg"] },
  { input: ["febdgc", "cge", "fcdgea", "dbgcefa", "bcdeg", "egadb", "dgacfb", "bdfgc", "ec", "ebfc"], output: ["dfbgc", "adbfgc", "cefdga", "ce"] },
  { input: ["cbd", "afgcedb", "gefdc", "cbgde", "aecdbg", "afcdbe", "bd", "gabce", "gadb", "gafceb"], output: ["bcega", "ecgdb", "ceabfgd", "gebdc"] },
  { input: ["dfabc", "agcfb", "gaf", "gf", "cebdfa", "adfbge", "gcafdeb", "cdfg", "bgcea", "cbfgda"], output: ["gabcf", "ecbdaf", "dcfg", "fg"] },
  { input: ["adebcf", "baefd", "dbegfc", "gedbacf", "fdbce", "fda", "fgbcad", "af", "afec", "gabed"], output: ["feac", "ebfdgc", "ecgbfd", "gdeba"] },
  { input: ["cgebad", "ad", "fcdbag", "dfca", "fbdge", "cabfg", "fgabd", "dag", "acfgbe", "eacfbdg"], output: ["da", "dag", "fgcba", "ad"] },
  { input: ["bface", "cfbgaed", "abegdc", "gdecbf", "aed", "dfeba", "da", "dgfa", "bdegf", "bafgde"], output: ["dgfa", "ead", "fgad", "dbfgec"] },
  { input: ["afgbdce", "ebacf", "abde", "ed", "ced", "cadfg", "gcdfbe", "ecdbaf", "faecd", "fegabc"], output: ["ed", "bade", "de", "afdceb"] },
  { input: ["aeg", "cdbaeg", "cabdfe", "fgceb", "cafgde", "ga", "adgb", "efdagbc", "eacbd", "cbaeg"], output: ["cdaefb", "defgbac", "gae", "dagb"] },
  { input: ["ebgf", "bcdef", "bgdca", "ebafdc", "dfegcb", "gf", "dgbcf", "cfg", "cdebgaf", "caefgd"], output: ["cbeadgf", "fg", "fg", "cgf"] },
  { input: ["aeg", "afcde", "cagbfe", "gcbdfa", "bafgc", "edbcgaf", "bfge", "acgfe", "ecdbag", "ge"], output: ["ge", "ecadbg", "ecabgd", "ega"] },
  { input: ["gfdca", "fabcg", "eafcbdg", "efdgc", "fead", "egacfd", "bdaecg", "da", "cefbdg", "dga"], output: ["feda", "agd", "cdgabe", "bcafdge"] },
  { input: ["gb", "fgbedc", "fegba", "efagbc", "bgca", "fbdea", "cegfa", "fbcadge", "aedcfg", "fbg"], output: ["fcgbde", "bfage", "cgfea", "fbecgd"] },
  { input: ["gbf", "acgdef", "dfabceg", "ebfadg", "bgde", "gfead", "cgeafb", "fabgd", "dcabf", "bg"], output: ["fdcgaeb", "bg", "gbfcea", "afbgd"] },
  { input: ["begcf", "baec", "cafgb", "fedcgba", "bag", "ba", "fadcg", "dfabeg", "afgbce", "fdbecg"], output: ["bcadfeg", "gcfeb", "daegfb", "caeb"] },
  { input: ["badgfc", "fdagecb", "bgefa", "egcab", "agbfed", "fa", "eafd", "dbfge", "dcbgfe", "fag"], output: ["agf", "fa", "faed", "fga"] },
  { input: ["cdabgf", "decbg", "gceabf", "dfeabg", "gfeab", "adb", "dafe", "bedga", "cadfbge", "da"], output: ["gebdfa", "aefgb", "deaf", "cbedg"] },
  { input: ["dfaceg", "fabecdg", "gedf", "gbecaf", "de", "fcaeg", "dec", "cabfd", "adbecg", "fdeac"], output: ["ed", "cbfda", "de", "cbfeag"] },
  { input: ["cgbeaf", "afbdc", "bega", "gbfec", "cea", "dcfgbe", "ae", "dbaegfc", "ebacf", "cegfad"], output: ["gbae", "cefba", "aec", "gfbec"] },
  { input: ["gb", "fgdeac", "efcbgda", "eacbd", "agefd", "gab", "edbag", "ebfgac", "dgfb", "edabgf"], output: ["bfgd", "egdaf", "gb", "bag"] },
  { input: ["fdeabcg", "gabdfc", "gedfca", "dabce", "cfge", "edbagf", "geacd", "dafgc", "ge", "ega"], output: ["gfce", "ega", "dfcbga", "gfedacb"] },
  { input: ["ad", "dca", "cefgba", "abfcdg", "dcbaeg", "dbea", "ecabg", "gdcea", "egfcd", "gafbcde"], output: ["egfdcab", "gcdfe", "bgeca", "adc"] },
  { input: ["efag", "eba", "ea", "abfecd", "cedafgb", "gabfec", "fgbdac", "gcdeb", "acfbg", "gceab"], output: ["bae", "ecgab", "cbfga", "gfae"] },
  { input: ["bafcged", "cfb", "dfgba", "afbdec", "cf", "daebc", "bcgfde", "dagbec", "dfcba", "caef"], output: ["cf", "agbdf", "caefdbg", "fegdcb"] },
  { input: ["cgfadbe", "dceabg", "adebg", "fgaedc", "gcba", "dca", "ca", "aedbc", "geafdb", "dbfce"], output: ["cdabe", "gbdea", "eacfgd", "adc"] },
  { input: ["eabfdg", "cfbdag", "eb", "bfdeca", "feb", "dfcbgae", "ecgfa", "aebfc", "dfcba", "edbc"], output: ["ecfba", "cafge", "be", "be"] },
  { input: ["ea", "agbde", "cfbdea", "egdbfa", "abdgfc", "ead", "bfdga", "cbged", "agfe", "begcfad"], output: ["baecdf", "adgeb", "ade", "cbfadeg"] },
  { input: ["dabfe", "fgceab", "adfec", "aegdb", "dacegb", "fgdb", "bef", "fb", "dafbeg", "bedfcga"], output: ["abdge", "abgfde", "gfdb", "eagfdb"] },
  { input: ["badcf", "egabdc", "gb", "efdgac", "gedafb", "gab", "fecadgb", "bgfe", "gefda", "abgdf"], output: ["bag", "cbedag", "agdfceb", "dfcba"] },
  { input: ["aedgcf", "eacdbg", "ea", "gae", "efcgd", "gcfab", "gcebdf", "facebgd", "gfaec", "faed"], output: ["afde", "eafd", "ceadgb", "cdbage"] },
  { input: ["gbeadc", "fgdac", "ecagf", "ebfg", "abdfce", "cgfeab", "ge", "dcgbafe", "aefcb", "ecg"], output: ["ebdgca", "gec", "ecbdaf", "dbafec"] },
  { input: ["cdfbe", "adcfeb", "acf", "efgbcad", "ac", "dbca", "befgcd", "gabef", "geadcf", "febac"], output: ["ac", "eacbdf", "caf", "ecdgbf"] },
  { input: ["aebcfd", "cdabe", "abfeg", "defbagc", "bfgced", "fd", "gbcdea", "edf", "fbdae", "dacf"], output: ["dcaeb", "cfad", "dfe", "fcad"] },
  { input: ["bdcg", "fecdba", "bd", "cagbef", "dgeab", "dbgcfae", "ebd", "gedcab", "eafdg", "gcaeb"], output: ["cgbd", "aecgb", "bgcea", "ebd"] },
  { input: ["bcgfe", "dbecgf", "fgdbae", "afgce", "bedcf", "dbeafc", "bg", "cgdb", "cadgefb", "beg"], output: ["ecdfgb", "daegfb", "bfegc", "ecfbg"] },
  { input: ["ebgcd", "bdecgfa", "gdabfe", "gedfc", "dgf", "fg", "edabcf", "gcaf", "eafcdg", "cfeda"], output: ["dbaecf", "efgcd", "fgedab", "fgdeac"] },
  { input: ["gefcdb", "gfadb", "ea", "efgab", "gcefb", "gea", "cefa", "ecgfabd", "bcafge", "egcbda"], output: ["egacdbf", "gebfc", "ae", "ega"] },
  { input: ["cgedfa", "afd", "febagdc", "fadeg", "dgcabe", "cabgfd", "acef", "febgd", "cadge", "fa"], output: ["afedg", "fa", "abgced", "gacbdf"] },
  { input: ["dbcfea", "dc", "dbgae", "egdafb", "bcd", "debgc", "cgad", "egbdafc", "gcebad", "befgc"], output: ["cd", "dcb", "dgca", "decbg"] },
  { input: ["ecfad", "aegcbf", "fedcbg", "edcbf", "gaedcb", "fb", "cgeadfb", "gfdb", "edgbc", "cbf"], output: ["becafg", "bgdf", "fcb", "gecdbf"] },
  { input: ["ce", "dgecbfa", "bgdac", "gdbfae", "dce", "egdac", "acdebf", "cfeadg", "adgfe", "fgce"], output: ["gaefbcd", "degca", "fcge", "dbcfeag"] },
  { input: ["fd", "eafcgb", "cdf", "dcfgeb", "fabce", "gecda", "dbfa", "beagcfd", "deacf", "abfcde"], output: ["defca", "fecab", "cdaeg", "ecafb"] },
  { input: ["cagdbf", "ca", "gacfd", "cgbeafd", "dcfge", "gac", "gafdb", "dacb", "bfgaed", "becgaf"], output: ["gcdfa", "cdab", "dcab", "bcfeagd"] },
  { input: ["gadef", "aedgc", "afg", "gcadbf", "ecdfag", "fg", "gfbdeac", "adfeb", "cfge", "bgaedc"], output: ["gf", "bdcgfa", "dgafe", "bgcdaf"] },
  { input: ["agdc", "gefdbc", "dbgfa", "ad", "cfbaed", "dba", "cbdfg", "ecfdagb", "egfba", "dgcabf"], output: ["ecdgbf", "ad", "bgcdef", "abgef"] },
  { input: ["cadgfb", "bcgfae", "fedac", "efbca", "cagfb", "eb", "bae", "ecdgabf", "begc", "agbedf"], output: ["gedafb", "acbdegf", "aeb", "eb"] },
  { input: ["cdega", "efbg", "fbdegac", "ef", "daebcf", "badefg", "efagd", "efd", "fbgdca", "dbfga"], output: ["fadecbg", "bgfe", "ef", "agdbf"] },
  { input: ["fbedca", "fbcgaed", "dabcg", "fgdbce", "cgfadb", "gb", "bdafc", "afbg", "dgcae", "dbg"], output: ["bdacgf", "adfbc", "bg", "gedca"] },
  { input: ["befcg", "edb", "bgadfc", "dfaecb", "cdfegab", "egda", "agcdbe", "ed", "dcabg", "debgc"], output: ["geda", "aegd", "gcdafb", "dcefab"] }
];

function countWellKnownDigits(entries: Entry[]): number {
  return entries.flatMap(e => e.output).filter(o => o.length === 2 || o.length === 3 || o.length === 4 || o.length === 7).length;
}


// terse but hey it works lol
function decode(entry: Entry): number {
  const seven = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 3)[0];
  const one = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 2)[0];
  const four = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 4)[0];
  const eight = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 7)[0];
  
  // 9 has 6 segments and has all that 4 has
  const nine = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 6 && four.split("").every((fourCharacter => i.split("").indexOf(fourCharacter) >= 0)))[0];


  // 6 has 6 segments and is the difference of 7 and 9
  const six = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 6 && i !== nine && nine.split("").filter(nineCharacter => one.split("").indexOf(nineCharacter) < 0).every(remainingCharacter => i.split("").indexOf(remainingCharacter) >= 0))[0];
  // 0 is the remaining 6 segment
  const zero = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 6 && i !== six && i !== nine)[0];

  // 3 has 5 segments and has both of the 1 segments
  const three = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 5 && one.split("").every((oneCharacter => i.split("").indexOf(oneCharacter) >= 0)))[0];

  // 5 has 5 segments and has all that 6 has
  const five = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 5 && i.split("").every((fiveCharacter => six.split("").indexOf(fiveCharacter) >= 0)))[0];
  
  // 2 is the remaining 5 segment
  const two = entry.input.map(i => i.split("").sort().join("")).filter(i => i.length === 5 && i !== five && i !== three)[0];
  
  return entry.output.map(o => {
    switch (o.split("").sort().join("")) {
      case zero: return 0;
      case one: return 1;
      case two: return 2;
      case three: return 3;
      case four: return 4;
      case five: return 5;
      case six: return 6;
      case seven: return 7;
      case eight: return 8;
      case nine: return 9;
    }
  }).map(i => +i).reduce((acc,cur) => (acc * 10) + cur, 0);
}

function decodeAndSum(entries: Entry[]): number {
  return entries.map(decode).reduce((acc,cur) => acc + cur);
}

console.log(`expected: ${expected}, actual: ${countWellKnownDigits(given)}`);
console.log(`challenge: ${countWellKnownDigits(challengeInput)}`);

console.log(`expected sums: ${expectedSum}, actual: ${decodeAndSum(given)}`);
console.log(`challenge sum: ${decodeAndSum(challengeInput)}`);