type Point = {
  x: number;
  y: number;
}

const given = [
  [5,4,8,3,1,4,3,2,2,3],
  [2,7,4,5,8,5,4,7,1,1],
  [5,2,6,4,5,5,6,1,7,3],
  [6,1,4,1,3,3,6,1,4,6],
  [6,3,5,7,3,8,5,4,7,8],
  [4,1,6,7,5,2,4,6,4,5],
  [2,1,7,6,8,4,1,7,2,1],
  [6,8,8,2,8,8,1,1,3,4],
  [4,8,4,6,8,4,8,5,5,4],
  [5,2,8,3,7,5,1,5,2,6]
];
const expected1 = 0;
const expected2 = 35;
const expected10 = 204;
const expected100 = 1656;

const expectedSynchronized = 195;

const challenge = [
  [8,2,7,1,6,5,3,8,3,6],
  [7,5,6,7,6,2,6,7,7,5],
  [2,3,1,5,7,1,3,3,1,6],
  [6,5,4,2,6,5,5,3,1,5],
  [2,4,5,3,6,3,7,3,3,3],
  [1,2,4,7,2,6,4,3,2,8],
  [2,3,2,5,1,4,6,6,1,4],
  [2,1,1,5,8,4,3,1,7,1],
  [6,1,8,2,3,7,6,2,8,2],
  [2,3,8,4,7,3,8,6,7,5]
];

function printLevels(energyLevels: number[][]): void {
  for (let i = 0; i < energyLevels.length; i++) {
    let line = "";
    for (let j = 0; j < energyLevels[0].length; j++) {
      line += `\t${energyLevels[i][j]}`;
    }
    console.log(line);
  }
}

function handleFlash(point: Point, energyLevels: number[][]) {
  if (point.x - 1 >= 0) {
    incrementEnergyLevel({x: point.x - 1, y: point.y}, energyLevels);
  }

  if (point.x + 1 < energyLevels.length) {
    incrementEnergyLevel({x: point.x + 1, y: point.y}, energyLevels);
  }

  if (point.y - 1 >= 0) {
    incrementEnergyLevel({x: point.x, y: point.y - 1}, energyLevels);
  }

  if (point.y + 1 < energyLevels[0].length) {
    incrementEnergyLevel({x: point.x, y: point.y + 1}, energyLevels);
  }

  if (point.x + 1 < energyLevels.length && point.y + 1 < energyLevels[0].length) {
    incrementEnergyLevel({x: point.x + 1, y: point.y + 1}, energyLevels);
  }

  if (point.x - 1 >= 0 && point.y + 1 < energyLevels[0].length) {
    incrementEnergyLevel({x: point.x - 1, y: point.y + 1}, energyLevels);
  }

  if (point.x + 1 < energyLevels.length && point.y - 1 >= 0) {
    incrementEnergyLevel({x: point.x + 1, y: point.y - 1}, energyLevels);
  }

  if (point.x - 1 >= 0 && point.y - 1 >= 0) {
    incrementEnergyLevel({x: point.x - 1, y: point.y - 1}, energyLevels);
  }
}

function incrementEnergyLevel(point: Point, energyLevels: number[][]): void {
  const previousValue = energyLevels[point.y][point.x];

  energyLevels[point.y][point.x] += 1;

  if (previousValue === 9) {
    handleFlash(point, energyLevels);
  }
}

function step(energyLevels: number[][]): void {
  for (let i = 0; i < energyLevels.length; i++) {
    for (let j = 0; j < energyLevels[0].length; j++) {
      incrementEnergyLevel({x: j, y: i}, energyLevels);
    }
  }
}

// ?
function countAndResetOverflow(energyLevels: number[][]): number {
  let flashes = 0;

  for (let i = 0; i < energyLevels.length; i++) {
    for (let j = 0; j < energyLevels[0].length; j++) {
      if (energyLevels[i][j] > 9) {
        energyLevels[i][j] = 0;
        flashes += 1;
      }
    }
  }

  return flashes;
}

function countOctopusFlashes(energyLevels: number[][], steps: number): number {
  let flashes = 0;
  for (let i = 0; i < steps; i++) {
    // console.log("before step");
    // printLevels(energyLevels);

    step(energyLevels);
    flashes += countAndResetOverflow(energyLevels);

    // console.log("after step");
    // printLevels(energyLevels);
  }

  return flashes;
}

function countStepsUntilSynchronized(energyLevels: number[][]): number {
  let steps = 0;
  do {
    steps += 1;
    step(energyLevels);
  } while(countAndResetOverflow(energyLevels) != 100);

  return steps;
}

// modifies original array so we have to run one at a time

// console.log(`expected at 1: ${expected1}, actual: ${countOctopusFlashes(given, 1)}`);
// console.log(`expected at 2: ${expected2}, actual: ${countOctopusFlashes(given, 2)}`);
// console.log(`expected at 10: ${expected10}, actual: ${countOctopusFlashes(given, 10)}`);
// console.log(`expected at 100: ${expected100}, actual: ${countOctopusFlashes(given, 100)}`);
// console.log(`challenge 100: ${countOctopusFlashes(challenge, 100)}`);

console.log(`expected synchronized: ${expectedSynchronized}, actual: ${countStepsUntilSynchronized(given)}`);
console.log(`challenge synchronized: ${countStepsUntilSynchronized(challenge)}`);