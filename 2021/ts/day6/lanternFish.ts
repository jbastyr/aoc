const given = [3,4,3,1,2];
const expected18 = 26;
const expected80 = 5934;
const expected256 = 26984457539;

const challengeInput = [
  4,1,1,4,1,1,1,1,1,1,1,1,3,4,1,1,1,3,1,3,1,1,1,1,1,1,1,1,1,3,1,3,1,1,1,5,1,2,1,1,5,3,4,2,1,1,4,1,1,5,1,1,5,5,1,1,5,2,1,4,1,2,1,4,5,4,1,1,1,1,3,1,1,1,4,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,5,1,1,2,1,1,1,1,1,1,1,2,4,4,1,1,3,1,3,2,4,3,1,1,1,1,1,2,1,1,1,1,2,5,1,1,1,1,2,1,1,1,1,1,1,1,2,1,1,4,1,5,1,3,1,1,1,1,1,5,1,1,1,3,1,2,1,2,1,3,4,5,1,1,1,1,1,1,5,1,1,1,1,1,1,1,1,3,1,1,3,1,1,4,1,1,1,1,1,2,1,1,1,1,3,2,1,1,1,4,2,1,1,1,4,1,1,2,3,1,4,1,5,1,1,1,2,1,5,3,3,3,1,5,3,1,1,1,1,1,1,1,1,4,5,3,1,1,5,1,1,1,4,1,1,5,1,2,3,4,2,1,5,2,1,2,5,1,1,1,1,4,1,2,1,1,1,2,5,1,1,5,1,1,1,3,2,4,1,3,1,1,2,1,5,1,3,4,4,2,2,1,1,1,1,5,1,5,2
];


// This implementation works ok for smaller sets of data, but past 180 days or so it is super slow and ends up exceeding the heap
// since we are stuffing 100,000,000 items into this array. We can instead track the fish timer _counts_ just with the array index
// instead of needing to keep track of them as individual items

/**
function tickDay(fish: number[]): number[] {
  let newSpawn = 0;
  for (const f of fish) {
    if (f === 0) {
      newSpawn += 1;
    }
  }

  fish = fish.map(f => f === 0 ? 6 : f - 1);

  if (newSpawn > 0) {
    console.log(`adding ${newSpawn} fish`);
    Array(newSpawn).fill(8).forEach(f => fish.push(f));
  }

  return fish;
}

function calculateLanternFish(fish: number[], days: number): number {
  // console.log("Initial state: ", fish);
  for (let i = 0; i < days; i++) {
    fish = tickDay(fish);
    console.log(`After ${i + 1} day(s), fish: ${fish.length} `);
  }
  return fish.length;
}

**/

// use array index as the fish timer number instead of individual references to them
function reduceFishInput(fish: number[]): number[] {
  const fishTimers = Array(9).fill(0);

  fish.forEach(f => fishTimers[f] += 1);

  return fishTimers;
}

function tickDayFast(fishTimers: number[]): void {
  const newSpawn = fishTimers[0];

  for (let i = 1; i < fishTimers.length; i++) {
    fishTimers[i - 1] = fishTimers[i];
  }

  // respawn single fish
  fishTimers[6] += newSpawn;

  // new fish from cycle
  fishTimers[8] = newSpawn;
}

function calculateLanternFishFast(fish: number[], days: number): number {
  // console.log("Initial state: ", fish);
  let fishTimers = reduceFishInput(fish);
  for (let i = 0; i < days; i++) {
    tickDayFast(fishTimers)
    // console.log(`After ${i + 1} day(s), fish: `, fishTimers);
  }

  return fishTimers.reduce((acc,cur) => acc + cur);
}

console.log(`expected: ${expected18}, actual: ${calculateLanternFishFast(given, 18)}`);
console.log(`expected: ${expected80}, actual: ${calculateLanternFishFast(given, 80)}`);

console.log(`challenge: ${calculateLanternFishFast(challengeInput, 80)}`);

console.log(`expected: ${expected256}, actual: ${calculateLanternFishFast(given, 256)}`);

console.log(`challenge: ${calculateLanternFishFast(challengeInput, 256)}`);